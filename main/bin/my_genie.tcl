#-----------------------------------------------------------------------
# ::PUBLIC_COPYRIGHT_NOTICE::
#
# Version: ::RELNO::
#
#-----------------------------------------------------------------------


#----------------------------------------------------------------------s
# File:		java_genie.tcl
#
# Description:	The guts of the "java_genie" utility.
#
# Note:		In order to give (at least the appearance of) a speedy
#		script, we source in functionality from other files
#		only if we need to.
#-----------------------------------------------------------------------



#----
# This script uses a 3 spaces for indentation
#----
set at "@"


#----
# Process command-line args.
#----
smart_source "java_poa_genie/args.tcl"
parse_cmd_line_args



#----
# Make sure the the command-line args include at least one that
# will cause something to be generated.
#----
set found_needed_option 0

foreach option {interface client server antfile} {
  if {$pref(java_genie,want_$option)} { 
     set found_needed_option 1
     break 
  }
}
if {!$found_needed_option} {
   puts stderr "need to specify one or more of:"
   puts stderr "\t-interface, -client, -server, -antfile, -all"
   puts stderr "\tor specify -h to get a full usage description"
   exit
}


#----
# Parse the specified IDL file and terminate if there are errors
#----
if {$pref(all,want_diagnostics)} { 
   puts "$pref(java_genie,idl_file):"
}
set idl_file $pref(java_genie,idl_file)
set preproc_options $pref(java_genie,preproc_arg_list)

if {![idlgen_parse_idl_file $idl_file $preproc_options]} {
	exit
}


#----
# Find the list of interfaces the user wants us to work with.
# Terminate if there are none.
#----
determine_interface_list
set inter_list $pref(java_genie,inter_list)
if {$inter_list == {}} {
	puts stderr "error: no matching interfaces to process"
	exit
}

#----
# Unfortunately, the IDL-to-Java mapping has some holes in it.
# So check the IDL and warn the user if there is a possibility
# that we might generate java bad code.
#----
smart_source "std/java_poa_lib.tcl"
#java_sanity_check_idl



#----
# Copyright notice.
#----
if {![info exists pref(all,copyright)]} {
	set pref(all,copyright) {}
}

if {$pref(java_genie,want_interface) || $pref(java_genie,want_client)} {
   if {$pref(java_genie,want_complete)} {
	if {$pref(java_genie,want_log)} {
	   smart_source "java_poa_print/lib-full.tcl"
	   gen_java_print_funcs $pref(java_genie,want_any)
        }

	smart_source "java_poa_random/lib-full.tcl"
	gen_java_random_funcs $pref(java_genie,want_any)

   }
}

if {$pref(java_genie,want_client) && $pref(java_genie,want_complete)} {
   smart_source "java_poa_genie/gen_call_funcs.bi"
   gen_call_funcs $inter_list "call_funcs" $pref(all,copyright)
}

#----
# Generate client.java
#----
if {$pref(java_genie,want_client)} {
	smart_source "java_poa_genie/gen_client_java.bi"
	gen_client "[java_package_name ""]/client" $inter_list $pref(all,copyright)
}

#----
# Generate servant classes
#----
if {$pref(java_genie,want_servant)} {
    smart_source "java_poa_genie/gen_impl_java.bi"
    foreach inter $inter_list {
        gen_impl_java $inter $pref(all,copyright)
    }
}



#----
# Generate server.java
#----
if {$pref(java_genie,want_server)} {
	smart_source "java_poa_genie/gen_server_java.bi"
	gen_server_java "[java_package_name ""]/server" $inter_list $pref(all,copyright)
        switch $pref(java_genie,strategy) {
            activator {
                smart_source "java_poa_genie/gen_servant_activator_java.bi"
                gen_servant_activator_java "[java_package_name ""]/ServantActivatorImpl" \
                     $inter_list $pref(all,copyright)
            }
            locator {
                smart_source "java_poa_genie/gen_servant_locator_java.bi"
                gen_servant_locator_java "[java_package_name ""]/ServantLocatorImpl" \
                     $inter_list $pref(all,copyright)
            }
        }
}


#----
# Generate the ant build.xml file
#----
if {$pref(java_genie,want_antfile)} {
   smart_source "java_poa_genie/gen_ant_build_file.bi"
   gen_build_xml $pref(java_genie,platform) $pref(all,copyright)
}
