@echo off

REM Setting environment variables.
REM call setenvBrioOne

call idlgen my_genie.tcl -client -antfile -jP proto -I%DEAF_ROOT%\idl -dir %WORKDIR%\genie\client\%2 -complete %1

call idlgen my_genie.tcl -servant -server -antfile -jP proto -I%DEAF_ROOT%\idl -dir %WORKDIR%\genie\server\%2 -complete -notie -noinherit -threads -strategy activator -nons %1


REM @echo on

