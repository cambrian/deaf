@echo off

if "%1" == "noenv" goto IDLGEN

REM Setting environment variables.
call setenvDeaf


:IDLGEN
echo ************************************************************ 
echo Generating test server/client codes using idlgen
echo ************************************************************ 
call idlgen my_genie.tcl -include -client -antfile -jP proto -I%DEAF_ROOT%\idl -dir %WORKDIR%\genie\client -complete -ns %DEAF_ROOT%\idl\deaf\deaf.idl
call idlgen my_genie.tcl -include -servant -server -antfile -jP proto -I%DEAF_ROOT%\idl -dir %WORKDIR%\genie\server -complete -notie -inherit -threads -strategy activator -ns %DEAF_ROOT%\idl\deaf\deaf.idl
REM call idlgen my_genie.tcl -include -servant -server -antfile -jP proto -I%DEAF_ROOT%\idl -dir %WORKDIR%\genie\server -complete -tie -inherit -threads -strategy activator -nons %DEAF_ROOT%\idl\deaf\deaf.idl
if "%1" == "noenv" goto END


REM Cleaning up environment variables.
call unsetenvDeaf

@echo on

:END

