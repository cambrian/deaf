echo ************************************************************ 
echo Setting Environment Variables
echo ************************************************************ 

set DEAF_VERSION=011

set OLD_JAVA_HOME=%JAVA_HOME%
set OLD_IT_PRODUCT_DIR=%IT_PRODUCT_DIR%
set OLD_CLASSPATH=%CLASSPATH%
set OLD_PATH=%PATH%

if "%JAVA_HOME%" == "" set JAVA_HOME=H:\jdk1.3.1
set JDKDIR=%JAVA_HOME%
set JREDIR=%JAVA_HOME%\jre
set JAVACOMPILE=%JDKDIR%\bin\javac
set JAVADOC_CMD=%JDKDIR%\bin\javadoc

if "%DEAF_ROOT%" == "" set DEAF_ROOT=F:\deaf\main
if "%BUILD_ROOT%" == "" set BUILD_ROOT=%DEAF_ROOT%\build
set SOURCE_ROOT=%DEAF_ROOT%\java
set SOURCE_FILES=%SOURCE_ROOT%\proxy;%SOURCE_ROOT%\servant;%SOURCE_ROOT%\util;%SOURCE_ROOT%\impl;%SOURCE_ROOT%\test;%BUILD_ROOT%\idlgen
set WORKDIR=%BUILD_ROOT%
set OUTPUT_FILES=%WORKDIR%\classes
set APIDOC_DIR=%WORKDIR%\apidoc
set IDLGEN_DIR=%WORKDIR%\idlgen
set IDLGEN_OUTPUT_FILES=%WORKDIR%\idlgen\classes
REM set LIB_JARS=%DEAF_ROOT%\lib\log4j.jar;%DEAF_ROOT%\lib\junit.jar
set LIB_JARS=
for %%i in (%DEAF_ROOT%\lib\*.jar) do call addlibjar.bat %%i
set IDLGEN_JAR=%WORKDIR%\lib\idlgen.jar
set DEAF_PROXY_JAR=%WORKDIR%\lib\deaf_proxy.jar
set DEAF_IMPL_JAR=%WORKDIR%\lib\deaf_impl.jar
set DEAF_JAR=%WORKDIR%\lib\deaf%DEAF_VERSION%.jar

set DB_DRIVER=%DEAF_ROOT%\lib\classes12.zip

if "%IT_PRODUCT_DIR%" == "" set IT_PRODUCT_DIR=H:\iona
set IONA_HOME=%IT_PRODUCT_DIR%
set IONA_IDL_DIR=%IONA_HOME%\orbix_art\1.2\idl
set IONA_JAR_HOME=%IONA_HOME%\orbix_art\1.2\classes
set IONA_CONFIG_DIR=%IONA_HOME%\orbix_art\1.2;%IONA_HOME%\orbix_art\1.2\localhost;%IONA_HOME%\etc
set IONA_CONTRIB_CLASSES=%IONA_HOME%\orbix_art\1.2\contrib\classes\jdeps.jar


set IDLCLASSPATH=%IDLGEN_OUTPUT_FILES%;%DB_DRIVER%;%LIB_JARS%;%JDKDIR%\lib\tools.jar;%JDKDIR%\lib\dt.jar;%JREDIR%\lib\jaws.jar;%JREDIR%\lib\rt.jar;%JREDIR%\lib\i18n.jar;%IONA_JAR_HOME%\orbix2000.jar;%IONA_JAR_HOME%\omg.jar;%WORKDIR%;%IONA_CONTRIB_CLASSES%;%IONA_CONFIG_DIR%

set CLASSPATH=%IDLGEN_JAR%;%OUTPUT_FILES%\proxy;%OUTPUT_FILES%\servant;%OUTPUT_FILES%\util;%OUTPUT_FILES%\impl;%OUTPUT_FILES%\test;%DB_DRIVER%;%LIB_JARS%;%JDKDIR%\lib\tools.jar;%JDKDIR%\lib\dt.jar;%JREDIR%\lib\jaws.jar;%JREDIR%\lib\rt.jar;%JREDIR%\lib\i18n.jar;%IONA_JAR_HOME%\orbix2000.jar;%IONA_JAR_HOME%\omg.jar;%WORKDIR%;%IONA_CONTRIB_CLASSES%;%IONA_CONFIG_DIR%


set PATH=%BUILD_ROOT%;%DEAF_ROOT%;%PATH%

