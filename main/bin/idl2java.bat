@echo off

REM Setting environment variables.
REM call setenvBrioOne

REM [1] Using OpenORB
REM java org.openorb.compiler.IdlCompiler %1 -I ..\idl -notie -d ..\build

REM [2] Using Orbix
call idl -I%DEAF_ROOT% -I%DEAF_ROOT%\idl -I%WORKDIR%\idlgen -I%IONA_IDL_DIR% -jbase:-O%IDLGEN_DIR% -jpoa:-O%IDLGEN_DIR% %1
REM call idl -v -I%DEAF_ROOT% -I%DEAF_ROOT%\idl -I%WORKDIR%\idlgen -I%IONA_IDL_DIR% -jbase:-O%IDLGEN_DIR% -jpoa:-O%IDLGEN_DIR% %1

REM @echo on

