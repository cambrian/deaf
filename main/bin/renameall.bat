for %%i in (%DEAF_ROOT%\java\proxy\deaf\*.java) do perl rename.pl %%i
for %%i in (%DEAF_ROOT%\java\proxy\deaf\app\*.java) do perl rename.pl %%i
for %%i in (%DEAF_ROOT%\java\proxy\deaf\admin\*.java) do perl rename.pl %%i
for %%i in (%DEAF_ROOT%\java\proxy\deaf\auth\*.java) do perl rename.pl %%i
for %%i in (%DEAF_ROOT%\java\proxy\deaf\client\*.java) do perl rename.pl %%i
for %%i in (%DEAF_ROOT%\java\proxy\deaf\persist\*.java) do perl rename.pl %%i

for %%i in (%DEAF_ROOT%\java\impl\deaf\*.java) do perl rename.pl %%i
for %%i in (%DEAF_ROOT%\java\impl\deaf\app\*.java) do perl rename.pl %%i
for %%i in (%DEAF_ROOT%\java\impl\deaf\admin\*.java) do perl rename.pl %%i
for %%i in (%DEAF_ROOT%\java\impl\deaf\auth\*.java) do perl rename.pl %%i
for %%i in (%DEAF_ROOT%\java\impl\deaf\client\*.java) do perl rename.pl %%i
for %%i in (%DEAF_ROOT%\java\impl\deaf\persist\*.java) do perl rename.pl %%i

for %%i in (%DEAF_ROOT%\java\servant\deaf\*.java) do perl rename.pl %%i
for %%i in (%DEAF_ROOT%\java\servant\deaf\app\*.java) do perl rename.pl %%i
for %%i in (%DEAF_ROOT%\java\servant\deaf\admin\*.java) do perl rename.pl %%i
for %%i in (%DEAF_ROOT%\java\servant\deaf\auth\*.java) do perl rename.pl %%i
for %%i in (%DEAF_ROOT%\java\servant\deaf\client\*.java) do perl rename.pl %%i
for %%i in (%DEAF_ROOT%\java\servant\deaf\persist\*.java) do perl rename.pl %%i

for %%i in (%DEAF_ROOT%\java\util\deaf\*.java) do perl rename.pl %%i
for %%i in (%DEAF_ROOT%\java\util\deaf\app\*.java) do perl rename.pl %%i
for %%i in (%DEAF_ROOT%\java\util\deaf\admin\*.java) do perl rename.pl %%i
for %%i in (%DEAF_ROOT%\java\util\deaf\auth\*.java) do perl rename.pl %%i
for %%i in (%DEAF_ROOT%\java\util\deaf\client\*.java) do perl rename.pl %%i
for %%i in (%DEAF_ROOT%\java\util\deaf\persist\*.java) do perl rename.pl %%i

for %%i in (%DEAF_ROOT%\java\util\deaf\util\*.java) do perl rename.pl %%i
for %%i in (%DEAF_ROOT%\java\util\deaf\proto\*.java) do perl rename.pl %%i

