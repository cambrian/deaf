@echo off

REM Setting environment variables.
call setenvDeaf


if "%1" == "env" goto END
if "%1" == "idl" goto IDL 
if "%1" == "idlall" goto IDL 
if "%1" == "genie" goto GENIE 
if "%1" == "java" goto CODE 
if "%1" == "jar" goto JAR 
if "%1" == "javadoc" goto JAVADOC 
if "%1" == "" goto ALL
goto CODE


:ALL

:IDL
echo ************************************************************ 
echo Compiling IDLs to Generate Java Code
echo ************************************************************ 
rem Compile Base IDL Code
for %%i in (%DEAF_ROOT%\idl\deaf\*.idl) do call idl2java.bat %%i
for %%i in (%DEAF_ROOT%\idl\deaf\app\*.idl) do call idl2java.bat %%i
for %%i in (%DEAF_ROOT%\idl\deaf\admin\*.idl) do call idl2java.bat %%i
for %%i in (%DEAF_ROOT%\idl\deaf\auth\*.idl) do call idl2java.bat %%i
for %%i in (%DEAF_ROOT%\idl\deaf\client\*.idl) do call idl2java.bat %%i
for %%i in (%DEAF_ROOT%\idl\deaf\persist\*.idl) do call idl2java.bat %%i
if "%1" == "idl" goto CLEANUP


:CODE

set JAVAC_FLAG=-deprecation -g 


if "%1" == "idlgen" goto IDLGEN
if "%1" == "idlgennoclean" goto IDLGENNOCLEAN
if "%1" == "genie" goto GENIE
if "%1" == "javanoidl" goto JAVANOIDL
if "%1" == "proxy" goto PROXY
if "%1" == "servant" goto SERVANT
if "%1" == "util" goto UTIL
if "%1" == "impl" goto IMPL
if "%1" == "test" goto TEST
if "%1" == "jar" goto JAR
if "%1" == "clean" goto CLEAN
if "%1" == "noclean" goto CORE


:IDLCLEAN
:IDLGEN
echo Removing Directory: %IDLGEN_OUTPUT_FILES%\deaf
rmdir /s /q %IDLGEN_OUTPUT_FILES%\deaf
REM echo Directory %IDLGEN_OUTPUT_FILES%\deaf removed
if "%1" == "idlclean" goto CLEANUP


:IDLGENNOCLEAN
rem Compile IDL generated files
echo ************************************************************ 
echo Compiling IDL generated files
echo ************************************************************ 

%JAVACOMPILE% %JAVAC_FLAG% -d %IDLGEN_OUTPUT_FILES% -sourcepath %IDLGEN_DIR% -classpath %IDLCLASSPATH% %IDLGEN_DIR%\deaf\*.java
%JAVACOMPILE% %JAVAC_FLAG% -d %IDLGEN_OUTPUT_FILES% -sourcepath %IDLGEN_DIR% -classpath %IDLCLASSPATH% %IDLGEN_DIR%\deaf\app\*.java
%JAVACOMPILE% %JAVAC_FLAG% -d %IDLGEN_OUTPUT_FILES% -sourcepath %IDLGEN_DIR% -classpath %IDLCLASSPATH% %IDLGEN_DIR%\deaf\admin\*.java
%JAVACOMPILE% %JAVAC_FLAG% -d %IDLGEN_OUTPUT_FILES% -sourcepath %IDLGEN_DIR% -classpath %IDLCLASSPATH% %IDLGEN_DIR%\deaf\auth\*.java
%JAVACOMPILE% %JAVAC_FLAG% -d %IDLGEN_OUTPUT_FILES% -sourcepath %IDLGEN_DIR% -classpath %IDLCLASSPATH% %IDLGEN_DIR%\deaf\client\*.java
%JAVACOMPILE% %JAVAC_FLAG% -d %IDLGEN_OUTPUT_FILES% -sourcepath %IDLGEN_DIR% -classpath %IDLCLASSPATH% %IDLGEN_DIR%\deaf\persist\*.java
%JAVACOMPILE% %JAVAC_FLAG% -d %IDLGEN_OUTPUT_FILES% -sourcepath %IDLGEN_DIR% -classpath %IDLCLASSPATH% %IDLGEN_DIR%\deaf\BasePolicyPackage\*.java


:IDLJAR
echo ************************************************************ 
echo Packaging idl-generated class files into Jar
echo ************************************************************ 
del /q %IDLGEN_JAR%
jar cf %IDLGEN_JAR% -C %IDLGEN_OUTPUT_FILES% deaf



:GENIE
if not "%1" == "genie" goto SKIPGENIE
echo ************************************************************ 
echo Generating test server/client codes using idlgen.
echo ************************************************************ 
call genieall noenv
call mvpackageall
if "%1" == "genie" goto CLEANUP
:SKIPGENIE



if "%1" == "idlgen" goto CLEANUP
if "%1" == "idlgennoclean" goto CLEANUP
if "%1" == "idlall" goto CLEANUP


:JAVANOIDL

:CLEAN
echo Removing Directory: %OUTPUT_FILES%\proxy\deaf
rmdir /s /q %OUTPUT_FILES%\proxy\deaf
REM echo Directory %OUTPUT_FILES%\proxy\deaf removed
echo Removing Directory: %OUTPUT_FILES%\servant\deaf
rmdir /s /q %OUTPUT_FILES%\servant\deaf
REM echo Directory %OUTPUT_FILES%\servant\deaf removed
echo Removing Directory: %OUTPUT_FILES%\impl\deaf
rmdir /s /q %OUTPUT_FILES%\impl\deaf
REM echo Directory %OUTPUT_FILES%\impl\deaf removed
echo Removing Directory: %OUTPUT_FILES%\util\deaf
rmdir /s /q %OUTPUT_FILES%\util\deaf
REM echo Directory %OUTPUT_FILES%\util\deaf removed
echo Removing Directory: %OUTPUT_FILES%\test\deaf
rmdir /s /q %OUTPUT_FILES%\test\deaf
REM echo Directory %OUTPUT_FILES%\test\deaf removed
if "%1" == "clean" goto CLEANUP



:CORE

:PROXY
rem Compile Proxy Package
echo ************************************************************ 
echo Compiling Proxy Package
echo ************************************************************ 
%JAVACOMPILE% %JAVAC_FLAG% -d %OUTPUT_FILES%\proxy -sourcepath %SOURCE_FILES% -classpath %CLASSPATH% %SOURCE_ROOT%\proxy\deaf\*.java 
%JAVACOMPILE% %JAVAC_FLAG% -d %OUTPUT_FILES%\proxy -sourcepath %SOURCE_FILES% -classpath %CLASSPATH% %SOURCE_ROOT%\proxy\deaf\admin\*.java 
%JAVACOMPILE% %JAVAC_FLAG% -d %OUTPUT_FILES%\proxy -sourcepath %SOURCE_FILES% -classpath %CLASSPATH% %SOURCE_ROOT%\proxy\deaf\app\*.java 
%JAVACOMPILE% %JAVAC_FLAG% -d %OUTPUT_FILES%\proxy -sourcepath %SOURCE_FILES% -classpath %CLASSPATH% %SOURCE_ROOT%\proxy\deaf\auth\*.java 
%JAVACOMPILE% %JAVAC_FLAG% -d %OUTPUT_FILES%\proxy -sourcepath %SOURCE_FILES% -classpath %CLASSPATH% %SOURCE_ROOT%\proxy\deaf\client\*.java 
%JAVACOMPILE% %JAVAC_FLAG% -d %OUTPUT_FILES%\proxy -sourcepath %SOURCE_FILES% -classpath %CLASSPATH% %SOURCE_ROOT%\proxy\deaf\persist\*.java 
if "%1" == "proxy" goto CLEANUP



:SERVANT
rem Compile Servant Package
echo ************************************************************ 
echo Compiling Servant Package
echo ************************************************************ 
%JAVACOMPILE% %JAVAC_FLAG% -d %OUTPUT_FILES%\servant -sourcepath %SOURCE_FILES% -classpath %CLASSPATH% %SOURCE_ROOT%\servant\deaf\*.java 
%JAVACOMPILE% %JAVAC_FLAG% -d %OUTPUT_FILES%\servant -sourcepath %SOURCE_FILES% -classpath %CLASSPATH% %SOURCE_ROOT%\servant\deaf\admin\*.java 
%JAVACOMPILE% %JAVAC_FLAG% -d %OUTPUT_FILES%\servant -sourcepath %SOURCE_FILES% -classpath %CLASSPATH% %SOURCE_ROOT%\servant\deaf\app\*.java 
%JAVACOMPILE% %JAVAC_FLAG% -d %OUTPUT_FILES%\servant -sourcepath %SOURCE_FILES% -classpath %CLASSPATH% %SOURCE_ROOT%\servant\deaf\auth\*.java 
%JAVACOMPILE% %JAVAC_FLAG% -d %OUTPUT_FILES%\servant -sourcepath %SOURCE_FILES% -classpath %CLASSPATH% %SOURCE_ROOT%\servant\deaf\client\*.java 
%JAVACOMPILE% %JAVAC_FLAG% -d %OUTPUT_FILES%\servant -sourcepath %SOURCE_FILES% -classpath %CLASSPATH% %SOURCE_ROOT%\servant\deaf\persist\*.java 
if "%1" == "servant" goto CLEANUP


:IMPL
rem Compile Impl Package
echo ************************************************************ 
echo Compiling Impl Package
echo ************************************************************ 
%JAVACOMPILE% %JAVAC_FLAG% -d %OUTPUT_FILES%\impl -sourcepath %SOURCE_FILES% -classpath %CLASSPATH% %SOURCE_ROOT%\impl\deaf\*.java 
%JAVACOMPILE% %JAVAC_FLAG% -d %OUTPUT_FILES%\impl -sourcepath %SOURCE_FILES% -classpath %CLASSPATH% %SOURCE_ROOT%\impl\deaf\admin\*.java 
%JAVACOMPILE% %JAVAC_FLAG% -d %OUTPUT_FILES%\impl -sourcepath %SOURCE_FILES% -classpath %CLASSPATH% %SOURCE_ROOT%\impl\deaf\app\*.java 
%JAVACOMPILE% %JAVAC_FLAG% -d %OUTPUT_FILES%\impl -sourcepath %SOURCE_FILES% -classpath %CLASSPATH% %SOURCE_ROOT%\impl\deaf\auth\*.java 
%JAVACOMPILE% %JAVAC_FLAG% -d %OUTPUT_FILES%\impl -sourcepath %SOURCE_FILES% -classpath %CLASSPATH% %SOURCE_ROOT%\impl\deaf\client\*.java 
%JAVACOMPILE% %JAVAC_FLAG% -d %OUTPUT_FILES%\impl -sourcepath %SOURCE_FILES% -classpath %CLASSPATH% %SOURCE_ROOT%\impl\deaf\persist\*.java 
if "%1" == "impl" goto CLEANUP



:UTIL
rem Compile Util Package
echo ************************************************************ 
echo Compiling Util Package
echo ************************************************************ 
%JAVACOMPILE% %JAVAC_FLAG% -d %OUTPUT_FILES%\util -sourcepath %SOURCE_FILES% -classpath %CLASSPATH% %SOURCE_ROOT%\util\deaf\util\*.java 
%JAVACOMPILE% %JAVAC_FLAG% -d %OUTPUT_FILES%\util -sourcepath %SOURCE_FILES% -classpath %CLASSPATH% %SOURCE_ROOT%\util\deaf\proto\*.java 

%JAVACOMPILE% %JAVAC_FLAG% -d %OUTPUT_FILES%\util -sourcepath %SOURCE_FILES% -classpath %CLASSPATH% %SOURCE_ROOT%\util\deaf\*.java 
%JAVACOMPILE% %JAVAC_FLAG% -d %OUTPUT_FILES%\util -sourcepath %SOURCE_FILES% -classpath %CLASSPATH% %SOURCE_ROOT%\util\deaf\admin\*.java 
%JAVACOMPILE% %JAVAC_FLAG% -d %OUTPUT_FILES%\util -sourcepath %SOURCE_FILES% -classpath %CLASSPATH% %SOURCE_ROOT%\util\deaf\app\*.java 
%JAVACOMPILE% %JAVAC_FLAG% -d %OUTPUT_FILES%\util -sourcepath %SOURCE_FILES% -classpath %CLASSPATH% %SOURCE_ROOT%\util\deaf\auth\*.java 
%JAVACOMPILE% %JAVAC_FLAG% -d %OUTPUT_FILES%\util -sourcepath %SOURCE_FILES% -classpath %CLASSPATH% %SOURCE_ROOT%\util\deaf\client\*.java 
%JAVACOMPILE% %JAVAC_FLAG% -d %OUTPUT_FILES%\util -sourcepath %SOURCE_FILES% -classpath %CLASSPATH% %SOURCE_ROOT%\util\deaf\persist\*.java 

REM %JAVACOMPILE% %JAVAC_FLAG% -d %OUTPUT_FILES%\util -sourcepath %SOURCE_FILES% -classpath %CLASSPATH% %SOURCE_ROOT%\util\deaf\admin\util\*.java 
REM %JAVACOMPILE% %JAVAC_FLAG% -d %OUTPUT_FILES%\util -sourcepath %SOURCE_FILES% -classpath %CLASSPATH% %SOURCE_ROOT%\util\deaf\app\util\*.java 
REM %JAVACOMPILE% %JAVAC_FLAG% -d %OUTPUT_FILES%\util -sourcepath %SOURCE_FILES% -classpath %CLASSPATH% %SOURCE_ROOT%\util\deaf\auth\util\*.java 
REM %JAVACOMPILE% %JAVAC_FLAG% -d %OUTPUT_FILES%\util -sourcepath %SOURCE_FILES% -classpath %CLASSPATH% %SOURCE_ROOT%\util\deaf\client\util\*.java 
REM %JAVACOMPILE% %JAVAC_FLAG% -d %OUTPUT_FILES%\util -sourcepath %SOURCE_FILES% -classpath %CLASSPATH% %SOURCE_ROOT%\util\deaf\persist\util\*.java 

if "%1" == "util" goto CLEANUP


:TEST
rem Compile Test Package
echo ************************************************************ 
echo Compiling Test Package
echo ************************************************************ 
%JAVACOMPILE% %JAVAC_FLAG% -d %OUTPUT_FILES%\test -sourcepath %SOURCE_FILES% -classpath %CLASSPATH% %SOURCE_ROOT%\test\deaf\*.java 

REM %JAVACOMPILE% %JAVAC_FLAG% -d %OUTPUT_FILES%\test -sourcepath %SOURCE_FILES% -classpath %CLASSPATH% %SOURCE_ROOT%\test\deaf\admin\*.java 
REM %JAVACOMPILE% %JAVAC_FLAG% -d %OUTPUT_FILES%\test -sourcepath %SOURCE_FILES% -classpath %CLASSPATH% %SOURCE_ROOT%\test\deaf\app\*.java 
REM %JAVACOMPILE% %JAVAC_FLAG% -d %OUTPUT_FILES%\test -sourcepath %SOURCE_FILES% -classpath %CLASSPATH% %SOURCE_ROOT%\test\deaf\auth\*.java 
REM %JAVACOMPILE% %JAVAC_FLAG% -d %OUTPUT_FILES%\test -sourcepath %SOURCE_FILES% -classpath %CLASSPATH% %SOURCE_ROOT%\test\deaf\client\*.java 
REM %JAVACOMPILE% %JAVAC_FLAG% -d %OUTPUT_FILES%\test -sourcepath %SOURCE_FILES% -classpath %CLASSPATH% %SOURCE_ROOT%\test\deaf\persist\*.java 
if "%1" == "test" goto CLEANUP


if "%1" == "java" goto CLEANUP


:JAR
REM Packaging class files into Jar
echo ************************************************************ 
echo Creating Jar files
echo ************************************************************ 
del /q %DEAF_PROXY_JAR%
jar cf %DEAF_PROXY_JAR% -C %OUTPUT_FILES%\proxy deaf
del /q %DEAF_IMPL_JAR%
jar cf %DEAF_IMPL_JAR% -C %OUTPUT_FILES%\servant deaf
jar uf %DEAF_IMPL_JAR% -C %OUTPUT_FILES%\util deaf
jar uf %DEAF_IMPL_JAR% -C %OUTPUT_FILES%\impl deaf

del /q %DEAF_JAR%
jar cfm %DEAF_JAR% %DEAF_ROOT%\bin\deaf.mf -C %IDLGEN_OUTPUT_FILES% deaf
jar uf %DEAF_JAR% -C %OUTPUT_FILES%\proxy deaf
jar uf %DEAF_JAR% -C %OUTPUT_FILES%\servant deaf
jar uf %DEAF_JAR% -C %OUTPUT_FILES%\util deaf
jar uf %DEAF_JAR% -C %OUTPUT_FILES%\impl deaf

if "%1" == "jar" goto CLEANUP


:JAVADOC
if not "%1" == "javadoc" goto SKIPJAVADOC
echo ************************************************************ 
echo Creating API docs...
echo ************************************************************ 
del /q %APIDOC_DIR%
%JAVADOC_CMD% -d %APIDOC_DIR% -sourcepath %SOURCE_FILES% @%DEAF_ROOT%\bin\package.list
if "%1" == "javadoc" goto CLEANUP
:SKIPJAVADOC



:CLEANUP
REM Cleaning up environment variables.
call unsetenvDeaf


:END
@echo on

