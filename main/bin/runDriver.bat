@echo off
REM ************************************************************ 
REM Script to run test servers. 
REM Prerequisite:
REM    You need to have both setenvDeaf and unsetenvDeaf scripts in your path.
REM    Otherwise, set JREDIR and CLASSPATH in this script.
REM ************************************************************ 


REM Setting environment variables.
call setenvDeaf


set JCMD=%JREDIR%\bin\java 
REM set JCMD=%JREDIR%\bin\java -Dorg.omg.CORBA.ORBClass=com.iona.corba.art.artimpl.ORBImpl -Dorg.omg.CORBA.ORBSingletonClass=com.iona.corba.art.artimpl.ORBSingleton
REM set JCMD=%JDKDIR%\bin\jdb -tclassic -launch -sourcepath %SOURCE_FILES%


if "%1" == "env" goto END
if "%1" == "cleanup" goto CLEANUP
if "%1" == "driver" goto DRIVER
if "%1" == "server" goto SERVER
if "%1" == "client" goto CLIENT
goto DRIVER


:DRIVER
echo [[[ Starting the driver...                               ]]] 
%JCMD% -classpath %CLASSPATH%  deaf.proto.Driver %1 %2 %3 %4 %5 %6 %7 %8 %9
goto CLEANUP


:SERVER
echo [[[ Starting the server...                               ]]] 
%JCMD% -classpath %CLASSPATH%  deaf.proto.TheServer %1 %2 %3 %4 %5 %6 %7 %8 %9
goto CLEANUP


:CLIENT
echo [[[ Starting the client...                               ]]] 
%JCMD% -classpath %CLASSPATH%  deaf.proto.TheClient %1 %2 %3 %4 %5 %6 %7 %8 %9
goto CLEANUP


:CLEANUP
REM Cleaning up environment variables.
call unsetenvDeaf


:END
@echo on
