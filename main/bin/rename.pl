#!/usr/bin/perl

# This script replaces all occurrences of "Impl" with "..."

# Multiple files can be given as input
for($i=0;$i<@ARGV;$i++) {
    $tmpfile = $ARGV[$i] . "_tmp";
    rename($ARGV[$i],$tmpfile);
    $newfile = $ARGV[$i];

    open(HLFILE, $tmpfile) || die "Sorry, I couldn't open $tmpfile\n";
    open(HFILE, ">".$newfile) || die "Sorry, I couldn't open $newfile\n";
    while(<HLFILE>) {
        s/\$ Id: \$/\$Id: rename.pl,v 1.2 2001/08/13 06:00:08 hyoon Exp $/g;

        print HFILE $_;
    }
    close(HFILE);
    close(HLFILE);
    unlink($tmpfile);
}

