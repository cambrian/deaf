/************************************************************
 *  Copyright 1999, Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis, 
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 ************************************************************/

// Global variables
var ForReading = 1;
var ForWriting = 2;
var fso = new ActiveXObject("Scripting.FileSystemObject");

var CR = "\r\n";
var header = "";
header += "/** " + CR;
header += " *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved." + CR;
header += " *  This software is distributed on an \"AS IS\" basis,               " + CR;
header += " *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        " + CR;
header += " *  " + CR;
header += " *  $" + "Id: $ " + CR;
header += " */ " + CR;
var oldPackageName = "deaf";
var newPackageName = "deaf";
var newAuthor = "Hyoungsoo Yoon";


// Functions
var javaCode = "";
function  changePackageName()
{
  var tmpCode = "";
  var lines = javaCode.split(CR);

  var  beforePackage = true;
  for(m=0;m<lines.length;m++)
  {
    if(lines[m].indexOf("package") == 0)
    {
        tmpCode += header;
        //tmpCode += "package " + newPackageName + ";" + CR;
        tmpCode += lines[m] + CR;
        beforePackage = false;
    }
    else if(beforePackage)
    {
        // skip
    }
    else
    {
      if(lines[m].indexOf("import") == 0)
      {
          //var re = new RegExp(oldPackageName, "i");
          //var newLine = lines[m].replace(re,newPackageName); 
          //tmpCode += newLine + CR;
          tmpCode += lines[m] + CR;
      }
      else if(lines[m].indexOf("@author") >= 0)
      {
          //tmpCode += "@author   " + newAuthor + CR;
          tmpCode += lines[m] + CR;
      }
      else if(m == lines.length-1) // last line
      {
          tmpCode += lines[m];
      }
      else 
      {
          tmpCode += lines[m] + CR;
      }
    }
  }
  javaCode = tmpCode;
}


function changeFolder(folderItem)
{
    var fpath = folderItem.path;
    var fc = new Enumerator(folderItem.files);
    for (; !fc.atEnd(); fc.moveNext())
    {
        var fileName = fc.item();
        var oldName = fileName.name;
        var tmpName = oldName.split('.');
        var fileRoot = fpath + "\\" + tmpName[0];
        var fileExt = tmpName[1];

        if(fileExt == 'java') {
            var oFile = fso.OpenTextFile(fileName, ForReading);
            javaCode = oFile.ReadAll();
            oFile.Close();
    
            var backupName = fileRoot + '.java2';
            var f2 = fso.GetFile(fileName);
            f2.Move (backupName);

            var newName = fileRoot + '.java';  
            var hFile = fso.OpenTextFile(newName, ForWriting, true);    
            //hFile = fso.CreateTextFile(fileName, true);
            changePackageName();
            hFile.WriteLine(javaCode) ;
            hFile.Close();
        }
    }
}


function  doFolders(folderPath)
{
    var f = fso.GetFolder(folderPath);
    changeFolder(f);
    var fc = new Enumerator(f.SubFolders);
    for (; !fc.atEnd(); fc.moveNext())
    {
        var sfolder = fc.item();
        doFolders(sfolder);
    }
}


// Main part
//var folderPath = "F:\\deaf\\main\\java";
doFolders(folderPath);

