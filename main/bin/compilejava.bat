@echo off

if "%1" == "" goto END

REM Setting environment variables.
call setenvDeaf

set JAVAC_FLAG=-deprecation -g

echo *** Compiling %1
%JAVACOMPILE% %JAVAC_FLAG% -d %OUTPUT_FILES% -sourcepath %SOURCE_FILES% -classpath %CLASSPATH% "%1"
echo *** Done.

REM Cleaning up environment variables.
call unsetenvDeaf

:END
@echo on

