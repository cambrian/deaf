#!/usr/bin/perl

# This script replaces all occurrences of "proto.deaf." with "deaf."

# Multiple files can be given as input
for($i=0;$i<@ARGV;$i++) {
    $tmpfile = $ARGV[$i] . "_tmp";
    rename($ARGV[$i],$tmpfile);
    $newfile = $ARGV[$i];

    open(HLFILE, $tmpfile) || die "Sorry, I couldn't open $tmpfile\n";
    open(HFILE, ">".$newfile) || die "Sorry, I couldn't open $newfile\n";
    while(<HLFILE>) {
        s/proto\.deaf/deaf/g;
        s/import\ proto\.\*/import\ deaf\.\*/g;
        s/import\ idlgen\.\*\;//g;
        print HFILE $_;
    }
    close(HFILE);
    close(HLFILE);
    unlink($tmpfile);
}

