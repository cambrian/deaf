echo ************************************************************ 
echo Resetting Environment Variables
echo ************************************************************ 

set DEAF_VERSION=

set JDKDIR=
set JREDIR=
set JAVACOMPILE=
set JAVADOC_CMD=
set WORKDIR=
set SOURCE_ROOT=
set SOURCE_FILES=
set OUTPUT_FILES=
set APIDOC_DIR=
set IDLGEN_DIR=
set IDLGEN_OUTPUT_FILES=
set IDLCLASSPATH=
set IDLGEN_JAR=
set DEAF_PROXY_JAR=
set DEAF_IMPL_JAR=
set DEAF_JAR=
set LIB_JARS=
set IONA_HOME=
set IONA_IDL_DIR=
set IONA_JAR_HOME=
set IONA_CONFIG_DIR=
set IONA_CONTRIB_CLASSES=
set DB_DRIVER=


set JAVA_HOME=%OLD_JAVA_HOME%
set IT_PRODUCT_DIR=%OLD_IT_PRODUCT_DIR%
set CLASSPATH=%OLD_CLASSPATH%
set PATH=%OLD_PATH%

set OLD_JAVA_HOME=
set OLD_IT_PRODUCT_DIR=
set OLD_CLASSPATH=
set OLD_PATH=

