/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: AuthenticationManagerProxy.java,v 1.4 2001/08/17 06:10:15 hyoon Exp $ 
 */ 
package deaf.auth;

import  deaf.*;
import  org.apache.log4j.*;
import  org.omg.CORBA.SystemException;


public class AuthenticationManagerProxy
    extends ManagerProxy
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(AuthenticationManagerProxy.class);
    }


    /**
     * Constructor.
     */
    public AuthenticationManagerProxy()
    {
        super();
    }

    /**
     * Constructor.
     */
    public AuthenticationManagerProxy(AuthenticationManager intf)
    {
        super(intf);
    }

    
    /**
     * Returns the reference to the CORBA object.
     */
    public AuthenticationManager getAuthenticationManagerReference()
    {
        return AuthenticationManagerHelper.narrow(getManagerReference());
    }

}



