/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: AuthorizationEngineProxy.java,v 1.4 2001/08/17 06:10:15 hyoon Exp $ 
 */ 
package deaf.auth;

import  deaf.*;
import  org.apache.log4j.*;
import  org.omg.CORBA.SystemException;


public class AuthorizationEngineProxy
    extends EngineProxy
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(AuthorizationEngineProxy.class);
    }


    /**
     * Constructor.
     */
    public AuthorizationEngineProxy()
    {
        super();
    }

    /**
     * Constructor.
     */
    public AuthorizationEngineProxy(AuthorizationEngine intf)
    {
        super(intf);
    }

    
    /**
     * Returns the reference to the CORBA object.
     */
    public AuthorizationEngine getAuthorizationEngineReference()
    {
        return AuthorizationEngineHelper.narrow(getEngineReference());
    }


    public deaf.Role getRole(
        deaf.SessionToken token
    ) throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering getRole(). SessionToken=" + toNonNullString(token));
        }
        
        deaf.Role _result = null;
        try {
            _result = getAuthorizationEngineReference().getRole(token);
        } catch(org.omg.CORBA.SystemException se) {
            logCat.error("CORBA SystemException: " + se.toString());
            System.out.println("getRole() failed with the following exception");
            se.printStackTrace(System.out);
        }

        return _result;
	}


}



