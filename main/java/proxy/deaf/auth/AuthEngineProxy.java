/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: AuthEngineProxy.java,v 1.4 2001/08/17 06:10:15 hyoon Exp $ 
 */ 
package deaf.auth;

import  deaf.*;
import  org.apache.log4j.*;
import  org.omg.CORBA.SystemException;


public class AuthEngineProxy
    extends EngineProxy
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(AuthEngineProxy.class);
    }


    /**
     * Constructor.
     */
    public AuthEngineProxy()
    {
        super();
    }

    /**
     * Constructor.
     */
    public AuthEngineProxy(AuthEngine intf)
    {
        super(intf);
    }

    
    /**
     * Returns the reference to the CORBA object.
     */
    public AuthEngine getAuthEngineReference()
    {
        return AuthEngineHelper.narrow(getEngineReference());
    }


    public deaf.auth.AuthenticationManagerProxy getAuthenticationManager() throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering getAuthenticationManager().");
        }
        
        deaf.auth.AuthenticationManager _result = null;
        try {
            _result = getAuthEngineReference().getAuthenticationManager();
        } catch(org.omg.CORBA.SystemException se) {
            logCat.error("CORBA SystemException: " + se.toString());
            System.out.println("getAuthenticationManager() failed with the following exception");
            se.printStackTrace(System.out);
        }

        return new AuthenticationManagerProxy(_result);
	}


    public deaf.auth.AuthorizationManagerProxy getAuthorizationManager() throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering getAuthorizationManager().");
        }
        
        deaf.auth.AuthorizationManager _result = null;
        try {
            _result = getAuthEngineReference().getAuthorizationManager();
        } catch(org.omg.CORBA.SystemException se) {
            logCat.error("CORBA SystemException: " + se.toString());
            System.out.println("getAuthorizationManager() failed with the following exception");
            se.printStackTrace(System.out);
        }

        return new AuthorizationManagerProxy(_result);
	}


    public deaf.auth.SessionManagerProxy getSessionManager() throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering getSessionManager().");
        }
        
        deaf.auth.SessionManager _result = null;
        try {
            _result = getAuthEngineReference().getSessionManager();
        } catch(org.omg.CORBA.SystemException se) {
            logCat.error("CORBA SystemException: " + se.toString());
            System.out.println("getSessionManager() failed with the following exception");
            se.printStackTrace(System.out);
        }

        return new SessionManagerProxy(_result);
	}


}



