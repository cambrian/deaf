/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: SessionEngineProxy.java,v 1.4 2001/08/17 06:10:15 hyoon Exp $ 
 */ 
package deaf.auth;

import  deaf.*;
import  org.apache.log4j.*;
import  org.omg.CORBA.SystemException;


public class SessionEngineProxy
    extends EngineProxy
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(SessionEngineProxy.class);
    }


    /**
     * Constructor.
     */
    public SessionEngineProxy()
    {
        super();
    }

    /**
     * Constructor.
     */
    public SessionEngineProxy(SessionEngine intf)
    {
        super(intf);
    }

    
    /**
     * Returns the reference to the CORBA object.
     */
    public SessionEngine getSessionEngineReference()
    {
        return SessionEngineHelper.narrow(getEngineReference());
    }


    public deaf.SessionObject getSessionObject(
        deaf.SessionToken token
    ) throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering getSessionObject(). SessionToken=" + toNonNullString(token));
        }
        
        deaf.SessionObject _result = null;
        try {
            _result = getSessionEngineReference().getSessionObject(token);
        } catch(org.omg.CORBA.SystemException se) {
            logCat.error("CORBA SystemException: " + se.toString());
            System.out.println("getSessionObject() failed with the following exception");
            se.printStackTrace(System.out);
        }

        return _result;
	}


}



