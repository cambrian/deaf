/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: AuthorizationManagerProxy.java,v 1.4 2001/08/17 06:10:15 hyoon Exp $ 
 */ 
package deaf.auth;

import  deaf.*;
import  org.apache.log4j.*;
import  org.omg.CORBA.SystemException;


public class AuthorizationManagerProxy
    extends ManagerProxy
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(AuthorizationManagerProxy.class);
    }


    /**
     * Constructor.
     */
    public AuthorizationManagerProxy()
    {
        super();
    }

    /**
     * Constructor.
     */
    public AuthorizationManagerProxy(AuthorizationManager intf)
    {
        super(intf);
    }

    
    /**
     * Returns the reference to the CORBA object.
     */
    public AuthorizationManager getAuthorizationManagerReference()
    {
        return AuthorizationManagerHelper.narrow(getManagerReference());
    }
    
}



