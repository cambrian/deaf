/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: DataEngineProxy.java,v 1.4 2001/08/17 06:10:15 hyoon Exp $ 
 */ 
package deaf.persist;

import  deaf.*;
import  org.apache.log4j.*;
import  org.omg.CORBA.SystemException;


public class DataEngineProxy
    extends EngineProxy
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(DataEngineProxy.class);
    }


    /**
     * Constructor.
     */
    public DataEngineProxy()
    {
        super();
    }

    /**
     * Constructor.
     */
    public DataEngineProxy(DataEngine intf)
    {
        super(intf);
    }

    
    /**
     * Returns the reference to the CORBA object.
     */
    public DataEngine getDataEngineReference()
    {
        return DataEngineHelper.narrow(getEngineReference());
    }


}



