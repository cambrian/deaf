/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: FileEngineProxy.java,v 1.4 2001/08/17 06:10:15 hyoon Exp $ 
 */ 
package deaf.persist;

import  deaf.*;
import  org.apache.log4j.*;
import  org.omg.CORBA.SystemException;


public class FileEngineProxy
    extends EngineProxy
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(FileEngineProxy.class);
    }


    /**
     * Constructor.
     */
    public FileEngineProxy()
    {
        super();
    }

    /**
     * Constructor.
     */
    public FileEngineProxy(FileEngine intf)
    {
        super(intf);
    }

    
    /**
     * Returns the reference to the CORBA object.
     */
    public FileEngine getFileEngineReference()
    {
        return FileEngineHelper.narrow(getEngineReference());
    }


}



