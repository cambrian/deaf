/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: DataManagerProxy.java,v 1.4 2001/08/17 06:10:15 hyoon Exp $ 
 */ 
package deaf.persist;

import  deaf.*;
import  org.apache.log4j.*;
import  org.omg.CORBA.SystemException;


public class DataManagerProxy
    extends ManagerProxy
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(DataManagerProxy.class);
    }


    /**
     * Constructor.
     */
    public DataManagerProxy()
    {
        super();
    }

    /**
     * Constructor.
     */
    public DataManagerProxy(DataManager intf)
    {
        super(intf);
    }

    
    /**
     * Returns the reference to the CORBA object.
     */
    public DataManager getDataManagerReference()
    {
        return DataManagerHelper.narrow(getManagerReference());
    }

}



