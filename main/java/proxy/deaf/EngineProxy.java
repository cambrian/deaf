/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: EngineProxy.java,v 1.4 2001/08/17 06:10:15 hyoon Exp $ 
 */ 
package deaf;

import  org.apache.log4j.*;
import  org.omg.CORBA.SystemException;


public class EngineProxy
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(EngineProxy.class);
    }
    static protected String toNonNullString(Object o)
    {
        return (o == null) ? "_nil" : o.toString();
    }

    
    //
    private Engine  m_engineRef = null;
    

    /**
     * Constructor.
     */
    public EngineProxy()
    {
    }

    /**
     * Constructor.
     */
    public EngineProxy(Engine intf)
    {
        m_engineRef = intf;
    }

    
    /**
     * Returns the reference to the CORBA object.
     */
    public Engine getEngineReference()
    {
        return m_engineRef;
    }



    public deaf.EngineHandle getHandle() throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering getHandle().");
        }
        
        deaf.EngineHandle _result = null;
        try {
            _result = m_engineRef.getHandle();
        }
        catch(org.omg.CORBA.SystemException se) {
            logCat.error("CORBA SystemException: " + se.toString());
            System.out.println("getHandle() failed with the following exception");
            se.printStackTrace(System.out);
        }

        return _result;
	}


    public deaf.ManagerProxy getManager() throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering getManager().");
        }
        
        deaf.Manager _result = null;
        try {
            _result = m_engineRef.getManager();
        }
        catch(org.omg.CORBA.SystemException se) {
            logCat.error("CORBA SystemException: " + se.toString());
            System.out.println("getManager() failed with the following exception");
            se.printStackTrace(System.out);
        }

        return new ManagerProxy(_result);
	}


    public boolean isIdentical(
        deaf.Engine engn
    ) throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering isIdentical(). Engine=" + toNonNullString(engn));
        }
        
        boolean _result = false;
        try {
            _result = m_engineRef.isIdentical(engn);
        }
        catch(org.omg.CORBA.SystemException se) {
            logCat.error("CORBA SystemException: " + se.toString());
            System.out.println("isIdentical() failed with the following exception");
            se.printStackTrace(System.out);
        }

        return _result;
	}


    public void remove(
        deaf.SessionToken token
    ) throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering remove(). SessionToken=" + toNonNullString(token));
        }
        
        try {
            m_engineRef.remove(token);
        }
        catch(org.omg.CORBA.SystemException se) {
            logCat.error("CORBA SystemException: " + se.toString());
            System.out.println("remove() failed with the following exception");
            se.printStackTrace(System.out);
        }
	}


    public void invoke(
        deaf.DynRequest req,
        deaf.DynResponseHolder res
    ) throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering invoke(). DynRequest=" + toNonNullString(req) + ";DynResponseHolder=" + toNonNullString(res));
        }
        
        try {
            m_engineRef.invoke(req, res);
        }
        catch(org.omg.CORBA.SystemException se) {
            logCat.error("CORBA SystemException: " + se.toString());
            System.out.println("invoke() failed with the following exception");
            se.printStackTrace(System.out);
        }
	}


}



