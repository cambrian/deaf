/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: ServiceProxy.java,v 1.4 2001/08/17 06:10:15 hyoon Exp $ 
 */ 
package deaf;

import  org.apache.log4j.*;
import  org.omg.CORBA.SystemException;


public class ServiceProxy
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(ServiceProxy.class);
    }
    static protected String toNonNullString(Object o)
    {
        return (o == null) ? "_nil" : o.toString();
    }

    //
    private Service  m_serviceRef = null;


    /**
     * Constructor.
     */
    public ServiceProxy()
    {

    }

    /**
     * Constructor.
     */
    public ServiceProxy(Service intf)
    {
        m_serviceRef = intf;
    }

    
    /**
     * Returns the reference to the CORBA object.
     */
    public Service getServiceReference()
    {
        return m_serviceRef;
    }


    public int version()
	{
        int _result = 0;
        _result = m_serviceRef.version();
        return _result;
	}

    public deaf.ServicePolicy policy()
	{
        deaf.ServicePolicy _result = null;
        _result = m_serviceRef.policy();
        return _result;
	}


}



