/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: ClientManagerProxy.java,v 1.4 2001/08/17 06:10:15 hyoon Exp $ 
 */ 
package deaf.client;

import  deaf.*;
import  org.apache.log4j.*;
import  org.omg.CORBA.SystemException;


public class ClientManagerProxy
    extends ManagerProxy
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(ClientManagerProxy.class);
    }


    /**
     * Constructor.
     */
    public ClientManagerProxy()
    {
        super();
    }

    /**
     * Constructor.
     */
    public ClientManagerProxy(ClientManager intf)
    {
        super(intf);
    }

    
    /**
     * Returns the reference to the CORBA object.
     */
    public ClientManager getClientManagerReference()
    {
        return ClientManagerHelper.narrow(getManagerReference());
    }


    public int version()
	{
        return 0;
	}


}



