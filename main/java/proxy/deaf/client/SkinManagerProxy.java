/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: SkinManagerProxy.java,v 1.4 2001/08/17 06:10:15 hyoon Exp $ 
 */ 
package deaf.client;

import deaf.*;

import  deaf.*;
import  org.apache.log4j.*;
import  org.omg.CORBA.SystemException;


public class SkinManagerProxy
    extends ManagerProxy
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(SkinManagerProxy.class);
    }


    /**
     * Constructor.
     */
    public SkinManagerProxy()
    {
        super();
    }

    /**
     * Constructor.
     */
    public SkinManagerProxy(SkinManager intf)
    {
        super(intf);
    }

    
    /**
     * Returns the reference to the CORBA object.
     */
    public SkinManager getSkinManagerReference()
    {
        return SkinManagerHelper.narrow(getManagerReference());
    }

}



