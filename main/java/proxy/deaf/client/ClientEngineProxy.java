/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: ClientEngineProxy.java,v 1.4 2001/08/17 06:10:15 hyoon Exp $ 
 */ 
package deaf.client;

import  deaf.*;
import  org.apache.log4j.*;
import  org.omg.CORBA.SystemException;


public class ClientEngineProxy
    extends EngineProxy
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(ClientEngineProxy.class);
    }


    /**
     * Constructor.
     */
    public ClientEngineProxy()
    {
        super();
    }

    /**
     * Constructor.
     */
    public ClientEngineProxy(ClientEngine intf)
    {
        super(intf);
    }

    
    /**
     * Returns the reference to the CORBA object.
     */
    public ClientEngine getClientEngineReference()
    {
        return ClientEngineHelper.narrow(getEngineReference());
    }


    public deaf.SessionToken login(
        deaf.client.LoginInfo info
    ) throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering login(). LoginInfo=" + toNonNullString(info));
        }
        
        deaf.SessionToken _result = null;
        try {
            _result = getClientEngineReference().login(info);
        } catch(org.omg.CORBA.SystemException se) {
            logCat.error("CORBA SystemException: " + se.toString());
            System.out.println("login() failed with the following exception");
            se.printStackTrace(System.out);
        }

        return _result;
	}


    public deaf.admin.PropManagerProxy getPropManager(
        deaf.SessionToken token
    ) throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering getPropManager(). SessionToken=" + toNonNullString(token));
        }
        
        deaf.admin.PropManager _result = null;
        try {
            _result = getClientEngineReference().getPropManager(token);
        } catch(org.omg.CORBA.SystemException se) {
            logCat.error("CORBA SystemException: " + se.toString());
            System.out.println("getPropManager() failed with the following exception");
            se.printStackTrace(System.out);
        }

        return new deaf.admin.PropManagerProxy(_result);
	}


    public deaf.client.SkinManagerProxy getSkinManager(
        deaf.SessionToken token
    ) throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering getSkinManager(). SessionToken=" + toNonNullString(token));
        }
        
        deaf.client.SkinManager _result = null;
        try {
            _result = getClientEngineReference().getSkinManager(token);
        } catch(org.omg.CORBA.SystemException se) {
            logCat.error("CORBA SystemException: " + se.toString());
            System.out.println("getSkinManager() failed with the following exception");
            se.printStackTrace(System.out);
        }

        return new deaf.client.SkinManagerProxy(_result);
	}


}



