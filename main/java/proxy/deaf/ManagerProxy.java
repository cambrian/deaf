/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: ManagerProxy.java,v 1.4 2001/08/17 06:10:15 hyoon Exp $ 
 */ 
package deaf;

import  org.apache.log4j.*;
import  org.omg.CORBA.SystemException;


public class ManagerProxy
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(ManagerProxy.class);
    }
    static protected String toNonNullString(Object o)
    {
        return (o == null) ? "_nil" : o.toString();
    }

    
    //
    private Manager  m_managerRef = null;
    

    /**
     * Constructor.
     */
    public ManagerProxy()
    {

    }

    /**
     * Constructor.
     */
    public ManagerProxy(Manager intf)
    {
        m_managerRef = intf;
    }

    
    /**
     * Returns the reference to the CORBA object.
     */
    public Manager getManagerReference()
    {
        return m_managerRef;
    }



    public deaf.ManagerPolicy policy()
	{
        deaf.ManagerPolicy _result = null;
        _result = m_managerRef.policy();
        return _result;
	}


    public deaf.ServiceProxy getService() throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering getService().");
        }
        
        deaf.Service _result = null;
        try {
            _result = m_managerRef.getService();
        }
        catch(org.omg.CORBA.SystemException se) {
            logCat.error("CORBA SystemException: " + se.toString());
            System.out.println("getService() failed with the following exception");
            se.printStackTrace(System.out);
        }

        return new ServiceProxy(_result);
	}


    public deaf.ManagerHandle getHandle() throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering getHandle().");
        }
        
        deaf.ManagerHandle _result = null;
        try {
            _result = m_managerRef.getHandle();
        }
        catch(org.omg.CORBA.SystemException se) {
            logCat.error("CORBA SystemException: " + se.toString());
            System.out.println("getHandle() failed with the following exception");
            se.printStackTrace(System.out);
        }

        return _result;
	}


    public deaf.EngineHandle createEngine(
        deaf.SessionToken token
    ) throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering createEngine().");
        }
        
        deaf.EngineHandle _result = null;
        try {
            _result = m_managerRef.createEngine(token);
        }
        catch(org.omg.CORBA.SystemException se) {
            logCat.error("CORBA SystemException: " + se.toString());
            System.out.println("createEngine() failed with the following exception");
            se.printStackTrace(System.out);
        }

        return _result;
	}


    public deaf.EngineProxy getEngine(
        deaf.SessionToken token,
        deaf.EngineHandle hand
    ) throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering getEngine().");
        }
        
        deaf.Engine _result = null;
        try {
            _result = m_managerRef.getEngine(token, hand);
        }
        catch(org.omg.CORBA.SystemException se) {
            logCat.error("CORBA SystemException: " + se.toString());
            System.out.println("getEngine() failed with the following exception");
            se.printStackTrace(System.out);
        }

        return new EngineProxy(_result);
	}


    public deaf.EngineHandle[] getEngineList(
        deaf.SessionToken token
    ) throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering getEngineList().");
        }
        
        deaf.EngineHandle[] _result = null;
        try {
            _result = m_managerRef.getEngineList(token);
        }
        catch(org.omg.CORBA.SystemException se) {
            logCat.error("CORBA SystemException: " + se.toString());
            System.out.println("getEngineList() failed with the following exception");
            se.printStackTrace(System.out);
        }

        return _result;
	}


    public void removeEngine(
        deaf.SessionToken token,
        deaf.EngineHandle hand
    ) throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering removeEngine().");
        }
        
        try {
            m_managerRef.removeEngine(token, hand);
        }
        catch(org.omg.CORBA.SystemException se) {
            logCat.error("CORBA SystemException: " + se.toString());
            System.out.println("removeEngine() failed with the following exception");
            se.printStackTrace(System.out);
        }
	}


}



