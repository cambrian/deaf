/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: AppServiceProxy.java,v 1.4 2001/08/17 06:10:15 hyoon Exp $ 
 */ 
package deaf;

import  org.apache.log4j.*;
import  org.omg.CORBA.SystemException;


public class AppServiceProxy
    extends ServiceProxy
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(AppServiceProxy.class);
    }
    

    /**
     * Constructor.
     */
    public AppServiceProxy()
    {
        super();
    }

    /**
     * Constructor.
     */
    public AppServiceProxy(AppService intf)
    {
        super(intf);
    }

    
    /**
     * Returns the reference to the CORBA object.
     */
    public AppService getAppServiceReference()
    {
        return AppServiceHelper.narrow(getServiceReference());
    }


    public deaf.app.AppManagerProxy getAppManager() throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering getAppManager().");
        }
        
        deaf.app.AppManager _result = null;
        try {
            _result = getAppServiceReference().getAppManager();
        } catch(org.omg.CORBA.SystemException se) {
            logCat.error("CORBA SystemException: " + se.toString());
            System.out.println("getAppManager() failed with the following exception");
            se.printStackTrace(System.out);
        }

        return new deaf.app.AppManagerProxy(_result);
	}

}



