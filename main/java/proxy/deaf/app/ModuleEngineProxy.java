/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: ModuleEngineProxy.java,v 1.4 2001/08/17 06:10:15 hyoon Exp $ 
 */ 
package deaf.app;

import  deaf.*;
import  org.apache.log4j.*;
import  org.omg.CORBA.SystemException;


public class ModuleEngineProxy
    extends EngineProxy
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(ModuleEngineProxy.class);
    }


    /**
     * Constructor.
     */
    public ModuleEngineProxy()
    {
        super();
    }

    /**
     * Constructor.
     */
    public ModuleEngineProxy(ModuleEngine intf)
    {
        super(intf);
    }

    
    /**
     * Returns the reference to the CORBA object.
     */
    public ModuleEngine getModuleEngineReference()
    {
        return ModuleEngineHelper.narrow(getEngineReference());
    }


    public deaf.admin.AdminManagerProxy getAdminManager() throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering getAdminManager().");
        }
        
        deaf.admin.AdminManager _result = null;
        try {
            _result = getModuleEngineReference().getAdminManager();
        } catch(org.omg.CORBA.SystemException se) {
            logCat.error("CORBA SystemException: " + se.toString());
            System.out.println("getAdminManager() failed with the following exception");
            se.printStackTrace(System.out);
        }

        return new deaf.admin.AdminManagerProxy(_result);
	}


    public deaf.ManagerHandle[] getManagerList(
        deaf.SessionToken token
    ) throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering getManagerList(). SessionToken=" + toNonNullString(token));
        }
        
        deaf.ManagerHandle[] _result = null;
        try {
            _result = getModuleEngineReference().getManagerList(token);
        } catch(org.omg.CORBA.SystemException se) {
            logCat.error("CORBA SystemException: " + se.toString());
            System.out.println("getManagerList() failed with the following exception");
            se.printStackTrace(System.out);
        }

        return _result;
	}


}



