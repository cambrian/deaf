/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: ConfigEngineProxy.java,v 1.4 2001/08/17 06:10:15 hyoon Exp $ 
 */ 
package deaf.app;

import  deaf.*;
import  org.apache.log4j.*;
import  org.omg.CORBA.SystemException;


public class ConfigEngineProxy
    extends EngineProxy
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(ConfigEngineProxy.class);
    }


    /**
     * Constructor.
     */
    public ConfigEngineProxy()
    {
        super();
    }

    /**
     * Constructor.
     */
    public ConfigEngineProxy(ConfigEngine intf)
    {
        super(intf);
    }

    
    /**
     * Returns the reference to the CORBA object.
     */
    public ConfigEngine getConfigEngineReference()
    {
        return ConfigEngineHelper.narrow(getEngineReference());
    }


    public deaf.admin.PropManagerProxy getPropManager() throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering getPropManager().");
        }
        
        deaf.admin.PropManager _result = null;
        try {
            _result = getConfigEngineReference().getPropManager();
        } catch(org.omg.CORBA.SystemException se) {
            logCat.error("CORBA SystemException: " + se.toString());
            System.out.println("getPropManager() failed with the following exception");
            se.printStackTrace(System.out);
        }

        return new deaf.admin.PropManagerProxy(_result);
	}


    public deaf.ManagerHandle[] listAllModules(
        deaf.SessionToken token
    ) throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering listAllModules(). SessionToken=" + toNonNullString(token));
        }
        
        deaf.ManagerHandle[] _result = null;
        try {
            _result = getConfigEngineReference().listAllModules(token);
        } catch(org.omg.CORBA.SystemException se) {
            logCat.error("CORBA SystemException: " + se.toString());
            System.out.println("listAllModules() failed with the following exception");
            se.printStackTrace(System.out);
        }

        return _result;
	}


    public void registerModule(
        deaf.SessionToken token,
        deaf.ManagerHandle hand
    ) throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering registerModule(). SessionToken=" + toNonNullString(token) + "ManagerHandle=" + toNonNullString(hand));
        }
        
        try {
            getConfigEngineReference().registerModule(token, hand);
        } catch(org.omg.CORBA.SystemException se) {
            logCat.error("CORBA SystemException: " + se.toString());
            System.out.println("registerModule() failed with the following exception");
            se.printStackTrace(System.out);
        }
	}


    public void unregisterModule(
        deaf.SessionToken token,
        deaf.ManagerHandle hand
    ) throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering unregisterModule(). SessionToken=" + toNonNullString(token) + "ManagerHandle=" + toNonNullString(hand));
        }
        
        try {
            getConfigEngineReference().unregisterModule(token, hand);
        } catch(org.omg.CORBA.SystemException se) {
            logCat.error("CORBA SystemException: " + se.toString());
            System.out.println("unregisterModule() failed with the following exception");
            se.printStackTrace(System.out);
        }
	}


}



