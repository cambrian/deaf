/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: AppManagerProxy.java,v 1.4 2001/08/17 06:10:15 hyoon Exp $ 
 */ 
package deaf.app;

import  deaf.*;
import  org.apache.log4j.*;
import  org.omg.CORBA.SystemException;


public class AppManagerProxy
    extends ManagerProxy
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(AppManagerProxy.class);
    }


    /**
     * Constructor.
     */
    public AppManagerProxy()
    {
        super();
    }

    /**
     * Constructor.
     */
    public AppManagerProxy(AppManager intf)
    {
        super(intf);
    }

    
    /**
     * Returns the reference to the CORBA object.
     */
    public AppManager getAppManagerReference()
    {
        return AppManagerHelper.narrow(getManagerReference());
    }

    public int version()
	{
        int _result = 0;
        _result = getAppManagerReference().version();
        return _result;
	}


}



