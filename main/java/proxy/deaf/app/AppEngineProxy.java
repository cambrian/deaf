/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: AppEngineProxy.java,v 1.4 2001/08/17 06:10:15 hyoon Exp $ 
 */ 
package deaf.app;

import  deaf.*;
import  org.apache.log4j.*;
import  org.omg.CORBA.SystemException;


public class AppEngineProxy
    extends EngineProxy
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(AppEngineProxy.class);
    }


    /**
     * Constructor.
     */
    public AppEngineProxy()
    {
        super();
    }

    /**
     * Constructor.
     */
    public AppEngineProxy(AppEngine intf)
    {
        super(intf);
    }

    
    /**
     * Returns the reference to the CORBA object.
     */
    public AppEngine getAppEngineReference()
    {
        return AppEngineHelper.narrow(getEngineReference());
    }


    public deaf.app.ConfigManagerProxy getConfigManager() throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering getConfigManager().");
        }
        
        deaf.app.ConfigManager _result = null;
        try {
            _result = getAppEngineReference().getConfigManager();
        } catch(org.omg.CORBA.SystemException se) {
            logCat.error("CORBA SystemException: " + se.toString());
            System.out.println("getConfigManager() failed with the following exception");
            se.printStackTrace(System.out);
        }

        return new ConfigManagerProxy(_result);
	}


    public deaf.admin.LocatorManagerProxy getLocatorManager() throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering getLocatorManager().");
        }
        
        deaf.admin.LocatorManager _result = null;
        try {
            _result = getAppEngineReference().getLocatorManager();
        } catch(org.omg.CORBA.SystemException se) {
            logCat.error("CORBA SystemException: " + se.toString());
            System.out.println("getLocatorManager() failed with the following exception");
            se.printStackTrace(System.out);
        }

        return new deaf.admin.LocatorManagerProxy(_result);
	}


    public deaf.client.ClientManagerProxy getDefaultClient() throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering getDefaultClient().");
        }
        
        deaf.client.ClientManager _result = null;
        try {
            _result = getAppEngineReference().getDefaultClient();
        } catch(org.omg.CORBA.SystemException se) {
            logCat.error("CORBA SystemException: " + se.toString());
            System.out.println("getDefaultClient() failed with the following exception");
            se.printStackTrace(System.out);
        }

        return new deaf.client.ClientManagerProxy(_result);
	}


    public deaf.client.ClientManagerProxy getClient(
        deaf.ManagerHandle hand
    ) throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering getClient(). ManagerHandle=" + toNonNullString(hand));
        }
        
        deaf.client.ClientManager _result = null;
        try {
            _result = getAppEngineReference().getClient(hand);
        } catch(org.omg.CORBA.SystemException se) {
            logCat.error("CORBA SystemException: " + se.toString());
            System.out.println("getClient() failed with the following exception");
            se.printStackTrace(System.out);
        }

        return new deaf.client.ClientManagerProxy(_result);
	}


    public deaf.ManagerHandle[] getClientList() throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering getClientList().");
        }
        
        deaf.ManagerHandle[] _result = null;
        try {
            _result = getAppEngineReference().getClientList();
        } catch(org.omg.CORBA.SystemException se) {
            logCat.error("CORBA SystemException: " + se.toString());
            System.out.println("getClientList() failed with the following exception");
            se.printStackTrace(System.out);
        }

        return _result;
	}


    public deaf.app.ModuleManagerProxy getModule(
        deaf.SessionToken token,
        deaf.ManagerHandle hand
    ) throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering getModule(). SessionToken=" + toNonNullString(token) + "; ManagerHandle=" + toNonNullString(hand));
        }
        
        deaf.app.ModuleManager _result = null;
        try {
            _result = getAppEngineReference().getModule(token, hand);
        } catch(org.omg.CORBA.SystemException se) {
            logCat.error("CORBA SystemException: " + se.toString());
            System.out.println("getModule() failed with the following exception");
            se.printStackTrace(System.out);
        }

        return new ModuleManagerProxy(_result);
	}


    public deaf.ManagerHandle[] getModuleList(
        deaf.SessionToken token
    ) throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering getModuleList(). SessionToken=" + toNonNullString(token));
        }
        
        deaf.ManagerHandle[] _result = null;
        try {
            _result = getAppEngineReference().getModuleList(token);
        } catch(org.omg.CORBA.SystemException se) {
            logCat.error("CORBA SystemException: " + se.toString());
            System.out.println("getModuleList() failed with the following exception");
            se.printStackTrace(System.out);
        }

        return _result;
	}


    public void startModule(
        deaf.SessionToken token,
        deaf.ManagerHandle hand
    ) throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering startModule(). SessionToken=" + toNonNullString(token) + "; ManagerHandle=" + toNonNullString(hand));
        }
        
        try {
            getAppEngineReference().startModule(token, hand);
        } catch(org.omg.CORBA.SystemException se) {
            logCat.error("CORBA SystemException: " + se.toString());
            System.out.println("startModule() failed with the following exception");
            se.printStackTrace(System.out);
        }
	}


    public void shutdownAllModules(
        deaf.SessionToken token
    ) throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering shutdownAllModules(). SessionToken=" + toNonNullString(token));
        }
        
        try {
            getAppEngineReference().shutdownAllModules(token);
        } catch(org.omg.CORBA.SystemException se) {
            logCat.error("CORBA SystemException: " + se.toString());
            System.out.println("shutdownAllModules() failed with the following exception");
            se.printStackTrace(System.out);
        }
	}


    public void shutdownModule(
        deaf.SessionToken token,
        deaf.ManagerHandle hand
    ) throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering shutdownModule(). SessionToken=" + toNonNullString(token) + "; ManagerHandle=" + toNonNullString(hand));
        }
        
        try {
            getAppEngineReference().shutdownModule(token, hand);
        } catch(org.omg.CORBA.SystemException se) {
            logCat.error("CORBA SystemException: " + se.toString());
            System.out.println("shutdownModule() failed with the following exception");
            se.printStackTrace(System.out);
        }
	}


}



