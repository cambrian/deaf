/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: ModuleManagerProxy.java,v 1.4 2001/08/17 06:10:15 hyoon Exp $ 
 */ 
package deaf.app;

import  deaf.*;
import  org.apache.log4j.*;
import  org.omg.CORBA.SystemException;


public class ModuleManagerProxy
    extends ManagerProxy
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(ModuleManagerProxy.class);
    }


    /**
     * Constructor.
     */
    public ModuleManagerProxy()
    {
        super();
    }

    /**
     * Constructor.
     */
    public ModuleManagerProxy(ModuleManager intf)
    {
        super(intf);
    }

    
    /**
     * Returns the reference to the CORBA object.
     */
    public ModuleManager getModuleManagerReference()
    {
        return ModuleManagerHelper.narrow(getManagerReference());
    }


    public int version()
	{
        int _result = 0;
        _result = getModuleManagerReference().version();
        return _result;
	}


}



