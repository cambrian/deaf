/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: PropEngineProxy.java,v 1.4 2001/08/17 06:10:15 hyoon Exp $ 
 */ 
package deaf.admin;

import  deaf.*;
import  org.apache.log4j.*;
import  org.omg.CORBA.SystemException;


public class PropEngineProxy
    extends EngineProxy
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(PropEngineProxy.class);
    }


    /**
     * Constructor.
     */
    public PropEngineProxy()
    {
        super();
    }

    /**
     * Constructor.
     */
    public PropEngineProxy(PropEngine intf)
    {
        super(intf);
    }

    
    /**
     * Returns the reference to the CORBA object.
     */
    public PropEngine getPropEngineReference()
    {
        return PropEngineHelper.narrow(getEngineReference());
    }


    public deaf.Property getProperty(
        java.lang.String name
    ) throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering getProperty(). String=" + toNonNullString(name));
        }
        
        deaf.Property _result = null;
        try {
            _result = getPropEngineReference().getProperty(name);
        } catch(org.omg.CORBA.SystemException se) {
            logCat.error("CORBA SystemException: " + se.toString());
            System.out.println("getProperty() failed with the following exception");
            se.printStackTrace(System.out);
        }

        return _result;
	}


    public java.lang.String getPropertyValue(
        java.lang.String name
    ) throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering getPropertyValue(). String=" + toNonNullString(name));
        }
        
        String _result = null;
        try {
            _result = getPropEngineReference().getPropertyValue(name);
        } catch(org.omg.CORBA.SystemException se) {
            logCat.error("CORBA SystemException: " + se.toString());
            System.out.println("getPropertyValue() failed with the following exception");
            se.printStackTrace(System.out);
        }

        return _result;
	}


    public void setProperty(
        deaf.Property prop
    ) throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering getProperty(). Property=" + toNonNullString(prop));
        }
        
        try {
            getPropEngineReference().setProperty(prop);
        } catch(org.omg.CORBA.SystemException se) {
            logCat.error("CORBA SystemException: " + se.toString());
            System.out.println("setProperty() failed with the following exception");
            se.printStackTrace(System.out);
        }
	}


}



