/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: LogEngineProxy.java,v 1.4 2001/08/17 06:10:15 hyoon Exp $ 
 */ 
package deaf.admin;

import  deaf.*;
import  org.apache.log4j.*;
import  org.omg.CORBA.SystemException;


public class LogEngineProxy
    extends EngineProxy
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(LogEngineProxy.class);
    }


    /**
     * Constructor.
     */
    public LogEngineProxy()
    {
        super();
    }

    /**
     * Constructor.
     */
    public LogEngineProxy(LogEngine intf)
    {
        super(intf);
    }

    
    /**
     * Returns the reference to the CORBA object.
     */
    public LogEngine getLogEngineReference()
    {
        return LogEngineHelper.narrow(getEngineReference());
    }


    public java.lang.String locale()
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering locale().");
        }
        
        String _result = null;
        _result = getLogEngineReference().locale();
        return _result;
	}


    public void locale(
        java.lang.String _val
    )
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering locale(). String=" + toNonNullString(_val));
        }
        
        getLogEngineReference().locale(_val);
	}


    public java.lang.String format()
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering format().");
        }
        
        String _result = null;
        _result = getLogEngineReference().format();
        return _result;
	}


    public void format(
        java.lang.String _val
    )
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering format(). String=" + toNonNullString(_val));
        }
        
        getLogEngineReference().format(_val);
	}


    public int defaultLevel()
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering defaultLevel().");
        }
        
        int _result = 0;
        _result = getLogEngineReference().defaultLevel();
        return _result;
	}


    public void defaultLevel(
        int _val
    )
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering defaultLevel(). String=" + _val);
        }
        
        getLogEngineReference().defaultLevel(_val);
	}


    public void writeMessage(
        int level,
        java.lang.String message
    ) throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering writeMessage(). level=" + level + "; message=" + toNonNullString(message));
        }
        
        try {
            getLogEngineReference().writeMessage(level, message);
        } catch(org.omg.CORBA.SystemException se) {
            logCat.error("CORBA SystemException: " + se.toString());
            System.out.println("getModuleManager() failed with the following exception");
            se.printStackTrace(System.out);
        }
	}


}



