/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: AdminEngineProxy.java,v 1.4 2001/08/17 06:10:15 hyoon Exp $ 
 */ 
package deaf.admin;

import  deaf.*;
import  org.apache.log4j.*;
import  org.omg.CORBA.SystemException;


public class AdminEngineProxy
    extends EngineProxy
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(AdminEngineProxy.class);
    }


    /**
     * Constructor.
     */
    public AdminEngineProxy()
    {
        super();
    }

    /**
     * Constructor.
     */
    public AdminEngineProxy(AdminEngine intf)
    {
        super(intf);
    }

    
    /**
     * Returns the reference to the CORBA object.
     */
    public AdminEngine getAdminEngineReference()
    {
        return AdminEngineHelper.narrow(getEngineReference());
    }


    public deaf.app.ModuleManagerProxy getModuleManager(
        deaf.SessionToken token
    ) throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering getModuleManager().");
        }
        
        deaf.app.ModuleManager _result = null;
        try {
            _result = getAdminEngineReference().getModuleManager(token);
        } catch(org.omg.CORBA.SystemException se) {
            logCat.error("CORBA SystemException: " + se.toString());
            System.out.println("getModuleManager() failed with the following exception");
            se.printStackTrace(System.out);
        }

        return new deaf.app.ModuleManagerProxy(_result);
	}


    public deaf.admin.LocatorManagerProxy getLocatorManager(
        deaf.SessionToken token
    ) throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering getLocatorManager().");
        }
        
        deaf.admin.LocatorManager _result = null;
        try {
            _result = getAdminEngineReference().getLocatorManager(token);
        } catch(org.omg.CORBA.SystemException se) {
            logCat.error("CORBA SystemException: " + se.toString());
            System.out.println("getLocatorManager() failed with the following exception");
            se.printStackTrace(System.out);
        }

        return new LocatorManagerProxy(_result);
	}


    public deaf.admin.PropManagerProxy getPropManager(
        deaf.SessionToken token
    ) throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering getPropManager().");
        }
        
        deaf.admin.PropManager _result = null;
        try {
            _result = getAdminEngineReference().getPropManager(token);
        } catch(org.omg.CORBA.SystemException se) {
            logCat.error("CORBA SystemException: " + se.toString());
            System.out.println("getPropManager() failed with the following exception");
            se.printStackTrace(System.out);
        }

        return new PropManagerProxy(_result);
	}


    public deaf.admin.LogManagerProxy getLogManager(
        deaf.SessionToken token
    ) throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering getLogManager().");
        }
        
        deaf.admin.LogManager _result = null;
        try {
            _result = getAdminEngineReference().getLogManager(token);
        } catch(org.omg.CORBA.SystemException se) {
            logCat.error("CORBA SystemException: " + se.toString());
            System.out.println("getLogManager() failed with the following exception");
            se.printStackTrace(System.out);
        }

        return new LogManagerProxy(_result);
	}


    public void startManager(
        deaf.SessionToken token,
        deaf.ManagerHandle hand
    ) throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering startManager().");
        }
        
        try {
            getAdminEngineReference().startManager(token, hand);
        } catch(org.omg.CORBA.SystemException se) {
            logCat.error("CORBA SystemException: " + se.toString());
            System.out.println("startManager() failed with the following exception");
            se.printStackTrace(System.out);
        }
	}


    public void shutdownAllManagers(
        deaf.SessionToken token
    ) throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering shutdownAllManagers().");
        }
        
        try {
            getAdminEngineReference().shutdownAllManagers(token);
        } catch(org.omg.CORBA.SystemException se) {
            logCat.error("CORBA SystemException: " + se.toString());
            System.out.println("shutdownAllManagers() failed with the following exception");
            se.printStackTrace(System.out);
        }
	}


    public void shutdownManager(
        deaf.SessionToken token,
        deaf.ManagerHandle hand
    ) throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering shutdownManager().");
        }
        
        try {
            getAdminEngineReference().shutdownManager(token, hand);
        } catch(org.omg.CORBA.SystemException se) {
            logCat.error("CORBA SystemException: " + se.toString());
            System.out.println("shutdownManager() failed with the following exception");
            se.printStackTrace(System.out);
        }
	}


}



