/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: LocatorEngineProxy.java,v 1.4 2001/08/17 06:10:15 hyoon Exp $ 
 */ 
package deaf.admin;

import  deaf.*;
import  org.apache.log4j.*;
import  org.omg.CORBA.SystemException;


public class LocatorEngineProxy
    extends EngineProxy
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(LocatorEngineProxy.class);
    }


    /**
     * Constructor.
     */
    public LocatorEngineProxy()
    {
        super();
    }

    /**
     * Constructor.
     */
    public LocatorEngineProxy(LocatorEngine intf)
    {
        super(intf);
    }

    
    /**
     * Returns the reference to the CORBA object.
     */
    public LocatorEngine getLocatorEngineReference()
    {
        return LocatorEngineHelper.narrow(getEngineReference());
    }


    public deaf.ManagerHandle findManagerByRef(
        deaf.Manager ref
    ) throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering findManagerByRef().");
        }
        
        deaf.ManagerHandle _result = null;
        try {
            _result = getLocatorEngineReference().findManagerByRef(ref);
        } catch(org.omg.CORBA.SystemException se) {
            logCat.error("CORBA SystemException: " + se.toString());
            System.out.println("findManagerByRef() failed with the following exception");
            se.printStackTrace(System.out);
        }

        return _result;
	}


    public deaf.ManagerHandle findManagerByName(
        java.lang.String name
    ) throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering findManagerByName().");
        }
        
        deaf.ManagerHandle _result = null;
        try {
            _result = getLocatorEngineReference().findManagerByName(name);
        } catch(org.omg.CORBA.SystemException se) {
            logCat.error("CORBA SystemException: " + se.toString());
            System.out.println("findManagerByName() failed with the following exception");
            se.printStackTrace(System.out);
        }

        return _result;
	}


}



