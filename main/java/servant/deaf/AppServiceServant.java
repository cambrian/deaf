/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: AppServiceServant.java,v 1.4 2001/08/17 06:10:15 hyoon Exp $ 
 */ 
package deaf;

import org.omg.CORBA.*;
import org.omg.PortableServer.*;


public class AppServiceServant
    extends AppServicePOA
{
    org.omg.PortableServer.POA m_poa = null;

    private  AppServiceImpl   m_appServiceImpl = null;


    /**
     * Returns the server-side implementation object.
     *
     * @return AppServiceImpl  the delegated object.
     */
    public AppServiceImpl getAppServiceImpl()
    {
        return m_appServiceImpl;
    }

    /**
     * Returns the server-side implementation object.
     *
     * @return ServiceImpl  the delegated object.
     */
    public ServiceImpl getServiceImpl()
    {
        return m_appServiceImpl;
    }

    /**
    * Constructor of AppServiceServant
    *
    */
    public AppServiceServant()
    {
        this((POA) null);
    }

    /**
    * Constructor of AppServiceServant
    *
    */
    public AppServiceServant(AppServiceImpl an_impl) 
    {
        this(null, an_impl);
    }

    /**
    * Constructor of AppServiceServant
    *
    */
    public AppServiceServant(POA the_poa)
    {
        this(the_poa, new AppServiceImpl());
    }

    /**
    * Constructor of AppServiceServant
    *
    */
    public AppServiceServant(POA the_poa, AppServiceImpl an_impl) 
    {
        m_poa = the_poa;
        m_appServiceImpl = an_impl;
    }



   public deaf.app.AppManager getAppManager(
   )
   throws org.omg.CORBA.SystemException,
    deaf.AppException
    {
       deaf.app.AppManager       _result;
       _result =  null;

       _result = getAppServiceImpl().getAppManager().getAppManagerReference();

        return _result;
   }


   public int version()
    {
        int                             _result;

       _result = getAppServiceImpl().version();

        return _result;
   }


   public deaf.ServicePolicy policy()
    {
        deaf.ServicePolicy        _result;

       _result = getAppServiceImpl().policy();

        return _result;
   }

    /**
    * Overrides the inherited _default_POA
    * The inherited _default_POA returns the root POA, by overriding it the correct POA is returned.<br>
    */
    public org.omg.PortableServer.POA _default_POA()
    {
        return m_poa;
    }

}



