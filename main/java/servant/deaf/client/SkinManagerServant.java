/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: SkinManagerServant.java,v 1.4 2001/08/17 06:10:16 hyoon Exp $ 
 */ 
package deaf.client;

import deaf.*;

import org.omg.CORBA.*;
import org.omg.PortableServer.*;


public class SkinManagerServant
    extends SkinManagerPOA
{
    org.omg.PortableServer.POA m_poa = null;

    private  SkinManagerImpl   m_skinManagerImpl = null;


    /**
     * Returns the server-side implementation object.
     *
     * @return SkinManagerImpl  the delegated object.
     */
    public SkinManagerImpl getSkinManagerImpl()
    {
        return m_skinManagerImpl;
    }

    /**
     * Returns the server-side implementation object.
     *
     * @return ManagerImpl  the delegated object.
     */
    public ManagerImpl getManagerImpl()
    {
        return m_skinManagerImpl;
    }

    /**
    * Constructor of SkinManagerServant
    *
    */
    public SkinManagerServant()
    {
        this((POA) null);
    }

    /**
    * Constructor of SkinManagerServant
    *
    */
    public SkinManagerServant(SkinManagerImpl an_impl) 
    {
        this(null, an_impl);
    }

    /**
    * Constructor of SkinManagerServant
    *
    */
    public SkinManagerServant(POA the_poa)
    {
        this(the_poa, new SkinManagerImpl());
    }

    /**
    * Constructor of SkinManagerServant
    *
    */
    public SkinManagerServant(POA the_poa, SkinManagerImpl an_impl) 
    {
        m_poa = the_poa;
        m_skinManagerImpl = an_impl;
    }



    public deaf.Service getService()
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.Service              _result;
        _result = getSkinManagerImpl().getService().getServiceReference();
        return _result;
    }
    public deaf.ManagerHandle getHandle()
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.ManagerHandle        _result;
        _result = getSkinManagerImpl().getHandle();
        return _result;
    }
    public deaf.EngineHandle createEngine(deaf.SessionToken token)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.EngineHandle         _result;
        _result = getSkinManagerImpl().createEngine(token);
        return _result;
    }
    public deaf.Engine getEngine(deaf.SessionToken token, deaf.EngineHandle hand)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.Engine               _result;
        _result = getSkinManagerImpl().getEngine(token, hand).getEngineReference();
        return _result;
    }
    public deaf.EngineHandle[] getEngineList(deaf.SessionToken token)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.EngineHandle[]       _result;
        _result = getSkinManagerImpl().getEngineList(token);
        return _result;
    }
    public void removeEngine(deaf.SessionToken token, deaf.EngineHandle hand)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getSkinManagerImpl().removeEngine(token, hand);
    }
    public deaf.ManagerPolicy policy()
    {
        deaf.ManagerPolicy        _result;
        _result = getSkinManagerImpl().policy();
        return _result;
    }

    /**
    * Overrides the inherited _default_POA
    * The inherited _default_POA returns the root POA, by overriding it the correct POA is returned.<br>
    */
    public org.omg.PortableServer.POA _default_POA()
    {
        return m_poa;
    }

}



