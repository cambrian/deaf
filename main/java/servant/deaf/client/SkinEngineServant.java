/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: SkinEngineServant.java,v 1.4 2001/08/17 06:10:16 hyoon Exp $ 
 */ 
package deaf.client;

import deaf.*;

import org.omg.CORBA.*;
import org.omg.PortableServer.*;


public class SkinEngineServant
    extends SkinEnginePOA
{
    org.omg.PortableServer.POA m_poa = null;

    private  SkinEngineImpl   m_skinEngineImpl = null;


    /**
     * Returns the server-side implementation object.
     *
     * @return SkinEngineImpl  the delegated object.
     */
    public SkinEngineImpl getSkinEngineImpl()
    {
        return m_skinEngineImpl;
    }

    /**
     * Returns the server-side implementation object.
     *
     * @return EngineImpl  the delegated object.
     */
    public EngineImpl getEngineImpl()
    {
        return m_skinEngineImpl;
    }

    /**
    * Constructor of SkinEngineServant
    *
    */
    public SkinEngineServant()
    {
        this((POA) null);
    }

    /**
    * Constructor of SkinEngineServant
    *
    */
    public SkinEngineServant(SkinEngineImpl an_impl) 
    {
        this(null, an_impl);
    }

    /**
    * Constructor of SkinEngineServant
    *
    */
    public SkinEngineServant(POA the_poa)
    {
        this(the_poa, new SkinEngineImpl());
    }

    /**
    * Constructor of SkinEngineServant
    *
    */
    public SkinEngineServant(POA the_poa, SkinEngineImpl an_impl) 
    {
        m_poa = the_poa;
        m_skinEngineImpl = an_impl;
    }


    public deaf.EngineHandle getHandle()
       throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.EngineHandle         _result;
        _result = getSkinEngineImpl().getHandle();
        return _result;
    }
    public deaf.Manager getManager()
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.Manager              _result;
        _result = getSkinEngineImpl().getManager().getManagerReference();
        return _result;
    }
    public boolean isIdentical(deaf.Engine engn)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        boolean  _result;
        _result = getSkinEngineImpl().isIdentical(engn);
        return _result;
    }
    public void remove(deaf.SessionToken token)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getSkinEngineImpl().remove(token);
    }
    public void invoke(deaf.DynRequest req, deaf.DynResponseHolder res)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getSkinEngineImpl().invoke(req, res);
    }


    public deaf.client.Layout getDefaultLayout()
       throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.client.Layout        _result;
        _result = getSkinEngineImpl().getDefaultLayout();
        return _result;
    }
    public deaf.client.Layout getLayout(deaf.SessionToken token)
       throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.client.Layout        _result;
        _result = getSkinEngineImpl().getLayout(token);
        return _result;
    }


    /**
    * Overrides the inherited _default_POA
    * The inherited _default_POA returns the root POA, by overriding it the correct POA is returned.<br>
    */
    public org.omg.PortableServer.POA _default_POA()
    {
        return m_poa;
    }

}



