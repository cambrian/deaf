/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: ClientManagerServant.java,v 1.4 2001/08/17 06:10:16 hyoon Exp $ 
 */ 
package deaf.client;

import deaf.*;

import org.omg.CORBA.*;
import org.omg.PortableServer.*;


public class ClientManagerServant
    extends ClientManagerPOA
{
    org.omg.PortableServer.POA m_poa = null;

    private  ClientManagerImpl   m_clientManagerImpl = null;


    /**
     * Returns the server-side implementation object.
     *
     * @return ClientManagerImpl  the delegated object.
     */
    public ClientManagerImpl getClientManagerImpl()
    {
        return m_clientManagerImpl;
    }

    /**
     * Returns the server-side implementation object.
     *
     * @return ManagerImpl  the delegated object.
     */
    public ManagerImpl getManagerImpl()
    {
        return m_clientManagerImpl;
    }

    /**
    * Constructor of ClientManagerServant
    *
    */
    public ClientManagerServant()
    {
        this((POA) null);
    }

    /**
    * Constructor of ClientManagerServant
    *
    */
    public ClientManagerServant(ClientManagerImpl an_impl) 
    {
        this(null, an_impl);
    }

    /**
    * Constructor of ClientManagerServant
    *
    */
    public ClientManagerServant(POA the_poa)
    {
        this(the_poa, new ClientManagerImpl());
    }

    /**
    * Constructor of ClientManagerServant
    *
    */
    public ClientManagerServant(POA the_poa, ClientManagerImpl an_impl) 
    {
        m_poa = the_poa;
        m_clientManagerImpl = an_impl;
    }


    public deaf.Service getService()
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.Service              _result;
        _result = getClientManagerImpl().getService().getServiceReference();
        return _result;
    }
    public deaf.ManagerHandle getHandle()
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.ManagerHandle        _result;
        _result = getClientManagerImpl().getHandle();
        return _result;
    }
    public deaf.EngineHandle createEngine(deaf.SessionToken token)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.EngineHandle         _result;
        _result = getClientManagerImpl().createEngine(token);
        return _result;
    }
    public deaf.Engine getEngine(deaf.SessionToken token, deaf.EngineHandle hand)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.Engine               _result;
        _result = getClientManagerImpl().getEngine(token, hand).getEngineReference();
        return _result;
    }
    public deaf.EngineHandle[] getEngineList(deaf.SessionToken token)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.EngineHandle[]       _result;
        _result = getClientManagerImpl().getEngineList(token);
        return _result;
    }
    public void removeEngine(deaf.SessionToken token, deaf.EngineHandle hand)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getClientManagerImpl().removeEngine(token, hand);
    }
    public deaf.ManagerPolicy policy()
    {
        deaf.ManagerPolicy        _result;
        _result = getClientManagerImpl().policy();
        return _result;
    }

    public int version()
    {
        int                             _result;
        _result = getClientManagerImpl().version();
        return _result;
    }

    /**
    * Overrides the inherited _default_POA
    * The inherited _default_POA returns the root POA, by overriding it the correct POA is returned.<br>
    */
    public org.omg.PortableServer.POA _default_POA()
    {
        return m_poa;
    }

}



