/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: ClientEngineServant.java,v 1.4 2001/08/17 06:10:16 hyoon Exp $ 
 */ 
package deaf.client;

import deaf.*;

import org.omg.CORBA.*;
import org.omg.PortableServer.*;


public class ClientEngineServant
    extends ClientEnginePOA
{
    org.omg.PortableServer.POA m_poa = null;

    private  ClientEngineImpl   m_clientEngineImpl = null;


    /**
     * Returns the server-side implementation object.
     *
     * @return ClientEngineImpl  the delegated object.
     */
    public ClientEngineImpl getClientEngineImpl()
    {
        return m_clientEngineImpl;
    }

    /**
     * Returns the server-side implementation object.
     *
     * @return EngineImpl  the delegated object.
     */
    public EngineImpl getEngineImpl()
    {
        return m_clientEngineImpl;
    }

    /**
    * Constructor of ClientEngineServant
    *
    */
    public ClientEngineServant()
    {
        this((POA) null);
    }

    /**
    * Constructor of ClientEngineServant
    *
    */
    public ClientEngineServant(ClientEngineImpl an_impl) 
    {
        this(null, an_impl);
    }

    /**
    * Constructor of ClientEngineServant
    *
    */
    public ClientEngineServant(POA the_poa)
    {
        this(the_poa, new ClientEngineImpl());
    }

    /**
    * Constructor of ClientEngineServant
    *
    */
    public ClientEngineServant(POA the_poa, ClientEngineImpl an_impl) 
    {
        m_poa = the_poa;
        m_clientEngineImpl = an_impl;
    }


    public deaf.EngineHandle getHandle()
       throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.EngineHandle         _result;
        _result = getClientEngineImpl().getHandle();
        return _result;
    }
    public deaf.Manager getManager()
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.Manager              _result;
        _result = getClientEngineImpl().getManager().getManagerReference();
        return _result;
    }
    public boolean isIdentical(deaf.Engine engn)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        boolean  _result;
        _result = getClientEngineImpl().isIdentical(engn);
        return _result;
    }
    public void remove(deaf.SessionToken token)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getClientEngineImpl().remove(token);
    }
    public void invoke(deaf.DynRequest req, deaf.DynResponseHolder res)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getClientEngineImpl().invoke(req, res);
    }


    public deaf.SessionToken login(deaf.client.LoginInfo info)
       throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.SessionToken         _result;
        _result = getClientEngineImpl().login(info);
        return _result;
    }
    public deaf.admin.PropManager getPropManager(deaf.SessionToken token)
       throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.admin.PropManager    _result;
        _result = getClientEngineImpl().getPropManager(token).getPropManagerReference();
        return _result;
    }
    public deaf.client.SkinManager getSkinManager(deaf.SessionToken token)
       throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.client.SkinManager   _result;
        _result = getClientEngineImpl().getSkinManager(token).getSkinManagerReference();
        return _result;
    }

    /**
    * Overrides the inherited _default_POA
    * The inherited _default_POA returns the root POA, by overriding it the correct POA is returned.<br>
    */
    public org.omg.PortableServer.POA _default_POA()
    {
        return m_poa;
    }

}



