/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: ServiceServant.java,v 1.3 2001/08/13 06:09:02 hyoon Exp $ 
 */ 
package deaf;

import org.omg.CORBA.*;
import org.omg.PortableServer.*;


public class ServiceServant
    extends ServicePOA
{
    org.omg.PortableServer.POA m_poa = null;


    private  ServiceImpl   m_serviceImpl = null;


    /**
     * Returns the server-side implementation object.
     *
     * @return ServiceImpl  the delegated object.
     */
    public ServiceImpl getServiceImpl()
    {
        return m_serviceImpl;
    }

    /**
    * Constructor of ServiceServant
    *
    */
    public ServiceServant()
    {
        this((POA) null);
    }

    /**
    * Constructor of ServiceServant
    *
    */
    public ServiceServant(ServiceImpl an_impl) 
    {
        this(null, an_impl);
    }

    /**
    * Constructor of ServiceServant
    *
    */
    public ServiceServant(POA the_poa)
    {
        this(the_poa, new ServiceImpl());
    }

    /**
    * Constructor of ServiceServant
    *
    */
    public ServiceServant(POA the_poa, ServiceImpl an_impl) 
    {
        m_poa = the_poa;
        m_serviceImpl = an_impl;
    }



   public int version()
    {
        int                             _result;

        _result = getServiceImpl().version();

        return _result;
   }


   public deaf.ServicePolicy policy()
    {
        deaf.ServicePolicy        _result;

        _result = getServiceImpl().policy();

        return _result;
   }

    /**
    * Overrides the inherited _default_POA
    * The inherited _default_POA returns the root POA, by overriding it the correct POA is returned.<br>
    */
    public org.omg.PortableServer.POA _default_POA()
    {
        return m_poa;
    }

}



