/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: PropManagerServant.java,v 1.4 2001/08/17 06:10:16 hyoon Exp $ 
 */ 
package deaf.admin;

import deaf.*;

import org.omg.CORBA.*;
import org.omg.PortableServer.*;


public class PropManagerServant
    extends PropManagerPOA
{
    org.omg.PortableServer.POA m_poa = null;

    private  PropManagerImpl   m_propManagerImpl = null;


    /**
     * Returns the server-side implementation object.
     *
     * @return PropManagerImpl  the delegated object.
     */
    public PropManagerImpl getPropManagerImpl()
    {
        return m_propManagerImpl;
    }

    /**
     * Returns the server-side implementation object.
     *
     * @return ManagerImpl  the delegated object.
     */
    public ManagerImpl getManagerImpl()
    {
        return m_propManagerImpl;
    }

    /**
    * Constructor of PropManagerServant
    *
    */
    public PropManagerServant()
    {
        this((POA) null);
    }

    /**
    * Constructor of PropManagerServant
    *
    */
    public PropManagerServant(PropManagerImpl an_impl) 
    {
        this(null, an_impl);
    }

    /**
    * Constructor of PropManagerServant
    *
    */
    public PropManagerServant(POA the_poa)
    {
        this(the_poa, new PropManagerImpl());
    }

    /**
    * Constructor of PropManagerServant
    *
    */
    public PropManagerServant(POA the_poa, PropManagerImpl an_impl) 
    {
        m_poa = the_poa;
        m_propManagerImpl = an_impl;
    }

    public deaf.Service getService()
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.Service              _result;
        _result = getPropManagerImpl().getService().getServiceReference();
        return _result;
    }
    public deaf.ManagerHandle getHandle()
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.ManagerHandle        _result;
        _result = getPropManagerImpl().getHandle();
        return _result;
    }
    public deaf.EngineHandle createEngine(deaf.SessionToken token)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.EngineHandle         _result;
        _result = getPropManagerImpl().createEngine(token);
        return _result;
    }
    public deaf.Engine getEngine(deaf.SessionToken token, deaf.EngineHandle hand)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.Engine               _result;
        _result = getPropManagerImpl().getEngine(token, hand).getEngineReference();
        return _result;
    }
    public deaf.EngineHandle[] getEngineList(deaf.SessionToken token)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.EngineHandle[]       _result;
        _result = getPropManagerImpl().getEngineList(token);
        return _result;
    }
    public void removeEngine(deaf.SessionToken token, deaf.EngineHandle hand)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getPropManagerImpl().removeEngine(token, hand);
    }
    public deaf.ManagerPolicy policy()
    {
        deaf.ManagerPolicy        _result;
        _result = getPropManagerImpl().policy();
        return _result;
    }

    /**
    * Overrides the inherited _default_POA
    * The inherited _default_POA returns the root POA, by overriding it the correct POA is returned.<br>
    */
    public org.omg.PortableServer.POA _default_POA()
    {
        return m_poa;
    }

}



