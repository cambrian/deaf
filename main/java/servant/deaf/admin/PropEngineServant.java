/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: PropEngineServant.java,v 1.4 2001/08/17 06:10:16 hyoon Exp $ 
 */ 
package deaf.admin;

import deaf.*;

import org.omg.CORBA.*;
import org.omg.PortableServer.*;


public class PropEngineServant
    extends PropEnginePOA
{
    org.omg.PortableServer.POA m_poa = null;

    private  PropEngineImpl   m_propEngineImpl = null;


    /**
     * Returns the server-side implementation object.
     *
     * @return PropEngineImpl  the delegated object.
     */
    public PropEngineImpl getPropEngineImpl()
    {
        return m_propEngineImpl;
    }

    /**
     * Returns the server-side implementation object.
     *
     * @return EngineImpl  the delegated object.
     */
    public EngineImpl getEngineImpl()
    {
        return m_propEngineImpl;
    }

    /**
    * Constructor of PropEngineServant
    *
    */
    public PropEngineServant()
    {
        this((POA) null);
    }

    /**
    * Constructor of PropEngineServant
    *
    */
    public PropEngineServant(PropEngineImpl an_impl) 
    {
        this(null, an_impl);
    }

    /**
    * Constructor of PropEngineServant
    *
    */
    public PropEngineServant(POA the_poa)
    {
        this(the_poa, new PropEngineImpl());
    }

    /**
    * Constructor of PropEngineServant
    *
    */
    public PropEngineServant(POA the_poa, PropEngineImpl an_impl) 
    {
        m_poa = the_poa;
        m_propEngineImpl = an_impl;
    }


    public deaf.EngineHandle getHandle()
       throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.EngineHandle         _result;
        _result = getPropEngineImpl().getHandle();
        return _result;
    }
    public deaf.Manager getManager()
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.Manager              _result;
        _result = getPropEngineImpl().getManager().getManagerReference();
        return _result;
    }
    public boolean isIdentical(deaf.Engine engn)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        boolean  _result;
        _result = getPropEngineImpl().isIdentical(engn);
        return _result;
    }
    public void remove(deaf.SessionToken token)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getPropEngineImpl().remove(token);
    }
    public void invoke(deaf.DynRequest req, deaf.DynResponseHolder res)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getPropEngineImpl().invoke(req, res);
    }


    public deaf.Property getProperty(String name)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.Property             _result;
        _result = getPropEngineImpl().getProperty(name);
        return _result;
    }
    public java.lang.String getPropertyValue(String name)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        java.lang.String                _result;
        _result = getPropEngineImpl().getPropertyValue(name);
        return _result;
    }
    public void setProperty(deaf.Property prop)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getPropEngineImpl().setProperty(prop);
    }

    /**
    * Overrides the inherited _default_POA
    * The inherited _default_POA returns the root POA, by overriding it the correct POA is returned.<br>
    */
    public org.omg.PortableServer.POA _default_POA()
    {
        return m_poa;
    }

}



