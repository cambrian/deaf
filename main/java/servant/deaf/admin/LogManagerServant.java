/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: LogManagerServant.java,v 1.4 2001/08/17 06:10:16 hyoon Exp $ 
 */ 
package deaf.admin;

import deaf.*;

import org.omg.CORBA.*;
import org.omg.PortableServer.*;


public class LogManagerServant
    extends LogManagerPOA
{
    org.omg.PortableServer.POA m_poa = null;

    private  LogManagerImpl   m_logManagerImpl = null;


    /**
     * Returns the server-side implementation object.
     *
     * @return LogManagerImpl  the delegated object.
     */
    public LogManagerImpl getLogManagerImpl()
    {
        return m_logManagerImpl;
    }

    /**
     * Returns the server-side implementation object.
     *
     * @return ManagerImpl  the delegated object.
     */
    public ManagerImpl getManagerImpl()
    {
        return m_logManagerImpl;
    }

    /**
    * Constructor of LogManagerServant
    *
    */
    public LogManagerServant()
    {
        this((POA) null);
    }

    /**
    * Constructor of LogManagerServant
    *
    */
    public LogManagerServant(LogManagerImpl an_impl) 
    {
        this(null, an_impl);
    }

    /**
    * Constructor of LogManagerServant
    *
    */
    public LogManagerServant(POA the_poa)
    {
        this(the_poa, new LogManagerImpl());
    }

    /**
    * Constructor of LogManagerServant
    *
    */
    public LogManagerServant(POA the_poa, LogManagerImpl an_impl) 
    {
        m_poa = the_poa;
        m_logManagerImpl = an_impl;
    }

    public deaf.Service getService()
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.Service              _result;
        _result = getLogManagerImpl().getService().getServiceReference();
        return _result;
    }
    public deaf.ManagerHandle getHandle()
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.ManagerHandle        _result;
        _result = getLogManagerImpl().getHandle();
        return _result;
    }
    public deaf.EngineHandle createEngine(deaf.SessionToken token)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.EngineHandle         _result;
        _result = getLogManagerImpl().createEngine(token);
        return _result;
    }
    public deaf.Engine getEngine(deaf.SessionToken token, deaf.EngineHandle hand)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.Engine               _result;
        _result = getLogManagerImpl().getEngine(token, hand).getEngineReference();
        return _result;
    }
    public deaf.EngineHandle[] getEngineList(deaf.SessionToken token)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.EngineHandle[]       _result;
        _result = getLogManagerImpl().getEngineList(token);
        return _result;
    }
    public void removeEngine(deaf.SessionToken token, deaf.EngineHandle hand)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getLogManagerImpl().removeEngine(token, hand);
    }
    public deaf.ManagerPolicy policy()
    {
        deaf.ManagerPolicy        _result;
        _result = getLogManagerImpl().policy();
        return _result;
    }

    /**
    * Overrides the inherited _default_POA
    * The inherited _default_POA returns the root POA, by overriding it the correct POA is returned.<br>
    */
    public org.omg.PortableServer.POA _default_POA()
    {
        return m_poa;
    }

}



