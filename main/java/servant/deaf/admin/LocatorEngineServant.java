/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: LocatorEngineServant.java,v 1.4 2001/08/17 06:10:16 hyoon Exp $ 
 */ 
package deaf.admin;

import deaf.*;

import org.omg.CORBA.*;
import org.omg.PortableServer.*;


public class LocatorEngineServant
    extends LocatorEnginePOA
{
    org.omg.PortableServer.POA m_poa = null;

    private  LocatorEngineImpl   m_locatorEngineImpl = null;


    /**
     * Returns the server-side implementation object.
     *
     * @return LocatorEngineImpl  the delegated object.
     */
    public LocatorEngineImpl getLocatorEngineImpl()
    {
        return m_locatorEngineImpl;
    }

    /**
     * Returns the server-side implementation object.
     *
     * @return EngineImpl  the delegated object.
     */
    public EngineImpl getEngineImpl()
    {
        return m_locatorEngineImpl;
    }

    /**
    * Constructor of LocatorEngineServant
    *
    */
    public LocatorEngineServant()
    {
        this((POA) null);
    }

    /**
    * Constructor of LocatorEngineServant
    *
    */
    public LocatorEngineServant(LocatorEngineImpl an_impl) 
    {
        this(null, an_impl);
    }


    /**
    * Constructor of LocatorEngineServant
    *
    */
    public LocatorEngineServant(POA the_poa)
    {
        this(the_poa, new LocatorEngineImpl());
    }

    /**
    * Constructor of LocatorEngineServant
    *
    */
    public LocatorEngineServant(POA the_poa, LocatorEngineImpl an_impl) 
    {
        m_poa = the_poa;
        m_locatorEngineImpl = an_impl;
    }


    public deaf.EngineHandle getHandle()
       throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.EngineHandle         _result;
        _result = getLocatorEngineImpl().getHandle();
        return _result;
    }
    public deaf.Manager getManager()
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.Manager              _result;
        _result = getLocatorEngineImpl().getManager().getManagerReference();
        return _result;
    }
    public boolean isIdentical(deaf.Engine engn)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        boolean  _result;
        _result = getLocatorEngineImpl().isIdentical(engn);
        return _result;
    }
    public void remove(deaf.SessionToken token)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getLocatorEngineImpl().remove(token);
    }
    public void invoke(deaf.DynRequest req, deaf.DynResponseHolder res)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getLocatorEngineImpl().invoke(req, res);
    }


    public deaf.ManagerHandle findManagerByRef(deaf.Manager ref)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
       deaf.ManagerHandle        _result;
        _result = getLocatorEngineImpl().findManagerByRef(new ManagerProxy(ref));
        return _result;
    }
    public deaf.ManagerHandle findManagerByName(String name)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.ManagerHandle        _result;
        _result = getLocatorEngineImpl().findManagerByName(name);
        return _result;
    }

    /**
    * Overrides the inherited _default_POA
    * The inherited _default_POA returns the root POA, by overriding it the correct POA is returned.<br>
    */
    public org.omg.PortableServer.POA _default_POA()
    {
        return m_poa;
    }

}



