/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: AdminEngineServant.java,v 1.4 2001/08/17 06:10:16 hyoon Exp $ 
 */ 
package deaf.admin;

import deaf.*;

import org.omg.CORBA.*;
import org.omg.PortableServer.*;


public class AdminEngineServant
    extends AdminEnginePOA
{
    org.omg.PortableServer.POA m_poa = null;


    private  AdminEngineImpl   m_adminEngineImpl = null;


    /**
     * Returns the server-side implementation object.
     *
     * @return AdminEngineImpl  the delegated object.
     */
    public AdminEngineImpl getAdminEngineImpl()
    {
        return m_adminEngineImpl;
    }

    /**
     * Returns the server-side implementation object.
     *
     * @return EngineImpl  the delegated object.
     */
    public EngineImpl getEngineImpl()
    {
        return m_adminEngineImpl;
    }

    /**
    * Constructor of AdminEngineServant
    *
    */
    public AdminEngineServant()
    {
        this((POA) null);
    }

    /**
    * Constructor of AdminEngineServant
    *
    */
    public AdminEngineServant(AdminEngineImpl an_impl) 
    {
        this(null, an_impl);
    }

    /**
    * Constructor of AdminEngineServant
    *
    */
    public AdminEngineServant(POA the_poa)
    {
        this(the_poa, new AdminEngineImpl());
    }

    /**
    * Constructor of AdminEngineServant
    *
    */
    public AdminEngineServant(POA the_poa, AdminEngineImpl an_impl) 
    {
        m_poa = the_poa;
        m_adminEngineImpl = an_impl;
    }


    public deaf.EngineHandle getHandle()
       throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.EngineHandle         _result;
        _result = getAdminEngineImpl().getHandle();
        return _result;
    }
    public deaf.Manager getManager()
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.Manager              _result;
        _result = getAdminEngineImpl().getManager().getManagerReference();
        return _result;
    }
    public boolean isIdentical(deaf.Engine engn)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        boolean  _result;
        _result = getAdminEngineImpl().isIdentical(engn);
        return _result;
    }
    public void remove(deaf.SessionToken token)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getAdminEngineImpl().remove(token);
    }
    public void invoke(deaf.DynRequest req, deaf.DynResponseHolder res)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getAdminEngineImpl().invoke(req, res);
    }


    public deaf.app.ModuleManager getModuleManager(deaf.SessionToken token)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
       deaf.app.ModuleManager    _result;
        _result = getAdminEngineImpl().getModuleManager(token).getModuleManagerReference();
       return _result;
    }
    public deaf.admin.LocatorManager getLocatorManager(deaf.SessionToken token)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.admin.LocatorManager _result;
        _result = getAdminEngineImpl().getLocatorManager(token).getLocatorManagerReference();
        return _result;
    }
    public deaf.admin.PropManager getPropManager(deaf.SessionToken token)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
       deaf.admin.PropManager    _result;
        _result = getAdminEngineImpl().getPropManager(token).getPropManagerReference();
       return _result;
    }
    public deaf.admin.LogManager getLogManager(deaf.SessionToken token)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.admin.LogManager     _result;
        _result = getAdminEngineImpl().getLogManager(token).getLogManagerReference();
        return _result;
    }
    public void startManager(deaf.SessionToken token, deaf.ManagerHandle hand)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getAdminEngineImpl().startManager(token, hand);
    }
    public void shutdownAllManagers(deaf.SessionToken token)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getAdminEngineImpl().shutdownAllManagers(token);
    }
    public void shutdownManager(deaf.SessionToken token, deaf.ManagerHandle hand)
        throws org.omg.CORBA.SystemException, deaf.AppException
    { 
        getAdminEngineImpl().shutdownManager(token, hand);
    }

    /**
    * Overrides the inherited _default_POA
    * The inherited _default_POA returns the root POA, by overriding it the correct POA is returned.<br>
    */
    public org.omg.PortableServer.POA _default_POA()
    {
        return m_poa;
    }

}



