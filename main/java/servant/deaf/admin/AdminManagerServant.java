/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: AdminManagerServant.java,v 1.4 2001/08/17 06:10:16 hyoon Exp $ 
 */ 
package deaf.admin;

import deaf.*;

import org.omg.CORBA.*;
import org.omg.PortableServer.*;


public class AdminManagerServant
    extends AdminManagerPOA
{
    org.omg.PortableServer.POA m_poa = null;

    private  AdminManagerImpl   m_adminManagerImpl = null;


    /**
     * Returns the server-side implementation object.
     *
     * @return AdminManagerImpl  the delegated object.
     */
    public AdminManagerImpl getAdminManagerImpl()
    {
        return m_adminManagerImpl;
    }

    /**
     * Returns the server-side implementation object.
     *
     * @return ManagerImpl  the delegated object.
     */
    public ManagerImpl getManagerImpl()
    {
        return m_adminManagerImpl;
    }

    /**
    * Constructor of AdminManagerServant
    *
    */
    public AdminManagerServant()
    {
        this((POA) null);
    }

    /**
    * Constructor of AdminManagerServant
    *
    */
    public AdminManagerServant(AdminManagerImpl an_impl) 
    {
        this(null, an_impl);
    }

    /**
    * Constructor of AdminManagerServant
    *
    */
    public AdminManagerServant(POA the_poa)
    {
        this(the_poa, new AdminManagerImpl());
    }

    /**
    * Constructor of AdminManagerServant
    *
    */
    public AdminManagerServant(POA the_poa, AdminManagerImpl an_impl) 
    {
        m_poa = the_poa;
        m_adminManagerImpl = an_impl;
    }



    public deaf.Service getService()
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.Service              _result;
        _result = getAdminManagerImpl().getService().getServiceReference();
        return _result;
    }
    public deaf.ManagerHandle getHandle()
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.ManagerHandle        _result;
        _result = getAdminManagerImpl().getHandle();
        return _result;
    }
    public deaf.EngineHandle createEngine(deaf.SessionToken token)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.EngineHandle         _result;
        _result = getAdminManagerImpl().createEngine(token);
        return _result;
    }
    public deaf.Engine getEngine(deaf.SessionToken token, deaf.EngineHandle hand)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.Engine               _result;
        _result = getAdminManagerImpl().getEngine(token, hand).getEngineReference();
        return _result;
    }
    public deaf.EngineHandle[] getEngineList(deaf.SessionToken token)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.EngineHandle[]       _result;
        _result = getAdminManagerImpl().getEngineList(token);
        return _result;
    }
    public void removeEngine(deaf.SessionToken token, deaf.EngineHandle hand)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getAdminManagerImpl().removeEngine(token, hand);
    }
    public deaf.ManagerPolicy policy()
    {
        deaf.ManagerPolicy        _result;
        _result = getAdminManagerImpl().policy();
        return _result;
    }


    /**
    * Overrides the inherited _default_POA
    * The inherited _default_POA returns the root POA, by overriding it the correct POA is returned.<br>
    */
    public org.omg.PortableServer.POA _default_POA()
    {
        return m_poa;
    }

}



