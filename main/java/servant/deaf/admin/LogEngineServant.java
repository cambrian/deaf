/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: LogEngineServant.java,v 1.4 2001/08/17 06:10:16 hyoon Exp $ 
 */ 
package deaf.admin;

import deaf.*;

import org.omg.CORBA.*;
import org.omg.PortableServer.*;


public class LogEngineServant
    extends LogEnginePOA
{
    org.omg.PortableServer.POA m_poa = null;


    private  LogEngineImpl   m_logEngineImpl = null;


    /**
     * Returns the server-side implementation object.
     *
     * @return LogEngineImpl  the delegated object.
     */
    public LogEngineImpl getLogEngineImpl()
    {
        return m_logEngineImpl;
    }

    /**
     * Returns the server-side implementation object.
     *
     * @return EngineImpl  the delegated object.
     */
    public EngineImpl getEngineImpl()
    {
        return m_logEngineImpl;
    }

    /**
    * Constructor of LogEngineServant
    *
    */
    public LogEngineServant()
    {
        this((POA) null);
    }

    /**
    * Constructor of LogEngineServant
    *
    */
    public LogEngineServant(LogEngineImpl an_impl) 
    {
        this(null, an_impl);
    }

    /**
    * Constructor of LogEngineServant
    *
    */
    public LogEngineServant(POA the_poa)
    {
        this(the_poa, new LogEngineImpl());
    }

    /**
    * Constructor of LogEngineServant
    *
    */
    public LogEngineServant(POA the_poa, LogEngineImpl an_impl) 
    {
        m_poa = the_poa;
        m_logEngineImpl = an_impl;
    }


    public deaf.EngineHandle getHandle()
       throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.EngineHandle         _result;
        _result = getLogEngineImpl().getHandle();
        return _result;
    }
    public deaf.Manager getManager()
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.Manager              _result;
        _result = getLogEngineImpl().getManager().getManagerReference();
        return _result;
    }
    public boolean isIdentical(deaf.Engine engn)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        boolean  _result;
        _result = getLogEngineImpl().isIdentical(engn);
        return _result;
    }
    public void remove(deaf.SessionToken token)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getLogEngineImpl().remove(token);
    }
    public void invoke(deaf.DynRequest req, deaf.DynResponseHolder res)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getLogEngineImpl().invoke(req, res);
    }


    public void writeMessage(int level, String message)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getLogEngineImpl().writeMessage(level, message);
    }

    public java.lang.String locale()
    {
        java.lang.String                _result;
        _result = getLogEngineImpl().locale();
        return _result;
    }

    public void locale(String _new_value)
    {
        getLogEngineImpl().locale(_new_value);
    }

    public java.lang.String format()
    {
        java.lang.String                _result;
        _result = getLogEngineImpl().format();
        return _result;
    }

    public void format(String  _new_value)
    {
        getLogEngineImpl().format(_new_value);
    }

    public int defaultLevel()
    {
        int                             _result;
        _result = getLogEngineImpl().defaultLevel();
        return _result;
    }


    public void defaultLevel(int _new_value)
    {
        getLogEngineImpl().defaultLevel(_new_value);
    }

    /**
    * Overrides the inherited _default_POA
    * The inherited _default_POA returns the root POA, by overriding it the correct POA is returned.<br>
    */
    public org.omg.PortableServer.POA _default_POA()
    {
        return m_poa;
    }

}



