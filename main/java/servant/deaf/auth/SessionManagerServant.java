/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: SessionManagerServant.java,v 1.4 2001/08/17 06:10:16 hyoon Exp $ 
 */ 
package deaf.auth;

import deaf.*;

import org.omg.CORBA.*;
import org.omg.PortableServer.*;


public class SessionManagerServant
    extends SessionManagerPOA
{
    org.omg.PortableServer.POA m_poa = null;

    private  SessionManagerImpl   m_sessionManagerImpl = null;


    /**
     * Returns the server-side implementation object.
     *
     * @return SessionManagerImpl  the delegated object.
     */
    public SessionManagerImpl getSessionManagerImpl()
    {
        return m_sessionManagerImpl;
    }

    /**
     * Returns the server-side implementation object.
     *
     * @return ManagerImpl  the delegated object.
     */
    public ManagerImpl getManagerImpl()
    {
        return m_sessionManagerImpl;
    }

    /**
    * Constructor of SessionManagerServant
    *
    */
    public SessionManagerServant()
    {
        this((POA) null);
    }

    /**
    * Constructor of SessionManagerServant
    *
    */
    public SessionManagerServant(SessionManagerImpl an_impl) 
    {
        this(null, an_impl);
    }

    /**
    * Constructor of SessionManagerServant
    *
    */
    public SessionManagerServant(POA the_poa)
    {
        this(the_poa, new SessionManagerImpl());
    }

    /**
    * Constructor of SessionManagerServant
    *
    */
    public SessionManagerServant(POA the_poa, SessionManagerImpl an_impl) 
    {
        m_poa = the_poa;
        m_sessionManagerImpl = an_impl;
    }



    public deaf.Service getService()
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.Service              _result;
        _result = getSessionManagerImpl().getService().getServiceReference();
        return _result;
    }
    public deaf.ManagerHandle getHandle()
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.ManagerHandle        _result;
        _result = getSessionManagerImpl().getHandle();
        return _result;
    }
    public deaf.EngineHandle createEngine(deaf.SessionToken token)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.EngineHandle         _result;
        _result = getSessionManagerImpl().createEngine(token);
        return _result;
    }
    public deaf.Engine getEngine(deaf.SessionToken token, deaf.EngineHandle hand)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.Engine               _result;
        _result = getSessionManagerImpl().getEngine(token, hand).getEngineReference();
        return _result;
    }
    public deaf.EngineHandle[] getEngineList(deaf.SessionToken token)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.EngineHandle[]       _result;
        _result = getSessionManagerImpl().getEngineList(token);
        return _result;
    }
    public void removeEngine(deaf.SessionToken token, deaf.EngineHandle hand)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getSessionManagerImpl().removeEngine(token, hand);
    }
    public deaf.ManagerPolicy policy()
    {
        deaf.ManagerPolicy        _result;
        _result = getSessionManagerImpl().policy();
        return _result;
    }

    /**
    * Overrides the inherited _default_POA
    * The inherited _default_POA returns the root POA, by overriding it the correct POA is returned.<br>
    */
    public org.omg.PortableServer.POA _default_POA()
    {
        return m_poa;
    }

}



