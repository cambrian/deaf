/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: AuthManagerServant.java,v 1.4 2001/08/17 06:10:16 hyoon Exp $ 
 */ 
package deaf.auth;

import deaf.*;

import org.omg.CORBA.*;
import org.omg.PortableServer.*;


public class AuthManagerServant
    extends AuthManagerPOA
{
    org.omg.PortableServer.POA m_poa = null;

    private  AuthManagerImpl   m_authManagerImpl = null;


    /**
     * Returns the server-side implementation object.
     *
     * @return AuthManagerImpl  the delegated object.
     */
    public AuthManagerImpl getAuthManagerImpl()
    {
        return m_authManagerImpl;
    }

    /**
     * Returns the server-side implementation object.
     *
     * @return ManagerImpl  the delegated object.
     */
    public ManagerImpl getManagerImpl()
    {
        return m_authManagerImpl;
    }

    /**
    * Constructor of AuthManagerServant
    *
    */
    public AuthManagerServant()
    {
        this((POA) null);
    }

    /**
    * Constructor of AuthManagerServant
    *
    */
    public AuthManagerServant(AuthManagerImpl an_impl) 
    {
        this(null, an_impl);
    }

    /**
    * Constructor of AuthManagerServant
    *
    */
    public AuthManagerServant(POA the_poa)
    {
        this(the_poa, new AuthManagerImpl());
    }

    /**
    * Constructor of AuthManagerServant
    *
    */
    public AuthManagerServant(POA the_poa, AuthManagerImpl an_impl) 
    {
        m_poa = the_poa;
        m_authManagerImpl = an_impl;
    }



    public deaf.Service getService()
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.Service              _result;
        _result = getAuthManagerImpl().getService().getServiceReference();
        return _result;
    }
    public deaf.ManagerHandle getHandle()
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.ManagerHandle        _result;
        _result = getAuthManagerImpl().getHandle();
        return _result;
    }
    public deaf.EngineHandle createEngine(deaf.SessionToken token)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.EngineHandle         _result;
        _result = getAuthManagerImpl().createEngine(token);
        return _result;
    }
    public deaf.Engine getEngine(deaf.SessionToken token, deaf.EngineHandle hand)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.Engine               _result;
        _result = getAuthManagerImpl().getEngine(token, hand).getEngineReference();
        return _result;
    }
    public deaf.EngineHandle[] getEngineList(deaf.SessionToken token)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.EngineHandle[]       _result;
        _result = getAuthManagerImpl().getEngineList(token);
        return _result;
    }
    public void removeEngine(deaf.SessionToken token, deaf.EngineHandle hand)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getAuthManagerImpl().removeEngine(token, hand);
    }
    public deaf.ManagerPolicy policy()
    {
        deaf.ManagerPolicy        _result;
        _result = getAuthManagerImpl().policy();
        return _result;
    }

    /**
    * Overrides the inherited _default_POA
    * The inherited _default_POA returns the root POA, by overriding it the correct POA is returned.<br>
    */
    public org.omg.PortableServer.POA _default_POA()
    {
        return m_poa;
    }

}



