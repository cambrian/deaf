/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: AuthorizationManagerServant.java,v 1.4 2001/08/17 06:10:16 hyoon Exp $ 
 */ 
package deaf.auth;

import deaf.*;

import org.omg.CORBA.*;
import org.omg.PortableServer.*;


public class AuthorizationManagerServant
    extends AuthorizationManagerPOA
{
    org.omg.PortableServer.POA m_poa = null;

    private  AuthorizationManagerImpl   m_authorizationManagerImpl = null;


    /**
     * Returns the server-side implementation object.
     *
     * @return AuthorizationManagerImpl  the delegated object.
     */
    public AuthorizationManagerImpl getAuthorizationManagerImpl()
    {
        return m_authorizationManagerImpl;
    }

    /**
     * Returns the server-side implementation object.
     *
     * @return ManagerImpl  the delegated object.
     */
    public ManagerImpl getManagerImpl()
    {
        return m_authorizationManagerImpl;
    }

    /**
    * Constructor of AuthorizationManagerServant
    *
    */
    public AuthorizationManagerServant()
    {
        this((POA) null);
    }

    /**
    * Constructor of AuthorizationManagerServant
    *
    */
    public AuthorizationManagerServant(AuthorizationManagerImpl an_impl) 
    {
        this(null, an_impl);
    }

    /**
    * Constructor of AuthorizationManagerServant
    *
    */
    public AuthorizationManagerServant(POA the_poa)
    {
        this(the_poa, new AuthorizationManagerImpl());
    }

    /**
    * Constructor of AuthorizationManagerServant
    *
    */
    public AuthorizationManagerServant(POA the_poa, AuthorizationManagerImpl an_impl) 
    {
        m_poa = the_poa;
        m_authorizationManagerImpl = an_impl;
    }



    public deaf.Service getService()
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.Service              _result;
        _result = getAuthorizationManagerImpl().getService().getServiceReference();
        return _result;
    }
    public deaf.ManagerHandle getHandle()
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.ManagerHandle        _result;
        _result = getAuthorizationManagerImpl().getHandle();
        return _result;
    }
    public deaf.EngineHandle createEngine(deaf.SessionToken token)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.EngineHandle         _result;
        _result = getAuthorizationManagerImpl().createEngine(token);
        return _result;
    }
    public deaf.Engine getEngine(deaf.SessionToken token, deaf.EngineHandle hand)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.Engine               _result;
        _result = getAuthorizationManagerImpl().getEngine(token, hand).getEngineReference();
        return _result;
    }
    public deaf.EngineHandle[] getEngineList(deaf.SessionToken token)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.EngineHandle[]       _result;
        _result = getAuthorizationManagerImpl().getEngineList(token);
        return _result;
    }
    public void removeEngine(deaf.SessionToken token, deaf.EngineHandle hand)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getAuthorizationManagerImpl().removeEngine(token, hand);
    }
    public deaf.ManagerPolicy policy()
    {
        deaf.ManagerPolicy        _result;
        _result = getAuthorizationManagerImpl().policy();
        return _result;
    }

    /**
    * Overrides the inherited _default_POA
    * The inherited _default_POA returns the root POA, by overriding it the correct POA is returned.<br>
    */
    public org.omg.PortableServer.POA _default_POA()
    {
        return m_poa;
    }

}



