/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: AuthenticationManagerServant.java,v 1.4 2001/08/17 06:10:16 hyoon Exp $ 
 */ 
package deaf.auth;

import deaf.*;

import org.omg.CORBA.*;
import org.omg.PortableServer.*;


public class AuthenticationManagerServant
    extends AuthenticationManagerPOA
{
    org.omg.PortableServer.POA m_poa = null;

    private  AuthenticationManagerImpl   m_authenticationManagerImpl = null;


    /**
     * Returns the server-side implementation object.
     *
     * @return AuthenticationManagerImpl  the delegated object.
     */
    public AuthenticationManagerImpl getAuthenticationManagerImpl()
    {
        return m_authenticationManagerImpl;
    }

    /**
     * Returns the server-side implementation object.
     *
     * @return ManagerImpl  the delegated object.
     */
    public ManagerImpl getManagerImpl()
    {
        return m_authenticationManagerImpl;
    }

    /**
    * Constructor of AuthenticationManagerServant
    *
    */
    public AuthenticationManagerServant()
    {
        this((POA) null);
    }

    /**
    * Constructor of AuthenticationManagerServant
    *
    */
    public AuthenticationManagerServant(AuthenticationManagerImpl an_impl) 
    {
        this(null, an_impl);
    }

    /**
    * Constructor of AuthenticationManagerServant
    *
    */
    public AuthenticationManagerServant(POA the_poa)
    {
        this(the_poa, new AuthenticationManagerImpl());
    }

    /**
    * Constructor of AuthenticationManagerServant
    *
    */
    public AuthenticationManagerServant(POA the_poa, AuthenticationManagerImpl an_impl) 
    {
        m_poa = the_poa;
        m_authenticationManagerImpl = an_impl;
    }



    public deaf.Service getService()
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.Service              _result;
        _result = getAuthenticationManagerImpl().getService().getServiceReference();
        return _result;
    }
    public deaf.ManagerHandle getHandle()
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.ManagerHandle        _result;
        _result = getAuthenticationManagerImpl().getHandle();
        return _result;
    }
    public deaf.EngineHandle createEngine(deaf.SessionToken token)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.EngineHandle         _result;
        _result = getAuthenticationManagerImpl().createEngine(token);
        return _result;
    }
    public deaf.Engine getEngine(deaf.SessionToken token, deaf.EngineHandle hand)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.Engine               _result;
        _result = getAuthenticationManagerImpl().getEngine(token, hand).getEngineReference();
        return _result;
    }
    public deaf.EngineHandle[] getEngineList(deaf.SessionToken token)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.EngineHandle[]       _result;
        _result = getAuthenticationManagerImpl().getEngineList(token);
        return _result;
    }
    public void removeEngine(deaf.SessionToken token, deaf.EngineHandle hand)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getAuthenticationManagerImpl().removeEngine(token, hand);
    }
    public deaf.ManagerPolicy policy()
    {
        deaf.ManagerPolicy        _result;
        _result = getAuthenticationManagerImpl().policy();
        return _result;
    }

    /**
    * Overrides the inherited _default_POA
    * The inherited _default_POA returns the root POA, by overriding it the correct POA is returned.<br>
    */
    public org.omg.PortableServer.POA _default_POA()
    {
        return m_poa;
    }

}



