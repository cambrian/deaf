/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: AuthorizationEngineServant.java,v 1.4 2001/08/17 06:10:16 hyoon Exp $ 
 */ 
package deaf.auth;

import deaf.*;

import org.omg.CORBA.*;
import org.omg.PortableServer.*;


public class AuthorizationEngineServant
    extends AuthorizationEnginePOA
{
    org.omg.PortableServer.POA m_poa = null;

    private  AuthorizationEngineImpl   m_authorizationEngineImpl = null;


    /**
     * Returns the server-side implementation object.
     *
     * @return AuthorizationEngineImpl  the delegated object.
     */
    public AuthorizationEngineImpl getAuthorizationEngineImpl()
    {
        return m_authorizationEngineImpl;
    }

    /**
     * Returns the server-side implementation object.
     *
     * @return EngineImpl  the delegated object.
     */
    public EngineImpl getEngineImpl()
    {
        return m_authorizationEngineImpl;
    }

    /**
    * Constructor of AuthorizationEngineServant
    *
    */
    public AuthorizationEngineServant()
    {
        this((POA) null);
    }

    /**
    * Constructor of AuthorizationEngineServant
    *
    */
    public AuthorizationEngineServant(AuthorizationEngineImpl an_impl) 
    {
        this(null, an_impl);
    }

    /**
    * Constructor of AuthorizationEngineServant
    *
    */
    public AuthorizationEngineServant(POA the_poa)
    {
        this(the_poa, new AuthorizationEngineImpl());
    }

    /**
    * Constructor of AuthorizationEngineServant
    *
    */
    public AuthorizationEngineServant(POA the_poa, AuthorizationEngineImpl an_impl) 
    {
        m_poa = the_poa;
        m_authorizationEngineImpl = an_impl;
    }


    public deaf.EngineHandle getHandle()
       throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.EngineHandle         _result;
        _result = getAuthorizationEngineImpl().getHandle();
        return _result;
    }
    public deaf.Manager getManager()
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.Manager              _result;
        _result = getAuthorizationEngineImpl().getManager().getManagerReference();
        return _result;
    }
    public boolean isIdentical(deaf.Engine engn)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        boolean  _result;
        _result = getAuthorizationEngineImpl().isIdentical(engn);
        return _result;
    }
    public void remove(deaf.SessionToken token)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getAuthorizationEngineImpl().remove(token);
    }
    public void invoke(deaf.DynRequest req, deaf.DynResponseHolder res)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getAuthorizationEngineImpl().invoke(req, res);
    }


    public deaf.Role getRole(deaf.SessionToken token)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.Role                 _result;
        _result = getAuthorizationEngineImpl().getRole(token);
        return _result;
    }

    /**
    * Overrides the inherited _default_POA
    * The inherited _default_POA returns the root POA, by overriding it the correct POA is returned.<br>
    */
    public org.omg.PortableServer.POA _default_POA()
    {
        return m_poa;
    }

}



