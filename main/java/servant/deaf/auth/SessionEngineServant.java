/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: SessionEngineServant.java,v 1.4 2001/08/17 06:10:16 hyoon Exp $ 
 */ 
package deaf.auth;

import deaf.*;

import org.omg.CORBA.*;
import org.omg.PortableServer.*;


public class SessionEngineServant
    extends SessionEnginePOA
{
    org.omg.PortableServer.POA m_poa = null;

    private  SessionEngineImpl   m_sessionEngineImpl = null;


    /**
     * Returns the server-side implementation object.
     *
     * @return SessionEngineImpl  the delegated object.
     */
    public SessionEngineImpl getSessionEngineImpl()
    {
        return m_sessionEngineImpl;
    }

    /**
     * Returns the server-side implementation object.
     *
     * @return EngineImpl  the delegated object.
     */
    public EngineImpl getEngineImpl()
    {
        return m_sessionEngineImpl;
    }

    /**
    * Constructor of SessionEngineServant
    *
    */
    public SessionEngineServant()
    {
        this((POA) null);
    }

    /**
    * Constructor of SessionEngineServant
    *
    */
    public SessionEngineServant(SessionEngineImpl an_impl) 
    {
        this(null, an_impl);
    }

    /**
    * Constructor of SessionEngineServant
    *
    */
    public SessionEngineServant(POA the_poa)
    {
        this(the_poa, new SessionEngineImpl());
    }

    /**
    * Constructor of SessionEngineServant
    *
    */
    public SessionEngineServant(POA the_poa, SessionEngineImpl an_impl) 
    {
        m_poa = the_poa;
        m_sessionEngineImpl = an_impl;
    }


    public deaf.EngineHandle getHandle()
       throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.EngineHandle         _result;
        _result = getSessionEngineImpl().getHandle();
        return _result;
    }
    public deaf.Manager getManager()
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.Manager              _result;
        _result = getSessionEngineImpl().getManager().getManagerReference();
        return _result;
    }
    public boolean isIdentical(deaf.Engine engn)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        boolean  _result;
        _result = getSessionEngineImpl().isIdentical(engn);
        return _result;
    }
    public void remove(deaf.SessionToken token)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getSessionEngineImpl().remove(token);
    }
    public void invoke(deaf.DynRequest req, deaf.DynResponseHolder res)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getSessionEngineImpl().invoke(req, res);
    }


    public deaf.SessionObject getSessionObject(deaf.SessionToken token)
       throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.SessionObject        _result;
        _result = getSessionEngineImpl().getSessionObject(token);
        return _result;
    }

    /**
    * Overrides the inherited _default_POA
    * The inherited _default_POA returns the root POA, by overriding it the correct POA is returned.<br>
    */
    public org.omg.PortableServer.POA _default_POA()
    {
        return m_poa;
    }

}



