/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: AuthenticationEngineServant.java,v 1.4 2001/08/17 06:10:16 hyoon Exp $ 
 */ 
package deaf.auth;


import deaf.*;

import org.omg.CORBA.*;
import org.omg.PortableServer.*;


public class AuthenticationEngineServant
    extends AuthenticationEnginePOA
{
    org.omg.PortableServer.POA m_poa = null;

    private  AuthenticationEngineImpl   m_authenticationEngineImpl = null;


    /**
     * Returns the server-side implementation object.
     *
     * @return AuthenticationEngineImpl  the delegated object.
     */
    public AuthenticationEngineImpl getAuthenticationEngineImpl()
    {
        return m_authenticationEngineImpl;
    }

    /**
     * Returns the server-side implementation object.
     *
     * @return EngineImpl  the delegated object.
     */
    public EngineImpl getEngineImpl()
    {
        return m_authenticationEngineImpl;
    }

    /**
    * Constructor of AuthenticationEngineServant
    *
    */
    public AuthenticationEngineServant()
    {
        this((POA) null);
    }

    /**
    * Constructor of AuthenticationEngineServant
    *
    */
    public AuthenticationEngineServant(AuthenticationEngineImpl an_impl) 
    {
        this(null, an_impl);
    }

    /**
    * Constructor of AuthenticationEngineServant
    *
    */
    public AuthenticationEngineServant(POA the_poa)
    {
        this(the_poa, new AuthenticationEngineImpl());
    }

    /**
    * Constructor of AuthenticationEngineServant
    *
    */
    public AuthenticationEngineServant(POA the_poa, AuthenticationEngineImpl an_impl) 
    {
        m_poa = the_poa;
        m_authenticationEngineImpl = an_impl;
    }


    public deaf.EngineHandle getHandle()
       throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.EngineHandle         _result;
        _result = getAuthenticationEngineImpl().getHandle();
        return _result;
    }
    public deaf.Manager getManager()
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.Manager              _result;
        _result = getAuthenticationEngineImpl().getManager().getManagerReference();
        return _result;
    }
    public boolean isIdentical(deaf.Engine engn)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        boolean  _result;
        _result = getAuthenticationEngineImpl().isIdentical(engn);
        return _result;
    }
    public void remove(deaf.SessionToken token)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getAuthenticationEngineImpl().remove(token);
    }
    public void invoke(deaf.DynRequest req, deaf.DynResponseHolder res)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getAuthenticationEngineImpl().invoke(req, res);
    }


    /**
    * Overrides the inherited _default_POA
    * The inherited _default_POA returns the root POA, by overriding it the correct POA is returned.<br>
    */
    public org.omg.PortableServer.POA _default_POA()
    {
        return m_poa;
    }

}



