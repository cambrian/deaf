/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: AuthEngineServant.java,v 1.4 2001/08/17 06:10:16 hyoon Exp $ 
 */ 
package deaf.auth;

import deaf.*;

import org.omg.CORBA.*;
import org.omg.PortableServer.*;


public class AuthEngineServant
    extends AuthEnginePOA
{
    org.omg.PortableServer.POA m_poa = null;

    private  AuthEngineImpl   m_authEngineImpl = null;


    /**
     * Returns the server-side implementation object.
     *
     * @return AuthEngineImpl  the delegated object.
     */
    public AuthEngineImpl getAuthEngineImpl()
    {
        return m_authEngineImpl;
    }

    /**
     * Returns the server-side implementation object.
     *
     * @return EngineImpl  the delegated object.
     */
    public EngineImpl getEngineImpl()
    {
        return m_authEngineImpl;
    }

    /**
    * Constructor of AuthEngineServant
    *
    */
    public AuthEngineServant()
    {
        this((POA) null);
    }

    /**
    * Constructor of AuthEngineServant
    *
    */
    public AuthEngineServant(AuthEngineImpl an_impl) 
    {
        this(null, an_impl);
    }

    /**
    * Constructor of AuthEngineServant
    *
    */
    public AuthEngineServant(POA the_poa)
    {
        this(the_poa, new AuthEngineImpl());
    }

    /**
    * Constructor of AuthEngineServant
    *
    */
    public AuthEngineServant(POA the_poa, AuthEngineImpl an_impl) 
    {
        m_poa = the_poa;
        m_authEngineImpl = an_impl;
    }


    public deaf.EngineHandle getHandle()
       throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.EngineHandle         _result;
        _result = getAuthEngineImpl().getHandle();
        return _result;
    }
    public deaf.Manager getManager()
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.Manager              _result;
        _result = getAuthEngineImpl().getManager().getManagerReference();
        return _result;
    }
    public boolean isIdentical(deaf.Engine engn)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        boolean  _result;
        _result = getAuthEngineImpl().isIdentical(engn);
        return _result;
    }
    public void remove(deaf.SessionToken token)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getAuthEngineImpl().remove(token);
    }
    public void invoke(deaf.DynRequest req, deaf.DynResponseHolder res)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getAuthEngineImpl().invoke(req, res);
    }

    public deaf.auth.AuthenticationManager getAuthenticationManager()
       throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.auth.AuthenticationManager _result;
        _result = getAuthEngineImpl().getAuthenticationManager().getAuthenticationManagerReference();
        return _result;
    }
    public deaf.auth.AuthorizationManager getAuthorizationManager()
       throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.auth.AuthorizationManager _result;
        _result = getAuthEngineImpl().getAuthorizationManager().getAuthorizationManagerReference();
        return _result;
    }
    public deaf.auth.SessionManager getSessionManager()
       throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.auth.SessionManager  _result;
        _result = getAuthEngineImpl().getSessionManager().getSessionManagerReference();
        return _result;
    }

    /**
    * Overrides the inherited _default_POA
    * The inherited _default_POA returns the root POA, by overriding it the correct POA is returned.<br>
    */
    public org.omg.PortableServer.POA _default_POA()
    {
        return m_poa;
    }

}



