/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: AppManagerServant.java,v 1.4 2001/08/17 06:10:16 hyoon Exp $ 
 */ 
package deaf.app;

import deaf.*;

import org.omg.CORBA.*;
import org.omg.PortableServer.*;


public class AppManagerServant
    extends AppManagerPOA
{
    org.omg.PortableServer.POA m_poa = null;

    private  AppManagerImpl   m_appManagerImpl = null;


    /**
     * Returns the server-side implementation object.
     *
     * @return AppManagerImpl  the delegated object.
     */
    public AppManagerImpl getAppManagerImpl()
    {
        return m_appManagerImpl;
    }

    /**
     * Returns the server-side implementation object.
     *
     * @return ManagerImpl  the delegated object.
     */
    public ManagerImpl getManagerImpl()
    {
        return m_appManagerImpl;
    }

    /**
    * Constructor of AppManagerServant
    *
    */
    public AppManagerServant()
    {
        this((POA) null);
    }

    /**
    * Constructor of AppManagerServant
    *
    */
    public AppManagerServant(AppManagerImpl an_impl) 
    {
        this(null, an_impl);
    }

    /**
    * Constructor of AppManagerServant
    *
    */
    public AppManagerServant(POA the_poa)
    {
        this(the_poa, new AppManagerImpl());
    }

    /**
    * Constructor of AppManagerServant
    *
    */
    public AppManagerServant(POA the_poa, AppManagerImpl an_impl) 
    {
        m_poa = the_poa;
        m_appManagerImpl = an_impl;
    }

    public deaf.Service getService()
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.Service              _result;
        _result = getAppManagerImpl().getService().getServiceReference();
        return _result;
    }
    public deaf.ManagerHandle getHandle()
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.ManagerHandle        _result;
        _result = getAppManagerImpl().getHandle();
        return _result;
    }
    public deaf.EngineHandle createEngine(deaf.SessionToken token)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.EngineHandle         _result;
        _result = getAppManagerImpl().createEngine(token);
        return _result;
    }
    public deaf.Engine getEngine(deaf.SessionToken token, deaf.EngineHandle hand)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.Engine               _result;
        _result = getAppManagerImpl().getEngine(token, hand).getEngineReference();
        return _result;
    }
    public deaf.EngineHandle[] getEngineList(deaf.SessionToken token)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.EngineHandle[]       _result;
        _result = getAppManagerImpl().getEngineList(token);
        return _result;
    }
    public void removeEngine(deaf.SessionToken token, deaf.EngineHandle hand)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getAppManagerImpl().removeEngine(token, hand);
    }
    public deaf.ManagerPolicy policy()
    {
        deaf.ManagerPolicy        _result;
        _result = getAppManagerImpl().policy();
        return _result;
    }

    public int version()
    {
        int                             _result;
        _result = getAppManagerImpl().version();
        return _result;
    }

    /**
    * Overrides the inherited _default_POA
    * The inherited _default_POA returns the root POA, by overriding it the correct POA is returned.<br>
    */
    public org.omg.PortableServer.POA _default_POA()
    {
        return m_poa;
    }

}



