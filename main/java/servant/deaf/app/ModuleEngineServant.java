/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: ModuleEngineServant.java,v 1.4 2001/08/17 06:10:16 hyoon Exp $ 
 */ 
package deaf.app;

import deaf.*;

import org.omg.CORBA.*;
import org.omg.PortableServer.*;


public class ModuleEngineServant
    extends ModuleEnginePOA
{
    org.omg.PortableServer.POA m_poa = null;

    private  ModuleEngineImpl   m_moduleEngineImpl = null;


    /**
     * Returns the server-side implementation object.
     *
     * @return ModuleEngineImpl  the delegated object.
     */
    public ModuleEngineImpl getModuleEngineImpl()
    {
        return m_moduleEngineImpl;
    }

    /**
     * Returns the server-side implementation object.
     *
     * @return EngineImpl  the delegated object.
     */
    public EngineImpl getEngineImpl()
    {
        return m_moduleEngineImpl;
    }

    /**
    * Constructor of ModuleEngineServant
    *
    */
    public ModuleEngineServant()
    {
        this((POA) null);
    }

    /**
    * Constructor of ModuleEngineServant
    *
    */
    public ModuleEngineServant(ModuleEngineImpl an_impl) 
    {
        this(null, an_impl);
    }

    /**
    * Constructor of ModuleEngineServant
    *
    */
    public ModuleEngineServant(POA the_poa)
    {
        this(the_poa, new ModuleEngineImpl());
    }

    /**
    * Constructor of ModuleEngineServant
    *
    */
    public ModuleEngineServant(POA the_poa, ModuleEngineImpl an_impl) 
    {
        m_poa = the_poa;
        m_moduleEngineImpl = an_impl;
    }


    public deaf.EngineHandle getHandle()
       throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.EngineHandle         _result;
        _result = getModuleEngineImpl().getHandle();
        return _result;
    }
    public deaf.Manager getManager()
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.Manager              _result;
        _result = getModuleEngineImpl().getManager().getManagerReference();
        return _result;
    }
    public boolean isIdentical(deaf.Engine engn)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        boolean  _result;
        _result = getModuleEngineImpl().isIdentical(engn);
        return _result;
    }
    public void remove(deaf.SessionToken token)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getModuleEngineImpl().remove(token);
    }
    public void invoke(deaf.DynRequest req, deaf.DynResponseHolder res)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getModuleEngineImpl().invoke(req, res);
    }


    public deaf.admin.AdminManager getAdminManager()
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.admin.AdminManager   _result;
        _result = getModuleEngineImpl().getAdminManager().getAdminManagerReference();
        return _result;
    }
    public deaf.ManagerHandle[] getManagerList(deaf.SessionToken token)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.ManagerHandle[]      _result;
        _result = getModuleEngineImpl().getManagerList(token);
        return _result;
    }

    
    /**
    * Overrides the inherited _default_POA
    * The inherited _default_POA returns the root POA, by overriding it the correct POA is returned.<br>
    */
    public org.omg.PortableServer.POA _default_POA()
    {
        return m_poa;
    }

}



