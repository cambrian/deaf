/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: ModuleManagerServant.java,v 1.4 2001/08/17 06:10:16 hyoon Exp $ 
 */ 
package deaf.app;

import deaf.*;

import org.omg.CORBA.*;
import org.omg.PortableServer.*;


public class ModuleManagerServant
    extends ModuleManagerPOA
{
    org.omg.PortableServer.POA m_poa = null;

    private  ModuleManagerImpl   m_moduleManagerImpl = null;


    /**
     * Returns the server-side implementation object.
     *
     * @return ModuleManagerImpl  the delegated object.
     */
    public ModuleManagerImpl getModuleManagerImpl()
    {
        return m_moduleManagerImpl;
    }

    /**
     * Returns the server-side implementation object.
     *
     * @return ManagerImpl  the delegated object.
     */
    public ManagerImpl getManagerImpl()
    {
        return m_moduleManagerImpl;
    }

    /**
    * Constructor of ModuleManagerServant
    *
    */
    public ModuleManagerServant()
    {
        this((POA) null);
    }

    /**
    * Constructor of ModuleManagerServant
    *
    */
    public ModuleManagerServant(ModuleManagerImpl an_impl) 
    {
        this(null, an_impl);
    }

    /**
    * Constructor of ModuleManagerServant
    *
    */
    public ModuleManagerServant(POA the_poa)
    {
        this(the_poa, new ModuleManagerImpl());
    }

    /**
    * Constructor of ModuleManagerServant
    *
    */
    public ModuleManagerServant(POA the_poa, ModuleManagerImpl an_impl) 
    {
        m_poa = the_poa;
        m_moduleManagerImpl = an_impl;
    }

    public deaf.Service getService()
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.Service              _result;
        _result = getModuleManagerImpl().getService().getServiceReference();
        return _result;
    }
    public deaf.ManagerHandle getHandle()
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.ManagerHandle        _result;
        _result = getModuleManagerImpl().getHandle();
        return _result;
    }
    public deaf.EngineHandle createEngine(deaf.SessionToken token)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.EngineHandle         _result;
        _result = getModuleManagerImpl().createEngine(token);
        return _result;
    }
    public deaf.Engine getEngine(deaf.SessionToken token, deaf.EngineHandle hand)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.Engine               _result;
        _result = getModuleManagerImpl().getEngine(token, hand).getEngineReference();
        return _result;
    }
    public deaf.EngineHandle[] getEngineList(deaf.SessionToken token)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.EngineHandle[]       _result;
        _result = getModuleManagerImpl().getEngineList(token);
        return _result;
    }
    public void removeEngine(deaf.SessionToken token, deaf.EngineHandle hand)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getModuleManagerImpl().removeEngine(token, hand);
    }
    public deaf.ManagerPolicy policy()
    {
        deaf.ManagerPolicy        _result;
        _result = getModuleManagerImpl().policy();
        return _result;
    }

    public int version()
    {
        int                             _result;
        _result = getModuleManagerImpl().version();
        return _result;
    }

    
    /**
    * Overrides the inherited _default_POA
    * The inherited _default_POA returns the root POA, by overriding it the correct POA is returned.<br>
    */
    public org.omg.PortableServer.POA _default_POA()
    {
        return m_poa;
    }

}



