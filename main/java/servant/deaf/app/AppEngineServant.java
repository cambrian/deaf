/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: AppEngineServant.java,v 1.4 2001/08/17 06:10:16 hyoon Exp $ 
 */ 
package deaf.app;

import deaf.*;

import org.omg.CORBA.*;
import org.omg.PortableServer.*;


public class AppEngineServant
    extends AppEnginePOA
{
    org.omg.PortableServer.POA m_poa = null;

    private  AppEngineImpl   m_appEngineImpl = null;


    /**
     * Returns the server-side implementation object.
     *
     * @return AppEngineImpl  the delegated object.
     */
    public AppEngineImpl getAppEngineImpl()
    {
        return m_appEngineImpl;
    }

    /**
     * Returns the server-side implementation object.
     *
     * @return EngineImpl  the delegated object.
     */
    public EngineImpl getEngineImpl()
    {
        return m_appEngineImpl;
    }

    /**
    * Constructor of AppEngineServant
    *
    */
    public AppEngineServant()
    {
        this((POA) null);
    }

    /**
    * Constructor of AppEngineServant
    *
    */
    public AppEngineServant(AppEngineImpl an_impl) 
    {
        this(null, an_impl);
    }

    /**
    * Constructor of AppEngineServant
    *
    */
    public AppEngineServant(POA the_poa)
    {
        this(the_poa, new AppEngineImpl());
    }

    /**
    * Constructor of AppEngineServant
    *
    */
    public AppEngineServant(POA the_poa, AppEngineImpl an_impl) 
    {
        m_poa = the_poa;
        m_appEngineImpl = an_impl;
    }


    public deaf.EngineHandle getHandle()
       throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.EngineHandle         _result;
        _result = getAppEngineImpl().getHandle();
        return _result;
    }
    public deaf.Manager getManager()
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.Manager              _result;
        _result = getAppEngineImpl().getManager().getManagerReference();
        return _result;
    }
    public boolean isIdentical(deaf.Engine engn)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        boolean  _result;
        _result = getAppEngineImpl().isIdentical(engn);
        return _result;
    }
    public void remove(deaf.SessionToken token)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getAppEngineImpl().remove(token);
    }
    public void invoke(deaf.DynRequest req, deaf.DynResponseHolder res)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getAppEngineImpl().invoke(req, res);
    }


    public deaf.app.ConfigManager getConfigManager()
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.app.ConfigManager    _result;
        _result = getAppEngineImpl().getConfigManager().getConfigManagerReference();
        return _result;
    }
    public deaf.admin.LocatorManager getLocatorManager()
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.admin.LocatorManager _result;
        _result = getAppEngineImpl().getLocatorManager().getLocatorManagerReference();
        return _result;
    }
    public deaf.client.ClientManager getDefaultClient()
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.client.ClientManager _result;
        _result = getAppEngineImpl().getDefaultClient().getClientManagerReference();
        return _result;
    }
    public deaf.client.ClientManager getClient(deaf.ManagerHandle hand)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.client.ClientManager _result;
        _result = getAppEngineImpl().getClient(hand).getClientManagerReference();
        return _result;
    }
    public deaf.ManagerHandle[] getClientList()
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.ManagerHandle[]      _result;
        _result = getAppEngineImpl().getClientList();
        return _result;
    }
    public deaf.app.ModuleManager getModule(deaf.SessionToken token, deaf.ManagerHandle  hand)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.app.ModuleManager    _result;
        _result = getAppEngineImpl().getModule(token, hand).getModuleManagerReference();
        return _result;
    }
    public deaf.ManagerHandle[] getModuleList(deaf.SessionToken token)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.ManagerHandle[]      _result;
        _result = getAppEngineImpl().getModuleList(token);
        return _result;
    }
    public void startModule(deaf.SessionToken token, deaf.ManagerHandle hand)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getAppEngineImpl().startModule(token, hand);
    }
    public void shutdownAllModules(deaf.SessionToken token)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getAppEngineImpl().shutdownAllModules(token);
    }
    public void shutdownModule(deaf.SessionToken token, deaf.ManagerHandle hand)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getAppEngineImpl().shutdownModule(token, hand);
    }

    /**
    * Overrides the inherited _default_POA
    * The inherited _default_POA returns the root POA, by overriding it the correct POA is returned.<br>
    */
    public org.omg.PortableServer.POA _default_POA()
    {
        return m_poa;
    }

}



