/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: ConfigEngineServant.java,v 1.4 2001/08/17 06:10:16 hyoon Exp $ 
 */ 
package deaf.app;

import deaf.*;

import org.omg.CORBA.*;
import org.omg.PortableServer.*;


public class ConfigEngineServant
    extends ConfigEnginePOA
{
    org.omg.PortableServer.POA m_poa = null;

    private  ConfigEngineImpl   m_configEngineImpl = null;


    /**
     * Returns the server-side implementation object.
     *
     * @return ConfigEngineImpl  the delegated object.
     */
    public ConfigEngineImpl getConfigEngineImpl()
    {
        return m_configEngineImpl;
    }

    /**
     * Returns the server-side implementation object.
     *
     * @return EngineImpl  the delegated object.
     */
    public EngineImpl getEngineImpl()
    {
        return m_configEngineImpl;
    }

    /**
    * Constructor of ConfigEngineServant
    *
    */
    public ConfigEngineServant()
    {
        this((POA) null);
    }

    /**
    * Constructor of ConfigEngineServant
    *
    */
    public ConfigEngineServant(ConfigEngineImpl an_impl) 
    {
        this(null, an_impl);
    }

    /**
    * Constructor of ConfigEngineServant
    *
    */
    public ConfigEngineServant(POA the_poa)
    {
        this(the_poa, new ConfigEngineImpl());
    }

    /**
    * Constructor of ConfigEngineServant
    *
    */
    public ConfigEngineServant(POA the_poa, ConfigEngineImpl an_impl) 
    {
        m_poa = the_poa;
        m_configEngineImpl = an_impl;
    }


    public deaf.EngineHandle getHandle()
       throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.EngineHandle         _result;
        _result = getConfigEngineImpl().getHandle();
        return _result;
    }
    public deaf.Manager getManager()
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.Manager              _result;
        _result = getConfigEngineImpl().getManager().getManagerReference();
        return _result;
    }
    public boolean isIdentical(deaf.Engine engn)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        boolean  _result;
        _result = getConfigEngineImpl().isIdentical(engn);
        return _result;
    }
    public void remove(deaf.SessionToken token)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getConfigEngineImpl().remove(token);
    }
    public void invoke(deaf.DynRequest req, deaf.DynResponseHolder res)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getConfigEngineImpl().invoke(req, res);
    }



    public deaf.admin.PropManager getPropManager()
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.admin.PropManager    _result;
        _result = getConfigEngineImpl().getPropManager().getPropManagerReference();
        return _result;
    }
    public deaf.ManagerHandle[] listAllModules(deaf.SessionToken token)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.ManagerHandle[]      _result;
        _result = getConfigEngineImpl().listAllModules(token);
        return _result;
    }
    public void registerModule(deaf.SessionToken token, deaf.ManagerHandle hand)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getConfigEngineImpl().registerModule(token, hand);
    }
    public void unregisterModule(deaf.SessionToken token, deaf.ManagerHandle hand)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getConfigEngineImpl().unregisterModule(token, hand);
    }

    
    /**
    * Overrides the inherited _default_POA
    * The inherited _default_POA returns the root POA, by overriding it the correct POA is returned.<br>
    */
    public org.omg.PortableServer.POA _default_POA()
    {
        return m_poa;
    }

}



