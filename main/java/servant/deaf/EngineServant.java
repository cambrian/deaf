/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: EngineServant.java,v 1.4 2001/08/17 06:10:15 hyoon Exp $ 
 */ 
package deaf;

import org.omg.CORBA.*;
import org.omg.PortableServer.*;


public class EngineServant
    extends EnginePOA
{
    org.omg.PortableServer.POA m_poa = null;

    private  EngineImpl   m_engineImpl = null;


    /**
     * Returns the server-side implementation object.
     *
     * @return EngineImpl  the delegated object.
     */
    public EngineImpl getEngineImpl()
    {
        return m_engineImpl;
    }

    /**
    * Constructor of EngineServant
    *
    */
    public EngineServant()
    {
        this((POA) null);
    }

    /**
    * Constructor of EngineServant
    *
    */
    public EngineServant(EngineImpl an_impl) 
    {
        this(null, an_impl);
    }

    /**
    * Constructor of EngineServant
    *
    */
    public EngineServant(POA the_poa)
    {
        this(the_poa, new EngineImpl());
    }

    /**
    * Constructor of EngineServant
    *
    */
    public EngineServant(POA the_poa, EngineImpl an_impl) 
    {
        m_poa = the_poa;
        m_engineImpl = an_impl;
    }


    public deaf.EngineHandle getHandle()
       throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.EngineHandle         _result;
        _result = getEngineImpl().getHandle();
        return _result;
    }
    public deaf.Manager getManager()
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.Manager              _result;
        _result = getEngineImpl().getManager().getManagerReference();
        return _result;
    }
    public boolean isIdentical(deaf.Engine engn)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        boolean  _result;
        _result = getEngineImpl().isIdentical(engn);
        return _result;
    }
    public void remove(deaf.SessionToken token)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getEngineImpl().remove(token);
    }
    public void invoke(deaf.DynRequest req, deaf.DynResponseHolder res)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getEngineImpl().invoke(req, res);
    }

    
    /**
    * Overrides the inherited _default_POA
    * The inherited _default_POA returns the root POA, by overriding it the correct POA is returned.<br>
    */
    public org.omg.PortableServer.POA _default_POA()
    {
        return m_poa;
    }

}



