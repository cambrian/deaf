/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: RepoEngineServant.java,v 1.4 2001/08/17 06:10:16 hyoon Exp $ 
 */ 
package deaf.persist;

import deaf.*;

import org.omg.CORBA.*;
import org.omg.PortableServer.*;


public class RepoEngineServant
    extends RepoEnginePOA
{
    org.omg.PortableServer.POA m_poa = null;

    private  RepoEngineImpl   m_repoEngineImpl = null;


    /**
     * Returns the server-side implementation object.
     *
     * @return RepoEngineImpl  the delegated object.
     */
    public RepoEngineImpl getRepoEngineImpl()
    {
        return m_repoEngineImpl;
    }

    /**
     * Returns the server-side implementation object.
     *
     * @return EngineImpl  the delegated object.
     */
    public EngineImpl getEngineImpl()
    {
        return m_repoEngineImpl;
    }

    /**
    * Constructor of RepoEngineServant
    *
    */
    public RepoEngineServant()
    {
        this((POA) null);
    }

    /**
    * Constructor of RepoEngineServant
    *
    */
    public RepoEngineServant(RepoEngineImpl an_impl) 
    {
        this(null, an_impl);
    }

    /**
    * Constructor of RepoEngineServant
    *
    */
    public RepoEngineServant(POA the_poa)
    {
        this(the_poa, new RepoEngineImpl());
    }

    /**
    * Constructor of RepoEngineServant
    *
    */
    public RepoEngineServant(POA the_poa, RepoEngineImpl an_impl) 
    {
        m_poa = the_poa;
        m_repoEngineImpl = an_impl;
    }


    public deaf.EngineHandle getHandle()
       throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.EngineHandle         _result;
        _result = getRepoEngineImpl().getHandle();
        return _result;
    }
    public deaf.Manager getManager()
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.Manager              _result;
        _result = getRepoEngineImpl().getManager().getManagerReference();
        return _result;
    }
    public boolean isIdentical(deaf.Engine engn)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        boolean  _result;
        _result = getRepoEngineImpl().isIdentical(engn);
        return _result;
    }
    public void remove(deaf.SessionToken token)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getRepoEngineImpl().remove(token);
    }
    public void invoke(deaf.DynRequest req, deaf.DynResponseHolder res)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getRepoEngineImpl().invoke(req, res);
    }


    /**
    * Overrides the inherited _default_POA
    * The inherited _default_POA returns the root POA, by overriding it the correct POA is returned.<br>
    */
    public org.omg.PortableServer.POA _default_POA()
    {
        return m_poa;
    }

}



