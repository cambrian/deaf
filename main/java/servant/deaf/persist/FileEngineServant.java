/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: FileEngineServant.java,v 1.4 2001/08/17 06:10:16 hyoon Exp $ 
 */ 
package deaf.persist;

import deaf.*;

import org.omg.CORBA.*;
import org.omg.PortableServer.*;


public class FileEngineServant
    extends FileEnginePOA
{
    org.omg.PortableServer.POA m_poa = null;

    private  FileEngineImpl   m_fileEngineImpl = null;


    /**
     * Returns the server-side implementation object.
     *
     * @return FileEngineImpl  the delegated object.
     */
    public FileEngineImpl getFileEngineImpl()
    {
        return m_fileEngineImpl;
    }

    /**
     * Returns the server-side implementation object.
     *
     * @return EngineImpl  the delegated object.
     */
    public EngineImpl getEngineImpl()
    {
        return m_fileEngineImpl;
    }

    /**
    * Constructor of FileEngineServant
    *
    */
    public FileEngineServant()
    {
        this((POA) null);
    }

    /**
    * Constructor of FileEngineServant
    *
    */
    public FileEngineServant(FileEngineImpl an_impl) 
    {
        this(null, an_impl);
    }

    /**
    * Constructor of FileEngineServant
    *
    */
    public FileEngineServant(POA the_poa)
    {
        this(the_poa, new FileEngineImpl());
    }

    /**
    * Constructor of FileEngineServant
    *
    */
    public FileEngineServant(POA the_poa, FileEngineImpl an_impl) 
    {
        m_poa = the_poa;
        m_fileEngineImpl = an_impl;
    }


    public deaf.EngineHandle getHandle()
       throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.EngineHandle         _result;
        _result = getFileEngineImpl().getHandle();
        return _result;
    }
    public deaf.Manager getManager()
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.Manager              _result;
        _result = getFileEngineImpl().getManager().getManagerReference();
        return _result;
    }
    public boolean isIdentical(deaf.Engine engn)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        boolean  _result;
        _result = getFileEngineImpl().isIdentical(engn);
        return _result;
    }
    public void remove(deaf.SessionToken token)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getFileEngineImpl().remove(token);
    }
    public void invoke(deaf.DynRequest req, deaf.DynResponseHolder res)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getFileEngineImpl().invoke(req, res);
    }


    /**
    * Overrides the inherited _default_POA
    * The inherited _default_POA returns the root POA, by overriding it the correct POA is returned.<br>
    */
    public org.omg.PortableServer.POA _default_POA()
    {
        return m_poa;
    }

}



