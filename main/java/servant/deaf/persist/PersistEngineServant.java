/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: PersistEngineServant.java,v 1.4 2001/08/17 06:10:16 hyoon Exp $ 
 */ 
package deaf.persist;

import deaf.*;

import org.omg.CORBA.*;
import org.omg.PortableServer.*;


public class PersistEngineServant
    extends PersistEnginePOA
{
    org.omg.PortableServer.POA m_poa = null;

    private  PersistEngineImpl   m_persistEngineImpl = null;


    /**
     * Returns the server-side implementation object.
     *
     * @return PersistEngineImpl  the delegated object.
     */
    public PersistEngineImpl getPersistEngineImpl()
    {
        return m_persistEngineImpl;
    }

    /**
     * Returns the server-side implementation object.
     *
     * @return EngineImpl  the delegated object.
     */
    public EngineImpl getEngineImpl()
    {
        return m_persistEngineImpl;
    }

    /**
    * Constructor of PersistEngineServant
    *
    */
    public PersistEngineServant()
    {
        this((POA) null);
    }

    /**
    * Constructor of PersistEngineServant
    *
    */
    public PersistEngineServant(PersistEngineImpl an_impl) 
    {
        this(null, an_impl);
    }

    /**
    * Constructor of PersistEngineServant
    *
    */
    public PersistEngineServant(POA the_poa)
    {
        this(the_poa, new PersistEngineImpl());
    }

    /**
    * Constructor of PersistEngineServant
    *
    */
    public PersistEngineServant(POA the_poa, PersistEngineImpl an_impl) 
    {
        m_poa = the_poa;
        m_persistEngineImpl = an_impl;
    }


    public deaf.EngineHandle getHandle()
       throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.EngineHandle         _result;
        _result = getPersistEngineImpl().getHandle();
        return _result;
    }
    public deaf.Manager getManager()
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.Manager              _result;
        _result = getPersistEngineImpl().getManager().getManagerReference();
        return _result;
    }
    public boolean isIdentical(deaf.Engine engn)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        boolean  _result;
        _result = getPersistEngineImpl().isIdentical(engn);
        return _result;
    }
    public void remove(deaf.SessionToken token)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getPersistEngineImpl().remove(token);
    }
    public void invoke(deaf.DynRequest req, deaf.DynResponseHolder res)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getPersistEngineImpl().invoke(req, res);
    }


    /**
    * Overrides the inherited _default_POA
    * The inherited _default_POA returns the root POA, by overriding it the correct POA is returned.<br>
    */
    public org.omg.PortableServer.POA _default_POA()
    {
        return m_poa;
    }

}



