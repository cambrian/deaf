/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: RepoManagerServant.java,v 1.4 2001/08/17 06:10:16 hyoon Exp $ 
 */ 
package deaf.persist;

import deaf.*;

import org.omg.CORBA.*;
import org.omg.PortableServer.*;


public class RepoManagerServant
    extends RepoManagerPOA
{
    org.omg.PortableServer.POA m_poa = null;

    private  RepoManagerImpl   m_repoManagerImpl = null;


    /**
     * Returns the server-side implementation object.
     *
     * @return RepoManagerImpl  the delegated object.
     */
    public RepoManagerImpl getRepoManagerImpl()
    {
        return m_repoManagerImpl;
    }

    /**
     * Returns the server-side implementation object.
     *
     * @return ManagerImpl  the delegated object.
     */
    public ManagerImpl getManagerImpl()
    {
        return m_repoManagerImpl;
    }

    /**
    * Constructor of RepoManagerServant
    *
    */
    public RepoManagerServant()
    {
        this((POA) null);
    }

    /**
    * Constructor of RepoManagerServant
    *
    */
    public RepoManagerServant(RepoManagerImpl an_impl) 
    {
        this(null, an_impl);
    }

    /**
    * Constructor of RepoManagerServant
    *
    */
    public RepoManagerServant(POA the_poa)
    {
        this(the_poa, new RepoManagerImpl());
    }

    /**
    * Constructor of RepoManagerServant
    *
    */
    public RepoManagerServant(POA the_poa, RepoManagerImpl an_impl) 
    {
        m_poa = the_poa;
        m_repoManagerImpl = an_impl;
    }



    public deaf.Service getService()
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.Service              _result;
        _result = getRepoManagerImpl().getService().getServiceReference();
        return _result;
    }
    public deaf.ManagerHandle getHandle()
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.ManagerHandle        _result;
        _result = getRepoManagerImpl().getHandle();
        return _result;
    }
    public deaf.EngineHandle createEngine(deaf.SessionToken token)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.EngineHandle         _result;
        _result = getRepoManagerImpl().createEngine(token);
        return _result;
    }
    public deaf.Engine getEngine(deaf.SessionToken token, deaf.EngineHandle hand)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.Engine               _result;
        _result = getRepoManagerImpl().getEngine(token, hand).getEngineReference();
        return _result;
    }
    public deaf.EngineHandle[] getEngineList(deaf.SessionToken token)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.EngineHandle[]       _result;
        _result = getRepoManagerImpl().getEngineList(token);
        return _result;
    }
    public void removeEngine(deaf.SessionToken token, deaf.EngineHandle hand)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getRepoManagerImpl().removeEngine(token, hand);
    }
    public deaf.ManagerPolicy policy()
    {
        deaf.ManagerPolicy        _result;
        _result = getRepoManagerImpl().policy();
        return _result;
    }

    /**
    * Overrides the inherited _default_POA
    * The inherited _default_POA returns the root POA, by overriding it the correct POA is returned.<br>
    */
    public org.omg.PortableServer.POA _default_POA()
    {
        return m_poa;
    }

}



