/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: DataEngineServant.java,v 1.4 2001/08/17 06:10:16 hyoon Exp $ 
 */ 
package deaf.persist;

import deaf.*;

import org.omg.CORBA.*;
import org.omg.PortableServer.*;


public class DataEngineServant
    extends DataEnginePOA
{
    org.omg.PortableServer.POA m_poa = null;

    private  DataEngineImpl   m_dataEngineImpl = null;


    /**
     * Returns the server-side implementation object.
     *
     * @return DataEngineImpl  the delegated object.
     */
    public DataEngineImpl getDataEngineImpl()
    {
        return m_dataEngineImpl;
    }

    /**
     * Returns the server-side implementation object.
     *
     * @return EngineImpl  the delegated object.
     */
    public EngineImpl getEngineImpl()
    {
        return m_dataEngineImpl;
    }

    /**
    * Constructor of DataEngineServant
    *
    */
    public DataEngineServant()
    {
        this((POA) null);
    }

    /**
    * Constructor of DataEngineServant
    *
    */
    public DataEngineServant(DataEngineImpl an_impl) 
    {
        this(null, an_impl);
    }

    /**
    * Constructor of DataEngineServant
    *
    */
    public DataEngineServant(POA the_poa)
    {
        this(the_poa, new DataEngineImpl());
    }

    /**
    * Constructor of DataEngineServant
    *
    */
    public DataEngineServant(POA the_poa, DataEngineImpl an_impl) 
    {
        m_poa = the_poa;
        m_dataEngineImpl = an_impl;
    }


    public deaf.EngineHandle getHandle()
       throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.EngineHandle         _result;
        _result = getDataEngineImpl().getHandle();
        return _result;
    }
    public deaf.Manager getManager()
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        deaf.Manager              _result;
        _result = getDataEngineImpl().getManager().getManagerReference();
        return _result;
    }
    public boolean isIdentical(deaf.Engine engn)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        boolean  _result;
        _result = getDataEngineImpl().isIdentical(engn);
        return _result;
    }
    public void remove(deaf.SessionToken token)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getDataEngineImpl().remove(token);
    }
    public void invoke(deaf.DynRequest req, deaf.DynResponseHolder res)
        throws org.omg.CORBA.SystemException, deaf.AppException
    {
        getDataEngineImpl().invoke(req, res);
    }


    /**
    * Overrides the inherited _default_POA
    * The inherited _default_POA returns the root POA, by overriding it the correct POA is returned.<br>
    */
    public org.omg.PortableServer.POA _default_POA()
    {
        return m_poa;
    }

}



