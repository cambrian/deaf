/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: PropManagerServantActivator.java,v 1.3 2001/08/13 06:09:03 hyoon Exp $ 
 */ 
package deaf.admin;

import  deaf.util.*;

import  java.io.*;
import  java.util.*;
 
import  org.apache.log4j.*;
import  org.omg.CORBA.*;
import  org.omg.CORBA.portable.*;
import  org.omg.PortableServer.*;
import  org.omg.PortableServer.POA.*;


/**
 * PropManagerServantActivator
 *
 * @version $Revision: 1.3 $
 * @author Hyoungsoo Yoon
 */
public class PropManagerServantActivator
    extends LocalObject
    implements ServantActivator
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(PropManagerServantActivator.class);
    }

    private PropManagerImpl  m_propManagerImpl;
    private org.omg.CORBA.ORB m_orb;

    public PropManagerServantActivator(PropManagerImpl mImpl,
                                  org.omg.CORBA.ORB orb)
    {
        m_propManagerImpl = mImpl;
        m_orb = orb;
    }

  
    public Servant incarnate(byte[] oid,
                             POA the_poa)
        throws ForwardRequest
    {
        String propManagerImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Incarnating propManagerImpl id [" + propManagerImpl_id + "]");

        if(m_propManagerImpl == null) {
            m_propManagerImpl = new PropManagerImpl();
        }
        PropManagerServant mServant = new PropManagerServant(the_poa, m_propManagerImpl);

        return mServant;
    }

    public void etherealize(byte[] oid,
                            org.omg.PortableServer.POA the_poa,
                            org.omg.PortableServer.Servant the_servant,
                            boolean cleanup_in_progress,
                            boolean remaining_activations)
    {
        String propManagerImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Etherializing propManagerImpl id [" + propManagerImpl_id + "]");

        if (the_servant instanceof PropManagerServant) {
            PropManagerServant mServant = (PropManagerServant) the_servant;
            //mServant.cleanup();
        }
    }

}







