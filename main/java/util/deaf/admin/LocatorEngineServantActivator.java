/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: LocatorEngineServantActivator.java,v 1.3 2001/08/13 06:09:03 hyoon Exp $ 
 */ 
package deaf.admin;

import  deaf.util.*;

import  java.io.*;
import  java.util.*;
 
import  org.apache.log4j.*;
import  org.omg.CORBA.*;
import  org.omg.CORBA.portable.*;
import  org.omg.PortableServer.*;
import  org.omg.PortableServer.POA.*;


/**
 * LocatorEngineServantActivator
 *
 * @version $Revision: 1.3 $
 * @author Hyoungsoo Yoon
 */
public class LocatorEngineServantActivator
    extends LocalObject
    implements ServantActivator
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(LocatorEngineServantActivator.class);
    }

    private LocatorEngineImpl  m_locatorEngineImpl;
    private org.omg.CORBA.ORB m_orb;

    public LocatorEngineServantActivator(LocatorEngineImpl eImpl,
                                  org.omg.CORBA.ORB orb)
    {
        m_locatorEngineImpl = eImpl;
        m_orb = orb;
    }

  
    public Servant incarnate(byte[] oid,
                             POA the_poa)
        throws ForwardRequest
    {
        String locatorEngineImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Incarnating locatorEngineImpl id [" + locatorEngineImpl_id + "]");

        if(m_locatorEngineImpl == null) {
            m_locatorEngineImpl = new LocatorEngineImpl();
        }
        LocatorEngineServant eServant = new LocatorEngineServant(the_poa, m_locatorEngineImpl);

        return eServant;
    }

    public void etherealize(byte[] oid,
                            org.omg.PortableServer.POA the_poa,
                            org.omg.PortableServer.Servant the_servant,
                            boolean cleanup_in_progress,
                            boolean remaining_activations)
    {
        String locatorEngineImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Etherealizing locatorEngineImpl id [" + locatorEngineImpl_id +"]");

        if (the_servant instanceof LocatorEngineServant) {
            LocatorEngineServant eServant = (LocatorEngineServant) the_servant;
            //eServant.cleanup();
        }
    }

}







