/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: LogManagerServantActivator.java,v 1.3 2001/08/13 06:09:03 hyoon Exp $ 
 */ 
package deaf.admin;

import  deaf.util.*;

import  java.io.*;
import  java.util.*;
 
import  org.apache.log4j.*;
import  org.omg.CORBA.*;
import  org.omg.CORBA.portable.*;
import  org.omg.PortableServer.*;
import  org.omg.PortableServer.POA.*;


/**
 * LogManagerServantActivator
 *
 * @version $Revision: 1.3 $
 * @author Hyoungsoo Yoon
 */
public class LogManagerServantActivator
    extends LocalObject
    implements ServantActivator
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(LogManagerServantActivator.class);
    }

    private LogManagerImpl  m_logManagerImpl;
    private org.omg.CORBA.ORB m_orb;

    public LogManagerServantActivator(LogManagerImpl mImpl,
                                  org.omg.CORBA.ORB orb)
    {
        m_logManagerImpl = mImpl;
        m_orb = orb;
    }

  
    public Servant incarnate(byte[] oid,
                             POA the_poa)
        throws ForwardRequest
    {
        String logManagerImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Incarnating logManagerImpl id [" + logManagerImpl_id + "]");

        if(m_logManagerImpl == null) {
            m_logManagerImpl = new LogManagerImpl();
        }
        LogManagerServant mServant = new LogManagerServant(the_poa, m_logManagerImpl);

        return mServant;
    }

    public void etherealize(byte[] oid,
                            org.omg.PortableServer.POA the_poa,
                            org.omg.PortableServer.Servant the_servant,
                            boolean cleanup_in_progress,
                            boolean remaining_activations)
    {
        String logManagerImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Etherializing logManagerImpl id [" + logManagerImpl_id + "]");

        if (the_servant instanceof LogManagerServant) {
            LogManagerServant mServant = (LogManagerServant) the_servant;
            //mServant.cleanup();
        }
    }

}







