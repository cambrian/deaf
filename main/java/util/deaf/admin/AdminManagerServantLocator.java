/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: AdminManagerServantLocator.java,v 1.3 2001/08/13 06:09:03 hyoon Exp $ 
 */ 
package deaf.admin;

import  deaf.util.*;
 
import  java.io.*;
import  java.util.*;
 
import  org.apache.log4j.*;
import  org.omg.CORBA.*;
import  org.omg.CORBA.portable.*;
import  org.omg.PortableServer.*;
import  org.omg.PortableServer.POA.*;
import  org.omg.PortableServer.ServantLocatorPackage.*;


/**
 * AdminManagerServantLocator
 *
 * @version $Revision: 1.3 $
 * @author Hyoungsoo Yoon
 */
public class AdminManagerServantLocator
    extends LocalObject
    implements ServantLocator
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(AdminManagerServantLocator.class);
    }

    private AdminManagerImpl m_adminManagerImpl;
    private org.omg.CORBA.ORB m_orb;
    
    public AdminManagerServantLocator(AdminManagerImpl mImpl,
                                org.omg.CORBA.ORB orb)
    {
        m_adminManagerImpl = mImpl;
        m_orb = orb;
    }
  
    public org.omg.PortableServer.Servant preinvoke(byte[] oid,
                                                    POA the_poa,
                                                    String operation,
                                                    CookieHolder the_cookie)
        throws ForwardRequest
    {
        String adminManagerImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Preinvoke called for adminManagerImpl id [" + adminManagerImpl_id + "]");

        if(m_adminManagerImpl == null) {
            m_adminManagerImpl = new AdminManagerImpl();
        }
        AdminManagerServant mServant = new AdminManagerServant(the_poa, m_adminManagerImpl);
        return mServant;
    }
  

    public void postinvoke(byte[] oid,
                           POA the_poa,
                           String operation,
                           java.lang.Object the_cookie,
                           org.omg.PortableServer.Servant the_servant)
    {
        String adminManagerImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Postinvoke called for adminManagerImpl id [" + adminManagerImpl_id + "]");

        if (the_servant instanceof AdminManagerServant) {
            AdminManagerServant mServant = (AdminManagerServant) the_servant;
            //mServant.cleanup();
        }
    }

}






