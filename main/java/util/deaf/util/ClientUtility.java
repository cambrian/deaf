/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: ClientUtility.java,v 1.3 2001/08/13 06:09:03 hyoon Exp $ 
 */ 
package deaf.util;

import  deaf.*;
 
import  java.io.*;
import  java.util.*;
 
import  org.apache.log4j.*;
import  org.omg.CORBA.*;


/**
 * ClientUtility is a utility class for client-related functions.
 *
 * @version $Revision: 1.3 $
 * @author Hyoungsoo Yoon
 */
public class ClientUtility implements deaf.CommonConstants
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(ClientUtility.class);
    }


    /**
     * Prevents the instantiation of this class.
     */
    private ClientUtility()
    {
    }

    /**
     * reads and destringifies an object reference stored in a file.
     */
    public static org.omg.CORBA.Object import_object(ORB orb, String filename) 
    {
        String ior = null;
        RandomAccessFile FileStream = null;
    
        try 
        {
            logCat.debug("Reading object reference from " + filename);
    
            FileStream = new RandomAccessFile(filename,"r");
            ior = FileStream.readLine();
            return orb.string_to_object(ior);
        }
        catch(Exception e)
        {
            logCat.error("Error opening file " + filename);
            return null;
        }
    }


    /**
     * reads and destringifies an object reference stored in a file.
     */
    public static org.omg.CORBA.Object resolve_object_name(
        org.omg.CosNaming.NamingContextExt default_context,
        org.omg.CosNaming.NamingContext app_context,
        String strObjectName) 
    {
        try 
        {
            logCat.debug("Resolving object name " + strObjectName);
    
            org.omg.CosNaming.NameComponent [] name;
            name = default_context.to_name(strObjectName);
            return app_context.resolve(name);
        }
        catch(Exception e)
        {
            logCat.error("Error resolving object name " + strObjectName);
            return null;
        }
    }


    /**
     * Returns the commnad line argument for the given option.
     * E.g. If args = ".. -a abc .."
     * Then, parseArgs(args, "-a") returns "abc".
     */
    public static String parseArgs(String[] args, String flag)
    {
        for(int i=0;i<args.length-1;i++) {
            if(args[i].equals(flag)) {
                return args[i+1];
            }
        }
        return null;
    }

}




