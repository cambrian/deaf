/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: DebugTrace.java,v 1.3 2001/08/13 06:09:03 hyoon Exp $ 
 */ 
package deaf.util;

import  java.io.*;
import  java.util.*;


/**
 * DebugTrace is a temporary class to print out debug information.
 *
 * @version $Revision: 1.3 $
 * @author Hyoungsoo Yoon
 */
public class DebugTrace
{
    private static boolean  s_debug = true;
    private static String   DEBUG_PREFIX = "[Debug] ";
    
    public static void setDebug(boolean bDebug)
    {
        s_debug = bDebug;
    }
    
    public static void assert(boolean bExp)
    {
        if(s_debug && !bExp) {
            //System.err.println(DEBUG_PREFIX + "Assertion failed!");
            throw new RuntimeException(DEBUG_PREFIX + "Assertion failed!");
        }
    }
    
    public static void println(String phrase)
    {
        if(s_debug) {
            System.err.println(DEBUG_PREFIX + phrase);
        }
    }

}




