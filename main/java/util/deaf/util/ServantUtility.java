/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: ServantUtility.java,v 1.3 2001/08/13 06:09:03 hyoon Exp $ 
 */ 
package deaf.util;

import  java.io.*;
import  java.util.*;
 
import  org.omg.CORBA.*;
import  org.omg.CORBA.portable.*;
import  org.omg.PortableServer.*;
import  org.omg.PortableServer.POA.*;
import  org.omg.PortableServer.POAPackage.*;
import  org.omg.PortableServer.ServantLocatorPackage.*;


/**
 * ServantUtility
 *
 * @version $Revision: 1.3 $
 * @author Hyoungsoo Yoon
 */
public class ServantUtility
{
    public static POA create_poa (POA         root_poa,
                                  String      adapter_name,
                                  POAManager  root_manager,
                                  Policy[]    policies)
    {
        try {
            return root_poa.create_POA(adapter_name, root_manager, policies);
        } catch (AdapterAlreadyExists already_exists) {
            DebugTrace.println("Adapter already exists.");
            return null;
        } catch (InvalidPolicy invalid_policy) {
            DebugTrace.println("Adapter invalid policy.");
            return null;
        }
    }


    public static POA create_simple_poa (POA         root_poa,
                                         String      adapter_name,
                                         POAManager  root_manager)
    {
        Policy policies[] = new Policy[2];
        policies[0] =
            root_poa.create_lifespan_policy(LifespanPolicyValue.PERSISTENT);
        policies[1] =
            root_poa.create_id_assignment_policy(IdAssignmentPolicyValue.USER_ID);

        return create_poa(root_poa, adapter_name, root_manager, policies);
    }

    public static POA create_defaultservant_poa (POA         root_poa,
                                                 String      adapter_name,
                                                 POAManager  root_manager)
    {
        Policy policies[] = new Policy[5];
        policies[0] =
            root_poa.create_lifespan_policy(LifespanPolicyValue.TRANSIENT);
        policies[1] =
            root_poa.create_id_assignment_policy(IdAssignmentPolicyValue.USER_ID);
        policies[2] =
            root_poa.create_servant_retention_policy(ServantRetentionPolicyValue.RETAIN);
        policies[3] =
            root_poa.create_request_processing_policy(RequestProcessingPolicyValue.USE_DEFAULT_SERVANT);
        policies[4] =
            root_poa.create_id_uniqueness_policy(IdUniquenessPolicyValue.MULTIPLE_ID);

        return create_poa(root_poa, adapter_name, root_manager, policies);
    }

    public static POA create_activator_poa (POA         root_poa,
                                            String      adapter_name,
                                            POAManager  root_manager)
    {
        Policy policies[] = new Policy[4];
        policies[0] =
            root_poa.create_lifespan_policy(LifespanPolicyValue.PERSISTENT);
        policies[1] =
            root_poa.create_id_assignment_policy(IdAssignmentPolicyValue.USER_ID);
        policies[2] =
            root_poa.create_servant_retention_policy(ServantRetentionPolicyValue.RETAIN);
        policies[3] =
            root_poa.create_request_processing_policy(RequestProcessingPolicyValue.USE_SERVANT_MANAGER);

        return create_poa(root_poa, adapter_name, root_manager, policies);
    }

    public static POA create_locator_poa (POA         root_poa,
                                          String      adapter_name,
                                          POAManager  root_manager)
    {
        Policy policies[] = new Policy[4];
        policies[0] =
            root_poa.create_lifespan_policy(LifespanPolicyValue.PERSISTENT);
        policies[1] =
            root_poa.create_id_assignment_policy(IdAssignmentPolicyValue.USER_ID);
        policies[2] =
            root_poa.create_servant_retention_policy(ServantRetentionPolicyValue.NON_RETAIN);
        policies[3] =
            root_poa.create_request_processing_policy(RequestProcessingPolicyValue.USE_SERVANT_MANAGER);

        return create_poa(root_poa, adapter_name, root_manager, policies);
    }

}

    










