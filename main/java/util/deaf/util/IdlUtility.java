/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: IdlUtility.java,v 1.3 2001/08/13 06:09:03 hyoon Exp $ 
 */ 
package deaf.util;

import  deaf.*;
import  deaf.app.*;
import  deaf.admin.*;
import  deaf.auth.*;
import  deaf.client.*;
import  deaf.persist.*;

import  java.io.*;
import  java.util.*;
 
import  org.omg.CORBA.*;
import  org.omg.CORBA.portable.*;
import  org.omg.PortableServer.POA;


/**
 * IdlUtility
 *
 * @version $Revision: 1.3 $
 * @author Hyoungsoo Yoon
 */
public class IdlUtility
{
    // Blocks creation of IdlUtility.
    // All methods in IdlUtility are static.
    private IdlUtility()
    {
    }


    /**
     * Returns the corresponding EngineImpl object of the given Engine.
     *
     * @param  engn EngineServant object to be converted.
     * @return EngineImpl the converted EngineImpl object of the given Engine.
     */
    static public EngineImpl convertToImpl(Engine engn)
    {
        if(engn == null) {
            return null;
        } else {
            try {
                if(engn instanceof AppEngine) {
                    //AppEngineServant eServant = new AppEngineServant(engn);
                    //return eServant.getAppEngineImpl();
                    return null;
                } else {
                    //EngineServant eServant = new EngineServant(engn);
                    //return eServant.getEngineImpl();
                    return null;
                }
            } catch (Exception ex) {
                System.out.println(ex);
            }
        }
        return null;
    }

    /**
     * Returns the corresponding EngineProxy object of the given Engine.
     *
     * @param  engn Engine object to be converted.
     * @return EngineProxy the converted EngineProxy object of the given Engine.
     */
    static public EngineProxy convertToProxy(Engine engn)
    {
        if(engn == null) {
            return null;
        } else {
            try {
                if(engn instanceof AppEngine) {
                    return new AppEngineProxy((AppEngine) engn);
                } else {
                    return new EngineProxy(engn);
                }
            } catch (Exception ex) {
                System.out.println(ex);
            }
        }
        return null;
    }

    /**
     * Returns the corresponding Engine object of the given EngineImpl.
     *
     * @param eImpl EngineImpl object to be converted.
     * @return Engine the converted Engine object of the given EngineImpl.
     */
    static public Engine convertFromImpl(EngineImpl eImpl)
    {
        if(eImpl == null) {
            return null;
        } else {
            try {
                if(eImpl instanceof AppEngineImpl) {
                    //AppEngineServant eServant = new AppEngineServant((AppEngineImpl) eImpl);
                    //return eServant;
                    return null;
                } else {
                    //EngineServant eServant = new EngineServant(eImpl);
                    //return eServant;
                    return null;
                }
            } catch (Exception ex) {
                System.out.println(ex);
            }
        }
        return null;
    }

    /**
     * Returns the corresponding Engine object of the given EngineProxy.
     *
     * @param eProxy EngineProxy object to be converted.
     * @return Engine the converted Engine object of the given EngineProxy.
     */
    static public Engine convertFromProxy(EngineProxy eProxy)
    {
        if(eProxy == null) {
            return null;
        } else {
            try {
                if(eProxy instanceof AppEngineProxy) {
                    return ((AppEngineProxy) eProxy).getAppEngineReference();
                } else {
                    return eProxy.getEngineReference();
                }
            } catch (Exception ex) {
                System.out.println(ex);
            }
        }
        return null;
    }


}




