/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: ServerUtility.java,v 1.3 2001/08/13 06:09:03 hyoon Exp $ 
 */ 
package deaf.util;

import  deaf.*;
 
import  java.io.*;
import  java.util.*;
 
import  org.apache.log4j.*;
import  org.omg.CORBA.*;
import  org.omg.CORBA.portable.*;
import  org.omg.PortableServer.*;



/**
 * ServerUtility is a utility class for server-related functions.
 *
 * @version $Revision: 1.3 $
 * @author Hyoungsoo Yoon
 */
public class ServerUtility implements deaf.CommonConstants
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(ServerUtility.class);
    }


    /**
     * Prevents the instantiation of this class.
     */
    private ServerUtility()
    {
    }

    /**
     * This function takes a poa and object id, builds an object
     * reference representing that object, and exports the object
     * reference to a file.
     */
    public static void export_object(ORB orb, POA poa, byte[] oid, String filename, String type_id)
    {
        try 
        {
            org.omg.CORBA.Object ref = poa.create_reference_with_id(oid, type_id);
            String stringified_ref = orb.object_to_string(ref);
      
            RandomAccessFile FileStream = null;
            try
            {
                FileStream = new RandomAccessFile(filename,"rw");
                FileStream.writeBytes(stringified_ref);
                FileStream.close();
            }
            catch(Exception ex)
            {
                logCat.error("Failed to write to " + filename );
            }
        }
        catch(org.omg.PortableServer.POAPackage.WrongPolicy ex)
        {
            logCat.error("Invalid POA policy " + ex );
        }
    } 


    /**
     * create or find application-default naming context.
     * Returns null if it failed to create or obtain valid naming context.
     */
    public static org.omg.CosNaming.NamingContext create_app_context(
        org.omg.CosNaming.NamingContextExt default_context,
        String app_name_string)
    {
        org.omg.CORBA.Object context_as_obj;

        org.omg.CosNaming.NamingContext app_context = null;
        try
        {
            // First see if the name is already bound.
            try 
            {
                context_as_obj = default_context.resolve_str(app_name_string);
            }
            catch (org.omg.CosNaming.NamingContextPackage.NotFound nf)
            {
                // Name is not yet bound, create a new context.
                org.omg.CosNaming.NameComponent [] app_name =
                    default_context.to_name(app_name_string);
                context_as_obj = default_context.bind_new_context(app_name);
            }

            // Narrow the reference to a naming context.
            app_context = org.omg.CosNaming.NamingContextHelper.narrow(context_as_obj);
        }
        catch (org.omg.CosNaming.NamingContextPackage.NotFound ns_nf)
        {
            logCat.error(ns_nf);
        }
        catch (org.omg.CosNaming.NamingContextPackage.InvalidName ns_in)
        {
            logCat.error(ns_in);
        }
        catch (org.omg.CosNaming.NamingContextPackage.AlreadyBound ns_ab)
        {
            logCat.error(ns_ab);
        }
        catch (org.omg.CosNaming.NamingContextPackage.CannotProceed ns_cp)
        {
            logCat.error(ns_cp);
        }
        
        if (app_context==null) {
            logCat.error("Error: Cannot narrow naming context called `"
                               + app_name_string +  "'." );
        }

        return app_context;
    }



    /**
     * Returns the commnad line argument for the given option.
     * E.g. If args = ".. -a abc .."
     * Then, parseArgs(args, "-a") returns "abc".
     */
    public static String parseArgs(String[] args, String flag)
    {
        for(int i=0;i<args.length-1;i++) {
            if(args[i].equals(flag)) {
                return args[i+1];
            }
        }
        return null;
    }


    /**
     * Shuts down the ORB.
     */
    public static void shutdownService(ORB global_orb)
    {
        if (global_orb != null)
        {
            try {
                global_orb.shutdown(true);
            } catch (Exception ex) {
                /* do nothing */
            }
        }
    }
}




