/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: CommonConstants.java,v 1.3 2001/08/13 06:09:03 hyoon Exp $ 
 */ 
package deaf;


/**
 * CommonConstants is a collection of variables commonly used in deaf and its sub-namespaces.
 *
 * @version $Revision: 1.3 $
 * @author Hyoungsoo Yoon
 */
public interface CommonConstants
{

    /**
     * Servant manager strategy: Sevant Activator.
     */
    static final String STRATEGY_ACTIVATOR = "activator";
    
    /**
     * Servant manager strategy: Sevant Locator.
     */
    static final String STRATEGY_LOCATOR = "locator";
    
    /**
     * Servant manager strategy: Sevant Defaultservant.
     */
    static final String STRATEGY_DEFAULTSERVANT = "defaultservant";
    

    /**
     * Whether to use the CORBA naming service: Use it.
     */
    static final String NAMESERVICE_YES = "ns_yes";
    
    /**
     * Whether to use the CORBA naming service: Do not use it.
     */
    static final String NAMESERVICE_NO = "ns_no";
    
}




