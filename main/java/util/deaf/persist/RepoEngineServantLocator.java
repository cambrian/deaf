/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: RepoEngineServantLocator.java,v 1.3 2001/08/13 06:09:03 hyoon Exp $ 
 */ 
package deaf.persist;

import  deaf.util.*;
 
import  java.io.*;
import  java.util.*;
 
import  org.apache.log4j.*;
import  org.omg.CORBA.*;
import  org.omg.CORBA.portable.*;
import  org.omg.PortableServer.*;
import  org.omg.PortableServer.POA.*;
import  org.omg.PortableServer.ServantLocatorPackage.*;


/**
 * RepoEngineServantLocator
 *
 * @version $Revision: 1.3 $
 * @author Hyoungsoo Yoon
 */
public class RepoEngineServantLocator
    extends LocalObject
    implements ServantLocator
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(RepoEngineServantLocator.class);
    }

    private RepoEngineImpl m_repoEngineImpl;
    private org.omg.CORBA.ORB m_orb;
    
    public RepoEngineServantLocator(RepoEngineImpl eImpl,
                                org.omg.CORBA.ORB orb)
    {
        m_repoEngineImpl = eImpl;
        m_orb = orb;
    }
  
    public org.omg.PortableServer.Servant preinvoke(byte[] oid,
                                                    POA the_poa,
                                                    String operation,
                                                    CookieHolder the_cookie)
        throws ForwardRequest
    {
        String repoEngineImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Preinvoke called for repoEngineImpl id [" + repoEngineImpl_id + "]");

        if(m_repoEngineImpl == null) {
            m_repoEngineImpl = new RepoEngineImpl();
        }
        RepoEngineServant eServant = new RepoEngineServant(the_poa, m_repoEngineImpl);
        return eServant;
    }
  

    public void postinvoke(byte[] oid,
                           POA the_poa,
                           String operation,
                           java.lang.Object the_cookie,
                           org.omg.PortableServer.Servant the_servant)
    {
        String repoEngineImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Postinvoke called for repoEngine id [" + repoEngineImpl_id + "]");

        if (the_servant instanceof RepoEngineServant) {
            RepoEngineServant eServant = (RepoEngineServant) the_servant;
            //eServant.cleanup();
        }
    }

}






