/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: PersistManagerServantLocator.java,v 1.3 2001/08/13 06:09:03 hyoon Exp $ 
 */ 
package deaf.persist;

import  deaf.util.*;
 
import  java.io.*;
import  java.util.*;
 
import  org.apache.log4j.*;
import  org.omg.CORBA.*;
import  org.omg.CORBA.portable.*;
import  org.omg.PortableServer.*;
import  org.omg.PortableServer.POA.*;
import  org.omg.PortableServer.ServantLocatorPackage.*;


/**
 * PersistManagerServantLocator
 *
 * @version $Revision: 1.3 $
 * @author Hyoungsoo Yoon
 */
public class PersistManagerServantLocator
    extends LocalObject
    implements ServantLocator
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(PersistManagerServantLocator.class);
    }

    private PersistManagerImpl m_persistManagerImpl;
    private org.omg.CORBA.ORB m_orb;
    
    public PersistManagerServantLocator(PersistManagerImpl mImpl,
                                org.omg.CORBA.ORB orb)
    {
        m_persistManagerImpl = mImpl;
        m_orb = orb;
    }
  
    public org.omg.PortableServer.Servant preinvoke(byte[] oid,
                                                    POA the_poa,
                                                    String operation,
                                                    CookieHolder the_cookie)
        throws ForwardRequest
    {
        String persistManagerImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Preinvoke called for persistManagerImpl id [" + persistManagerImpl_id + "]");

        if(m_persistManagerImpl == null) {
            m_persistManagerImpl = new PersistManagerImpl();
        }
        PersistManagerServant mServant = new PersistManagerServant(the_poa, m_persistManagerImpl);
        return mServant;
    }
  

    public void postinvoke(byte[] oid,
                           POA the_poa,
                           String operation,
                           java.lang.Object the_cookie,
                           org.omg.PortableServer.Servant the_servant)
    {
        String persistManagerImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Postinvoke called for persistManagerImpl id [" + persistManagerImpl_id + "]");

        if (the_servant instanceof PersistManagerServant) {
            PersistManagerServant mServant = (PersistManagerServant) the_servant;
            //mServant.cleanup();
        }
    }

}






