/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: DataManagerServantLocator.java,v 1.3 2001/08/13 06:09:03 hyoon Exp $ 
 */ 
package deaf.persist;

import  deaf.util.*;
 
import  java.io.*;
import  java.util.*;
 
import  org.apache.log4j.*;
import  org.omg.CORBA.*;
import  org.omg.CORBA.portable.*;
import  org.omg.PortableServer.*;
import  org.omg.PortableServer.POA.*;
import  org.omg.PortableServer.ServantLocatorPackage.*;


/**
 * DataManagerServantLocator
 *
 * @version $Revision: 1.3 $
 * @author Hyoungsoo Yoon
 */
public class DataManagerServantLocator
    extends LocalObject
    implements ServantLocator
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(DataManagerServantLocator.class);
    }

    private DataManagerImpl m_dataManagerImpl;
    private org.omg.CORBA.ORB m_orb;
    
    public DataManagerServantLocator(DataManagerImpl mImpl,
                                org.omg.CORBA.ORB orb)
    {
        m_dataManagerImpl = mImpl;
        m_orb = orb;
    }
  
    public org.omg.PortableServer.Servant preinvoke(byte[] oid,
                                                    POA the_poa,
                                                    String operation,
                                                    CookieHolder the_cookie)
        throws ForwardRequest
    {
        String dataManagerImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Preinvoke called for dataManagerImpl id [" + dataManagerImpl_id + "]");

        if(m_dataManagerImpl == null) {
            m_dataManagerImpl = new DataManagerImpl();
        }
        DataManagerServant mServant = new DataManagerServant(the_poa, m_dataManagerImpl);
        return mServant;
    }
  

    public void postinvoke(byte[] oid,
                           POA the_poa,
                           String operation,
                           java.lang.Object the_cookie,
                           org.omg.PortableServer.Servant the_servant)
    {
        String dataManagerImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Postinvoke called for dataManagerImpl id [" + dataManagerImpl_id + "]");

        if (the_servant instanceof DataManagerServant) {
            DataManagerServant mServant = (DataManagerServant) the_servant;
            //mServant.cleanup();
        }
    }

}






