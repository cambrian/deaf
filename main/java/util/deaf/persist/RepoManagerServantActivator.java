/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: RepoManagerServantActivator.java,v 1.3 2001/08/13 06:09:03 hyoon Exp $ 
 */ 
package deaf.persist;

import  deaf.util.*;

import  java.io.*;
import  java.util.*;
 
import  org.apache.log4j.*;
import  org.omg.CORBA.*;
import  org.omg.CORBA.portable.*;
import  org.omg.PortableServer.*;
import  org.omg.PortableServer.POA.*;


/**
 * RepoManagerServantActivator
 *
 * @version $Revision: 1.3 $
 * @author Hyoungsoo Yoon
 */
public class RepoManagerServantActivator
    extends LocalObject
    implements ServantActivator
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(RepoManagerServantActivator.class);
    }

    private RepoManagerImpl  m_repoManagerImpl;
    private org.omg.CORBA.ORB m_orb;

    public RepoManagerServantActivator(RepoManagerImpl mImpl,
                                  org.omg.CORBA.ORB orb)
    {
        m_repoManagerImpl = mImpl;
        m_orb = orb;
    }

  
    public Servant incarnate(byte[] oid,
                             POA the_poa)
        throws ForwardRequest
    {
        String repoManagerImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Incarnating repoManagerImpl id [" + repoManagerImpl_id + "]");

        if(m_repoManagerImpl == null) {
            m_repoManagerImpl = new RepoManagerImpl();
        }
        RepoManagerServant mServant = new RepoManagerServant(the_poa, m_repoManagerImpl);

        return mServant;
    }

    public void etherealize(byte[] oid,
                            org.omg.PortableServer.POA the_poa,
                            org.omg.PortableServer.Servant the_servant,
                            boolean cleanup_in_progress,
                            boolean remaining_activations)
    {
        String repoManagerImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Etherializing repoManagerImpl id [" + repoManagerImpl_id + "]");

        if (the_servant instanceof RepoManagerServant) {
            RepoManagerServant mServant = (RepoManagerServant) the_servant;
            //mServant.cleanup();
        }
    }

}







