/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: PersistEngineServantActivator.java,v 1.3 2001/08/13 06:09:03 hyoon Exp $ 
 */ 
package deaf.persist;

import  deaf.util.*;

import  java.io.*;
import  java.util.*;
 
import  org.apache.log4j.*;
import  org.omg.CORBA.*;
import  org.omg.CORBA.portable.*;
import  org.omg.PortableServer.*;
import  org.omg.PortableServer.POA.*;


/**
 * PersistEngineServantActivator
 *
 * @version $Revision: 1.3 $
 * @author Hyoungsoo Yoon
 */
public class PersistEngineServantActivator
    extends LocalObject
    implements ServantActivator
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(PersistEngineServantActivator.class);
    }

    private PersistEngineImpl  m_persistEngineImpl;
    private org.omg.CORBA.ORB m_orb;

    public PersistEngineServantActivator(PersistEngineImpl eImpl,
                                  org.omg.CORBA.ORB orb)
    {
        m_persistEngineImpl = eImpl;
        m_orb = orb;
    }

  
    public Servant incarnate(byte[] oid,
                             POA the_poa)
        throws ForwardRequest
    {
        String persistEngineImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Incarnating persistEngineImpl id [" + persistEngineImpl_id + "]");

        if(m_persistEngineImpl == null) {
            m_persistEngineImpl = new PersistEngineImpl();
        }
        PersistEngineServant eServant = new PersistEngineServant(the_poa, m_persistEngineImpl);

        return eServant;
    }

    public void etherealize(byte[] oid,
                            org.omg.PortableServer.POA the_poa,
                            org.omg.PortableServer.Servant the_servant,
                            boolean cleanup_in_progress,
                            boolean remaining_activations)
    {
        String persistEngineImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Etherealizing persistEngineImpl id [" + persistEngineImpl_id +"]");

        if (the_servant instanceof PersistEngineServant) {
            PersistEngineServant eServant = (PersistEngineServant) the_servant;
            //eServant.cleanup();
        }
    }

}







