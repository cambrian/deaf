/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: DataEngineServantActivator.java,v 1.3 2001/08/13 06:09:03 hyoon Exp $ 
 */ 
package deaf.persist;

import  deaf.util.*;

import  java.io.*;
import  java.util.*;
 
import  org.apache.log4j.*;
import  org.omg.CORBA.*;
import  org.omg.CORBA.portable.*;
import  org.omg.PortableServer.*;
import  org.omg.PortableServer.POA.*;


/**
 * DataEngineServantActivator
 *
 * @version $Revision: 1.3 $
 * @author Hyoungsoo Yoon
 */
public class DataEngineServantActivator
    extends LocalObject
    implements ServantActivator
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(DataEngineServantActivator.class);
    }

    private DataEngineImpl  m_dataEngineImpl;
    private org.omg.CORBA.ORB m_orb;

    public DataEngineServantActivator(DataEngineImpl eImpl,
                                  org.omg.CORBA.ORB orb)
    {
        m_dataEngineImpl = eImpl;
        m_orb = orb;
    }

  
    public Servant incarnate(byte[] oid,
                             POA the_poa)
        throws ForwardRequest
    {
        String dataEngineImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Incarnating dataEngineImpl id [" + dataEngineImpl_id + "]");

        if(m_dataEngineImpl == null) {
            m_dataEngineImpl = new DataEngineImpl();
        }
        DataEngineServant eServant = new DataEngineServant(the_poa, m_dataEngineImpl);

        return eServant;
    }

    public void etherealize(byte[] oid,
                            org.omg.PortableServer.POA the_poa,
                            org.omg.PortableServer.Servant the_servant,
                            boolean cleanup_in_progress,
                            boolean remaining_activations)
    {
        String dataEngineImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Etherealizing dataEngineImpl id [" + dataEngineImpl_id +"]");

        if (the_servant instanceof DataEngineServant) {
            DataEngineServant eServant = (DataEngineServant) the_servant;
            //eServant.cleanup();
        }
    }

}







