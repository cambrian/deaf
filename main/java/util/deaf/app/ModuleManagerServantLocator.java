/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: ModuleManagerServantLocator.java,v 1.3 2001/08/13 06:09:03 hyoon Exp $ 
 */ 
package deaf.app;

import  deaf.util.*;
 
import  java.io.*;
import  java.util.*;
 
import  org.apache.log4j.*;
import  org.omg.CORBA.*;
import  org.omg.CORBA.portable.*;
import  org.omg.PortableServer.*;
import  org.omg.PortableServer.POA.*;
import  org.omg.PortableServer.ServantLocatorPackage.*;


/**
 * ModuleManagerServantLocator
 *
 * @version $Revision: 1.3 $
 * @author Hyoungsoo Yoon
 */
public class ModuleManagerServantLocator
    extends LocalObject
    implements ServantLocator
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(ModuleManagerServantLocator.class);
    }

    private ModuleManagerImpl m_moduleManagerImpl;
    private org.omg.CORBA.ORB m_orb;
    
    public ModuleManagerServantLocator(ModuleManagerImpl mImpl,
                                org.omg.CORBA.ORB orb)
    {
        m_moduleManagerImpl = mImpl;
        m_orb = orb;
    }
  
    public org.omg.PortableServer.Servant preinvoke(byte[] oid,
                                                    POA the_poa,
                                                    String operation,
                                                    CookieHolder the_cookie)
        throws ForwardRequest
    {
        String moduleManagerImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Preinvoke called for moduleManagerImpl id [" + moduleManagerImpl_id + "]");

        if(m_moduleManagerImpl == null) {
            m_moduleManagerImpl = new ModuleManagerImpl();
        }
        ModuleManagerServant mServant = new ModuleManagerServant(the_poa, m_moduleManagerImpl);
        return mServant;
    }
  

    public void postinvoke(byte[] oid,
                           POA the_poa,
                           String operation,
                           java.lang.Object the_cookie,
                           org.omg.PortableServer.Servant the_servant)
    {
        String moduleManagerImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Postinvoke called for moduleManagerImpl id [" + moduleManagerImpl_id + "]");

        if (the_servant instanceof ModuleManagerServant) {
            ModuleManagerServant mServant = (ModuleManagerServant) the_servant;
            //mServant.cleanup();
        }
    }

}






