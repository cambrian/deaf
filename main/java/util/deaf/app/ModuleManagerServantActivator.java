/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: ModuleManagerServantActivator.java,v 1.3 2001/08/13 06:09:03 hyoon Exp $ 
 */ 
package deaf.app;

import  deaf.util.*;

import  java.io.*;
import  java.util.*;
 
import  org.apache.log4j.*;
import  org.omg.CORBA.*;
import  org.omg.CORBA.portable.*;
import  org.omg.PortableServer.*;
import  org.omg.PortableServer.POA.*;


/**
 * ModuleManagerServantActivator
 *
 * @version $Revision: 1.3 $
 * @author Hyoungsoo Yoon
 */
public class ModuleManagerServantActivator
    extends LocalObject
    implements ServantActivator
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(ModuleManagerServantActivator.class);
    }

    private ModuleManagerImpl  m_moduleManagerImpl;
    private org.omg.CORBA.ORB m_orb;

    public ModuleManagerServantActivator(ModuleManagerImpl mImpl,
                                  org.omg.CORBA.ORB orb)
    {
        m_moduleManagerImpl = mImpl;
        m_orb = orb;
    }

  
    public Servant incarnate(byte[] oid,
                             POA the_poa)
        throws ForwardRequest
    {
        String moduleManagerImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Incarnating moduleManagerImpl id [" + moduleManagerImpl_id + "]");

        if(m_moduleManagerImpl == null) {
            m_moduleManagerImpl = new ModuleManagerImpl();
        }
        ModuleManagerServant mServant = new ModuleManagerServant(the_poa, m_moduleManagerImpl);

        return mServant;
    }

    public void etherealize(byte[] oid,
                            org.omg.PortableServer.POA the_poa,
                            org.omg.PortableServer.Servant the_servant,
                            boolean cleanup_in_progress,
                            boolean remaining_activations)
    {
        String moduleManagerImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Etherializing moduleManagerImpl id [" + moduleManagerImpl_id + "]");

        if (the_servant instanceof ModuleManagerServant) {
            ModuleManagerServant mServant = (ModuleManagerServant) the_servant;
            //mServant.cleanup();
        }
    }

}







