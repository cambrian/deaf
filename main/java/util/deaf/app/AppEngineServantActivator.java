/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: AppEngineServantActivator.java,v 1.3 2001/08/13 06:09:03 hyoon Exp $ 
 */ 
package deaf.app;

import  deaf.util.*;

import  java.io.*;
import  java.util.*;
 
import  org.apache.log4j.*;
import  org.omg.CORBA.*;
import  org.omg.CORBA.portable.*;
import  org.omg.PortableServer.*;
import  org.omg.PortableServer.POA.*;


/**
 * AppEngineServantActivator
 *
 * @version $Revision: 1.3 $
 * @author Hyoungsoo Yoon
 */
public class AppEngineServantActivator
    extends LocalObject
    implements ServantActivator
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(AppEngineServantActivator.class);
    }

    private AppEngineImpl  m_appEngineImpl;
    private org.omg.CORBA.ORB m_orb;

    public AppEngineServantActivator(AppEngineImpl eImpl,
                                  org.omg.CORBA.ORB orb)
    {
        m_appEngineImpl = eImpl;
        m_orb = orb;
    }

  
    public Servant incarnate(byte[] oid,
                             POA the_poa)
        throws ForwardRequest
    {
        String appEngineImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Incarnating appEngineImpl id [" + appEngineImpl_id + "]");

        if(m_appEngineImpl == null) {
            m_appEngineImpl = new AppEngineImpl();
        }
        AppEngineServant eServant = new AppEngineServant(the_poa, m_appEngineImpl);

        return eServant;
    }

    public void etherealize(byte[] oid,
                            org.omg.PortableServer.POA the_poa,
                            org.omg.PortableServer.Servant the_servant,
                            boolean cleanup_in_progress,
                            boolean remaining_activations)
    {
        String appEngineImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Etherealizing appEngineImpl id [" + appEngineImpl_id +"]");

        if (the_servant instanceof AppEngineServant) {
            AppEngineServant eServant = (AppEngineServant) the_servant;
            //eServant.cleanup();
        }
    }

}







