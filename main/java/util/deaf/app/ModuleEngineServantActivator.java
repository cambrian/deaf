/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: ModuleEngineServantActivator.java,v 1.3 2001/08/13 06:09:03 hyoon Exp $ 
 */ 
package deaf.app;

import  deaf.util.*;

import  java.io.*;
import  java.util.*;
 
import  org.apache.log4j.*;
import  org.omg.CORBA.*;
import  org.omg.CORBA.portable.*;
import  org.omg.PortableServer.*;
import  org.omg.PortableServer.POA.*;


/**
 * ModuleEngineServantActivator
 *
 * @version $Revision: 1.3 $
 * @author Hyoungsoo Yoon
 */
public class ModuleEngineServantActivator
    extends LocalObject
    implements ServantActivator
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(ModuleEngineServantActivator.class);
    }

    private ModuleEngineImpl  m_moduleEngineImpl;
    private org.omg.CORBA.ORB m_orb;

    public ModuleEngineServantActivator(ModuleEngineImpl eImpl,
                                  org.omg.CORBA.ORB orb)
    {
        m_moduleEngineImpl = eImpl;
        m_orb = orb;
    }

  
    public Servant incarnate(byte[] oid,
                             POA the_poa)
        throws ForwardRequest
    {
        String moduleEngineImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Incarnating moduleEngineImpl id [" + moduleEngineImpl_id + "]");

        if(m_moduleEngineImpl == null) {
            m_moduleEngineImpl = new ModuleEngineImpl();
        }
        ModuleEngineServant eServant = new ModuleEngineServant(the_poa, m_moduleEngineImpl);

        return eServant;
    }

    public void etherealize(byte[] oid,
                            org.omg.PortableServer.POA the_poa,
                            org.omg.PortableServer.Servant the_servant,
                            boolean cleanup_in_progress,
                            boolean remaining_activations)
    {
        String moduleEngineImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Etherealizing moduleEngineImpl id [" + moduleEngineImpl_id +"]");

        if (the_servant instanceof ModuleEngineServant) {
            ModuleEngineServant eServant = (ModuleEngineServant) the_servant;
            //eServant.cleanup();
        }
    }

}







