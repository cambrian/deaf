/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: AppManagerServantLocator.java,v 1.3 2001/08/13 06:09:03 hyoon Exp $ 
 */ 
package deaf.app;

import  deaf.util.*;
 
import  java.io.*;
import  java.util.*;
 
import  org.apache.log4j.*;
import  org.omg.CORBA.*;
import  org.omg.CORBA.portable.*;
import  org.omg.PortableServer.*;
import  org.omg.PortableServer.POA.*;
import  org.omg.PortableServer.ServantLocatorPackage.*;


/**
 * AppManagerServantLocator
 *
 * @version $Revision: 1.3 $
 * @author Hyoungsoo Yoon
 */
public class AppManagerServantLocator
    extends LocalObject
    implements ServantLocator
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(AppManagerServantLocator.class);
    }

    private AppManagerImpl m_appManagerImpl;
    private org.omg.CORBA.ORB m_orb;
    
    public AppManagerServantLocator(AppManagerImpl mImpl,
                                org.omg.CORBA.ORB orb)
    {
        m_appManagerImpl = mImpl;
        m_orb = orb;
    }
  
    public org.omg.PortableServer.Servant preinvoke(byte[] oid,
                                                    POA the_poa,
                                                    String operation,
                                                    CookieHolder the_cookie)
        throws ForwardRequest
    {
        String appManagerImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Preinvoke called for appManagerImpl id [" + appManagerImpl_id + "]");

        if(m_appManagerImpl == null) {
            m_appManagerImpl = new AppManagerImpl();
        }
        AppManagerServant mServant = new AppManagerServant(the_poa, m_appManagerImpl);
        return mServant;
    }
  

    public void postinvoke(byte[] oid,
                           POA the_poa,
                           String operation,
                           java.lang.Object the_cookie,
                           org.omg.PortableServer.Servant the_servant)
    {
        String appManagerImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Postinvoke called for appManagerImpl id [" + appManagerImpl_id + "]");

        if (the_servant instanceof AppManagerServant) {
            AppManagerServant mServant = (AppManagerServant) the_servant;
            //mServant.cleanup();
        }
    }

}






