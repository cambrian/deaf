/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: AppEngineServantLocator.java,v 1.3 2001/08/13 06:09:03 hyoon Exp $ 
 */ 
package deaf.app;

import  deaf.util.*;
 
import  java.io.*;
import  java.util.*;
 
import  org.apache.log4j.*;
import  org.omg.CORBA.*;
import  org.omg.CORBA.portable.*;
import  org.omg.PortableServer.*;
import  org.omg.PortableServer.POA.*;
import  org.omg.PortableServer.ServantLocatorPackage.*;


/**
 * AppEngineServantLocator
 *
 * @version $Revision: 1.3 $
 * @author Hyoungsoo Yoon
 */
public class AppEngineServantLocator
    extends LocalObject
    implements ServantLocator
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(AppEngineServantLocator.class);
    }

    private AppEngineImpl m_appEngineImpl;
    private org.omg.CORBA.ORB m_orb;
    
    public AppEngineServantLocator(AppEngineImpl eImpl,
                                org.omg.CORBA.ORB orb)
    {
        m_appEngineImpl = eImpl;
        m_orb = orb;
    }
  
    public org.omg.PortableServer.Servant preinvoke(byte[] oid,
                                                    POA the_poa,
                                                    String operation,
                                                    CookieHolder the_cookie)
        throws ForwardRequest
    {
        String appEngineImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Preinvoke called for appEngineImpl id [" + appEngineImpl_id + "]");

        if(m_appEngineImpl == null) {
            m_appEngineImpl = new AppEngineImpl();
        }
        AppEngineServant eServant = new AppEngineServant(the_poa, m_appEngineImpl);
        return eServant;
    }
  

    public void postinvoke(byte[] oid,
                           POA the_poa,
                           String operation,
                           java.lang.Object the_cookie,
                           org.omg.PortableServer.Servant the_servant)
    {
        String appEngineImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Postinvoke called for appEngine id [" + appEngineImpl_id + "]");

        if (the_servant instanceof AppEngineServant) {
            AppEngineServant eServant = (AppEngineServant) the_servant;
            //eServant.cleanup();
        }
    }

}






