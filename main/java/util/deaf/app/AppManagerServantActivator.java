/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: AppManagerServantActivator.java,v 1.3 2001/08/13 06:09:03 hyoon Exp $ 
 */ 
package deaf.app;

import  deaf.util.*;

import  java.io.*;
import  java.util.*;
 
import  org.apache.log4j.*;
import  org.omg.CORBA.*;
import  org.omg.CORBA.portable.*;
import  org.omg.PortableServer.*;
import  org.omg.PortableServer.POA.*;


/**
 * AppManagerServantActivator
 *
 * @version $Revision: 1.3 $
 * @author Hyoungsoo Yoon
 */
public class AppManagerServantActivator
    extends LocalObject
    implements ServantActivator
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(AppManagerServantActivator.class);
    }

    private AppManagerImpl  m_appManagerImpl;
    private org.omg.CORBA.ORB m_orb;

    public AppManagerServantActivator(AppManagerImpl mImpl,
                                  org.omg.CORBA.ORB orb)
    {
        m_appManagerImpl = mImpl;
        m_orb = orb;
    }

  
    public Servant incarnate(byte[] oid,
                             POA the_poa)
        throws ForwardRequest
    {
        String appManagerImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Incarnating appManagerImpl id [" + appManagerImpl_id + "]");

        if(m_appManagerImpl == null) {
            m_appManagerImpl = new AppManagerImpl();
        }
        AppManagerServant mServant = new AppManagerServant(the_poa, m_appManagerImpl);

        return mServant;
    }

    public void etherealize(byte[] oid,
                            org.omg.PortableServer.POA the_poa,
                            org.omg.PortableServer.Servant the_servant,
                            boolean cleanup_in_progress,
                            boolean remaining_activations)
    {
        String appManagerImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Etherializing appManagerImpl id [" + appManagerImpl_id + "]");

        if (the_servant instanceof AppManagerServant) {
            AppManagerServant mServant = (AppManagerServant) the_servant;
            //mServant.cleanup();
        }
    }

}







