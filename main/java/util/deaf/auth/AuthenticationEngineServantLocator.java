/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: AuthenticationEngineServantLocator.java,v 1.3 2001/08/13 06:09:03 hyoon Exp $ 
 */ 
package deaf.auth;

import  deaf.util.*;
 
import  java.io.*;
import  java.util.*;
 
import  org.apache.log4j.*;
import  org.omg.CORBA.*;
import  org.omg.CORBA.portable.*;
import  org.omg.PortableServer.*;
import  org.omg.PortableServer.POA.*;
import  org.omg.PortableServer.ServantLocatorPackage.*;


/**
 * AuthenticationEngineServantLocator
 *
 * @version $Revision: 1.3 $
 * @authenticationor Hyoungsoo Yoon
 */
public class AuthenticationEngineServantLocator
    extends LocalObject
    implements ServantLocator
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(AuthenticationEngineServantLocator.class);
    }

    private AuthenticationEngineImpl m_authenticationEngineImpl;
    private org.omg.CORBA.ORB m_orb;
    
    public AuthenticationEngineServantLocator(AuthenticationEngineImpl eImpl,
                                org.omg.CORBA.ORB orb)
    {
        m_authenticationEngineImpl = eImpl;
        m_orb = orb;
    }
  
    public org.omg.PortableServer.Servant preinvoke(byte[] oid,
                                                    POA the_poa,
                                                    String operation,
                                                    CookieHolder the_cookie)
        throws ForwardRequest
    {
        String authenticationEngineImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Preinvoke called for authenticationEngineImpl id [" + authenticationEngineImpl_id + "]");

        if(m_authenticationEngineImpl == null) {
            m_authenticationEngineImpl = new AuthenticationEngineImpl();
        }
        AuthenticationEngineServant eServant = new AuthenticationEngineServant(the_poa, m_authenticationEngineImpl);
        return eServant;
    }
  

    public void postinvoke(byte[] oid,
                           POA the_poa,
                           String operation,
                           java.lang.Object the_cookie,
                           org.omg.PortableServer.Servant the_servant)
    {
        String authenticationEngineImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Postinvoke called for authenticationEngine id [" + authenticationEngineImpl_id + "]");

        if (the_servant instanceof AuthenticationEngineServant) {
            AuthenticationEngineServant eServant = (AuthenticationEngineServant) the_servant;
            //eServant.cleanup();
        }
    }

}






