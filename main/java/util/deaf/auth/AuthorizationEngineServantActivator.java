/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: AuthorizationEngineServantActivator.java,v 1.3 2001/08/13 06:09:03 hyoon Exp $ 
 */ 
package deaf.auth;

import  deaf.util.*;

import  java.io.*;
import  java.util.*;
 
import  org.apache.log4j.*;
import  org.omg.CORBA.*;
import  org.omg.CORBA.portable.*;
import  org.omg.PortableServer.*;
import  org.omg.PortableServer.POA.*;


/**
 * AuthorizationEngineServantActivator
 *
 * @version $Revision: 1.3 $
 * @authorizationor Hyoungsoo Yoon
 */
public class AuthorizationEngineServantActivator
    extends LocalObject
    implements ServantActivator
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(AuthorizationEngineServantActivator.class);
    }

    private AuthorizationEngineImpl  m_authorizationEngineImpl;
    private org.omg.CORBA.ORB m_orb;

    public AuthorizationEngineServantActivator(AuthorizationEngineImpl eImpl,
                                  org.omg.CORBA.ORB orb)
    {
        m_authorizationEngineImpl = eImpl;
        m_orb = orb;
    }

  
    public Servant incarnate(byte[] oid,
                             POA the_poa)
        throws ForwardRequest
    {
        String authorizationEngineImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Incarnating authorizationEngineImpl id [" + authorizationEngineImpl_id + "]");

        if(m_authorizationEngineImpl == null) {
            m_authorizationEngineImpl = new AuthorizationEngineImpl();
        }
        AuthorizationEngineServant eServant = new AuthorizationEngineServant(the_poa, m_authorizationEngineImpl);

        return eServant;
    }

    public void etherealize(byte[] oid,
                            org.omg.PortableServer.POA the_poa,
                            org.omg.PortableServer.Servant the_servant,
                            boolean cleanup_in_progress,
                            boolean remaining_activations)
    {
        String authorizationEngineImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Etherealizing authorizationEngineImpl id [" + authorizationEngineImpl_id +"]");

        if (the_servant instanceof AuthorizationEngineServant) {
            AuthorizationEngineServant eServant = (AuthorizationEngineServant) the_servant;
            //eServant.cleanup();
        }
    }

}







