/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: AuthManagerServantActivator.java,v 1.3 2001/08/13 06:09:03 hyoon Exp $ 
 */ 
package deaf.auth;

import  deaf.util.*;

import  java.io.*;
import  java.util.*;
 
import  org.apache.log4j.*;
import  org.omg.CORBA.*;
import  org.omg.CORBA.portable.*;
import  org.omg.PortableServer.*;
import  org.omg.PortableServer.POA.*;


/**
 * AuthManagerServantActivator
 *
 * @version $Revision: 1.3 $
 * @author Hyoungsoo Yoon
 */
public class AuthManagerServantActivator
    extends LocalObject
    implements ServantActivator
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(AuthManagerServantActivator.class);
    }

    private AuthManagerImpl  m_authManagerImpl;
    private org.omg.CORBA.ORB m_orb;

    public AuthManagerServantActivator(AuthManagerImpl mImpl,
                                  org.omg.CORBA.ORB orb)
    {
        m_authManagerImpl = mImpl;
        m_orb = orb;
    }

  
    public Servant incarnate(byte[] oid,
                             POA the_poa)
        throws ForwardRequest
    {
        String authManagerImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Incarnating authManagerImpl id [" + authManagerImpl_id + "]");

        if(m_authManagerImpl == null) {
            m_authManagerImpl = new AuthManagerImpl();
        }
        AuthManagerServant mServant = new AuthManagerServant(the_poa, m_authManagerImpl);

        return mServant;
    }

    public void etherealize(byte[] oid,
                            org.omg.PortableServer.POA the_poa,
                            org.omg.PortableServer.Servant the_servant,
                            boolean cleanup_in_progress,
                            boolean remaining_activations)
    {
        String authManagerImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Etherializing authManagerImpl id [" + authManagerImpl_id + "]");

        if (the_servant instanceof AuthManagerServant) {
            AuthManagerServant mServant = (AuthManagerServant) the_servant;
            //mServant.cleanup();
        }
    }

}







