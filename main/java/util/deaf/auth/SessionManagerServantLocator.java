/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: SessionManagerServantLocator.java,v 1.3 2001/08/13 06:09:03 hyoon Exp $ 
 */ 
package deaf.auth;

import  deaf.util.*;
 
import  java.io.*;
import  java.util.*;
 
import  org.apache.log4j.*;
import  org.omg.CORBA.*;
import  org.omg.CORBA.portable.*;
import  org.omg.PortableServer.*;
import  org.omg.PortableServer.POA.*;
import  org.omg.PortableServer.ServantLocatorPackage.*;


/**
 * SessionManagerServantLocator
 *
 * @version $Revision: 1.3 $
 * @sessionor Hyoungsoo Yoon
 */
public class SessionManagerServantLocator
    extends LocalObject
    implements ServantLocator
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(SessionManagerServantLocator.class);
    }

    private SessionManagerImpl m_sessionManagerImpl;
    private org.omg.CORBA.ORB m_orb;
    
    public SessionManagerServantLocator(SessionManagerImpl mImpl,
                                org.omg.CORBA.ORB orb)
    {
        m_sessionManagerImpl = mImpl;
        m_orb = orb;
    }
  
    public org.omg.PortableServer.Servant preinvoke(byte[] oid,
                                                    POA the_poa,
                                                    String operation,
                                                    CookieHolder the_cookie)
        throws ForwardRequest
    {
        String sessionManagerImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Preinvoke called for sessionManagerImpl id [" + sessionManagerImpl_id + "]");

        if(m_sessionManagerImpl == null) {
            m_sessionManagerImpl = new SessionManagerImpl();
        }
        SessionManagerServant mServant = new SessionManagerServant(the_poa, m_sessionManagerImpl);
        return mServant;
    }
  

    public void postinvoke(byte[] oid,
                           POA the_poa,
                           String operation,
                           java.lang.Object the_cookie,
                           org.omg.PortableServer.Servant the_servant)
    {
        String sessionManagerImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Postinvoke called for sessionManagerImpl id [" + sessionManagerImpl_id + "]");

        if (the_servant instanceof SessionManagerServant) {
            SessionManagerServant mServant = (SessionManagerServant) the_servant;
            //mServant.cleanup();
        }
    }

}






