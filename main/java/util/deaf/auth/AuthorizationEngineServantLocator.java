/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: AuthorizationEngineServantLocator.java,v 1.3 2001/08/13 06:09:03 hyoon Exp $ 
 */ 
package deaf.auth;

import  deaf.util.*;
 
import  java.io.*;
import  java.util.*;
 
import  org.apache.log4j.*;
import  org.omg.CORBA.*;
import  org.omg.CORBA.portable.*;
import  org.omg.PortableServer.*;
import  org.omg.PortableServer.POA.*;
import  org.omg.PortableServer.ServantLocatorPackage.*;


/**
 * AuthorizationEngineServantLocator
 *
 * @version $Revision: 1.3 $
 * @authorizationor Hyoungsoo Yoon
 */
public class AuthorizationEngineServantLocator
    extends LocalObject
    implements ServantLocator
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(AuthorizationEngineServantLocator.class);
    }

    private AuthorizationEngineImpl m_authorizationEngineImpl;
    private org.omg.CORBA.ORB m_orb;
    
    public AuthorizationEngineServantLocator(AuthorizationEngineImpl eImpl,
                                org.omg.CORBA.ORB orb)
    {
        m_authorizationEngineImpl = eImpl;
        m_orb = orb;
    }
  
    public org.omg.PortableServer.Servant preinvoke(byte[] oid,
                                                    POA the_poa,
                                                    String operation,
                                                    CookieHolder the_cookie)
        throws ForwardRequest
    {
        String authorizationEngineImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Preinvoke called for authorizationEngineImpl id [" + authorizationEngineImpl_id + "]");

        if(m_authorizationEngineImpl == null) {
            m_authorizationEngineImpl = new AuthorizationEngineImpl();
        }
        AuthorizationEngineServant eServant = new AuthorizationEngineServant(the_poa, m_authorizationEngineImpl);
        return eServant;
    }
  

    public void postinvoke(byte[] oid,
                           POA the_poa,
                           String operation,
                           java.lang.Object the_cookie,
                           org.omg.PortableServer.Servant the_servant)
    {
        String authorizationEngineImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Postinvoke called for authorizationEngine id [" + authorizationEngineImpl_id + "]");

        if (the_servant instanceof AuthorizationEngineServant) {
            AuthorizationEngineServant eServant = (AuthorizationEngineServant) the_servant;
            //eServant.cleanup();
        }
    }

}






