/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: SessionEngineServantLocator.java,v 1.3 2001/08/13 06:09:03 hyoon Exp $ 
 */ 
package deaf.auth;

import  deaf.util.*;
 
import  java.io.*;
import  java.util.*;
 
import  org.apache.log4j.*;
import  org.omg.CORBA.*;
import  org.omg.CORBA.portable.*;
import  org.omg.PortableServer.*;
import  org.omg.PortableServer.POA.*;
import  org.omg.PortableServer.ServantLocatorPackage.*;


/**
 * SessionEngineServantLocator
 *
 * @version $Revision: 1.3 $
 * @sessionor Hyoungsoo Yoon
 */
public class SessionEngineServantLocator
    extends LocalObject
    implements ServantLocator
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(SessionEngineServantLocator.class);
    }

    private SessionEngineImpl m_sessionEngineImpl;
    private org.omg.CORBA.ORB m_orb;
    
    public SessionEngineServantLocator(SessionEngineImpl eImpl,
                                org.omg.CORBA.ORB orb)
    {
        m_sessionEngineImpl = eImpl;
        m_orb = orb;
    }
  
    public org.omg.PortableServer.Servant preinvoke(byte[] oid,
                                                    POA the_poa,
                                                    String operation,
                                                    CookieHolder the_cookie)
        throws ForwardRequest
    {
        String sessionEngineImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Preinvoke called for sessionEngineImpl id [" + sessionEngineImpl_id + "]");

        if(m_sessionEngineImpl == null) {
            m_sessionEngineImpl = new SessionEngineImpl();
        }
        SessionEngineServant eServant = new SessionEngineServant(the_poa, m_sessionEngineImpl);
        return eServant;
    }
  

    public void postinvoke(byte[] oid,
                           POA the_poa,
                           String operation,
                           java.lang.Object the_cookie,
                           org.omg.PortableServer.Servant the_servant)
    {
        String sessionEngineImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Postinvoke called for sessionEngine id [" + sessionEngineImpl_id + "]");

        if (the_servant instanceof SessionEngineServant) {
            SessionEngineServant eServant = (SessionEngineServant) the_servant;
            //eServant.cleanup();
        }
    }

}






