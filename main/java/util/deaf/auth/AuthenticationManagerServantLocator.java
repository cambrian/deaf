/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: AuthenticationManagerServantLocator.java,v 1.3 2001/08/13 06:09:03 hyoon Exp $ 
 */ 
package deaf.auth;

import  deaf.util.*;
 
import  java.io.*;
import  java.util.*;
 
import  org.apache.log4j.*;
import  org.omg.CORBA.*;
import  org.omg.CORBA.portable.*;
import  org.omg.PortableServer.*;
import  org.omg.PortableServer.POA.*;
import  org.omg.PortableServer.ServantLocatorPackage.*;


/**
 * AuthenticationManagerServantLocator
 *
 * @version $Revision: 1.3 $
 * @authenticationor Hyoungsoo Yoon
 */
public class AuthenticationManagerServantLocator
    extends LocalObject
    implements ServantLocator
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(AuthenticationManagerServantLocator.class);
    }

    private AuthenticationManagerImpl m_authenticationManagerImpl;
    private org.omg.CORBA.ORB m_orb;
    
    public AuthenticationManagerServantLocator(AuthenticationManagerImpl mImpl,
                                org.omg.CORBA.ORB orb)
    {
        m_authenticationManagerImpl = mImpl;
        m_orb = orb;
    }
  
    public org.omg.PortableServer.Servant preinvoke(byte[] oid,
                                                    POA the_poa,
                                                    String operation,
                                                    CookieHolder the_cookie)
        throws ForwardRequest
    {
        String authenticationManagerImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Preinvoke called for authenticationManagerImpl id [" + authenticationManagerImpl_id + "]");

        if(m_authenticationManagerImpl == null) {
            m_authenticationManagerImpl = new AuthenticationManagerImpl();
        }
        AuthenticationManagerServant mServant = new AuthenticationManagerServant(the_poa, m_authenticationManagerImpl);
        return mServant;
    }
  

    public void postinvoke(byte[] oid,
                           POA the_poa,
                           String operation,
                           java.lang.Object the_cookie,
                           org.omg.PortableServer.Servant the_servant)
    {
        String authenticationManagerImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Postinvoke called for authenticationManagerImpl id [" + authenticationManagerImpl_id + "]");

        if (the_servant instanceof AuthenticationManagerServant) {
            AuthenticationManagerServant mServant = (AuthenticationManagerServant) the_servant;
            //mServant.cleanup();
        }
    }

}






