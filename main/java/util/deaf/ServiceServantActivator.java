/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: ServiceServantActivator.java,v 1.3 2001/08/13 06:09:03 hyoon Exp $ 
 */ 
package deaf;

import  deaf.util.*;

import  java.io.*;
import  java.util.*;
 
import  org.apache.log4j.*;
import  org.omg.CORBA.*;
import  org.omg.CORBA.portable.*;
import  org.omg.PortableServer.*;
import  org.omg.PortableServer.POA.*;


/**
 * ServiceServantActivator
 *
 * @version $Revision: 1.3 $
 * @author Hyoungsoo Yoon
 */
public class ServiceServantActivator
    extends LocalObject
    implements ServantActivator
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(ServiceServantActivator.class);
    }

    private ServiceImpl  m_serviceImpl;
    private org.omg.CORBA.ORB m_orb;

    public ServiceServantActivator(ServiceImpl sImpl,
                                  org.omg.CORBA.ORB orb)
    {
        m_serviceImpl = sImpl;
        m_orb = orb;
    }

  
    public Servant incarnate(byte[] oid,
                             POA the_poa)
        throws ForwardRequest
    {
        String serviceImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Incarnating serviceImpl id [" + serviceImpl_id + "]");

        if(m_serviceImpl == null) {
            m_serviceImpl = new ServiceImpl();
        }
        ServiceServant sServant = new ServiceServant(the_poa, m_serviceImpl);

        return sServant;
    }

    public void etherealize(byte[] oid,
                            org.omg.PortableServer.POA the_poa,
                            org.omg.PortableServer.Servant the_servant,
                            boolean cleanup_in_progress,
                            boolean remaining_activations)
    {
        String serviceImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Etherealizing serviceImpl id [" + serviceImpl_id +"]");

        if (the_servant instanceof ServiceServant) {
            ServiceServant sServant = (ServiceServant) the_servant;
            //sServant.cleanup();
        }
    }

}







