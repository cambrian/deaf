/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: SkinEngineServantLocator.java,v 1.3 2001/08/13 06:09:03 hyoon Exp $ 
 */ 
package deaf.client;

import  deaf.util.*;
 
import  java.io.*;
import  java.util.*;
 
import  org.apache.log4j.*;
import  org.omg.CORBA.*;
import  org.omg.CORBA.portable.*;
import  org.omg.PortableServer.*;
import  org.omg.PortableServer.POA.*;
import  org.omg.PortableServer.ServantLocatorPackage.*;


/**
 * SkinEngineServantLocator
 *
 * @version $Revision: 1.3 $
 * @author Hyoungsoo Yoon
 */
public class SkinEngineServantLocator
    extends LocalObject
    implements ServantLocator
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(SkinEngineServantLocator.class);
    }

    private SkinEngineImpl m_skinEngineImpl;
    private org.omg.CORBA.ORB m_orb;
    
    public SkinEngineServantLocator(SkinEngineImpl eImpl,
                                org.omg.CORBA.ORB orb)
    {
        m_skinEngineImpl = eImpl;
        m_orb = orb;
    }
  
    public org.omg.PortableServer.Servant preinvoke(byte[] oid,
                                                    POA the_poa,
                                                    String operation,
                                                    CookieHolder the_cookie)
        throws ForwardRequest
    {
        String skinEngineImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Preinvoke called for skinEngineImpl id [" + skinEngineImpl_id + "]");

        if(m_skinEngineImpl == null) {
            m_skinEngineImpl = new SkinEngineImpl();
        }
        SkinEngineServant eServant = new SkinEngineServant(the_poa, m_skinEngineImpl);
        return eServant;
    }
  

    public void postinvoke(byte[] oid,
                           POA the_poa,
                           String operation,
                           java.lang.Object the_cookie,
                           org.omg.PortableServer.Servant the_servant)
    {
        String skinEngineImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Postinvoke called for skinEngine id [" + skinEngineImpl_id + "]");

        if (the_servant instanceof SkinEngineServant) {
            SkinEngineServant eServant = (SkinEngineServant) the_servant;
            //eServant.cleanup();
        }
    }

}






