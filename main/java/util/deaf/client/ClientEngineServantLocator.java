/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: ClientEngineServantLocator.java,v 1.3 2001/08/13 06:09:03 hyoon Exp $ 
 */ 
package deaf.client;

import  deaf.util.*;
 
import  java.io.*;
import  java.util.*;
 
import  org.apache.log4j.*;
import  org.omg.CORBA.*;
import  org.omg.CORBA.portable.*;
import  org.omg.PortableServer.*;
import  org.omg.PortableServer.POA.*;
import  org.omg.PortableServer.ServantLocatorPackage.*;


/**
 * ClientEngineServantLocator
 *
 * @version $Revision: 1.3 $
 * @author Hyoungsoo Yoon
 */
public class ClientEngineServantLocator
    extends LocalObject
    implements ServantLocator
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(ClientEngineServantLocator.class);
    }

    private ClientEngineImpl m_clientEngineImpl;
    private org.omg.CORBA.ORB m_orb;
    
    public ClientEngineServantLocator(ClientEngineImpl eImpl,
                                org.omg.CORBA.ORB orb)
    {
        m_clientEngineImpl = eImpl;
        m_orb = orb;
    }
  
    public org.omg.PortableServer.Servant preinvoke(byte[] oid,
                                                    POA the_poa,
                                                    String operation,
                                                    CookieHolder the_cookie)
        throws ForwardRequest
    {
        String clientEngineImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Preinvoke called for clientEngineImpl id [" + clientEngineImpl_id + "]");

        if(m_clientEngineImpl == null) {
            m_clientEngineImpl = new ClientEngineImpl();
        }
        ClientEngineServant eServant = new ClientEngineServant(the_poa, m_clientEngineImpl);
        return eServant;
    }
  

    public void postinvoke(byte[] oid,
                           POA the_poa,
                           String operation,
                           java.lang.Object the_cookie,
                           org.omg.PortableServer.Servant the_servant)
    {
        String clientEngineImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Postinvoke called for clientEngine id [" + clientEngineImpl_id + "]");

        if (the_servant instanceof ClientEngineServant) {
            ClientEngineServant eServant = (ClientEngineServant) the_servant;
            //eServant.cleanup();
        }
    }

}






