/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: ClientEngineServantActivator.java,v 1.3 2001/08/13 06:09:03 hyoon Exp $ 
 */ 
package deaf.client;

import  deaf.util.*;

import  java.io.*;
import  java.util.*;
 
import  org.apache.log4j.*;
import  org.omg.CORBA.*;
import  org.omg.CORBA.portable.*;
import  org.omg.PortableServer.*;
import  org.omg.PortableServer.POA.*;


/**
 * ClientEngineServantActivator
 *
 * @version $Revision: 1.3 $
 * @author Hyoungsoo Yoon
 */
public class ClientEngineServantActivator
    extends LocalObject
    implements ServantActivator
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(ClientEngineServantActivator.class);
    }

    private ClientEngineImpl  m_clientEngineImpl;
    private org.omg.CORBA.ORB m_orb;

    public ClientEngineServantActivator(ClientEngineImpl eImpl,
                                  org.omg.CORBA.ORB orb)
    {
        m_clientEngineImpl = eImpl;
        m_orb = orb;
    }

  
    public Servant incarnate(byte[] oid,
                             POA the_poa)
        throws ForwardRequest
    {
        String clientEngineImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Incarnating clientEngineImpl id [" + clientEngineImpl_id + "]");

        if(m_clientEngineImpl == null) {
            m_clientEngineImpl = new ClientEngineImpl();
        }
        ClientEngineServant eServant = new ClientEngineServant(the_poa, m_clientEngineImpl);

        return eServant;
    }

    public void etherealize(byte[] oid,
                            org.omg.PortableServer.POA the_poa,
                            org.omg.PortableServer.Servant the_servant,
                            boolean cleanup_in_progress,
                            boolean remaining_activations)
    {
        String clientEngineImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Etherealizing clientEngineImpl id [" + clientEngineImpl_id +"]");

        if (the_servant instanceof ClientEngineServant) {
            ClientEngineServant eServant = (ClientEngineServant) the_servant;
            //eServant.cleanup();
        }
    }

}







