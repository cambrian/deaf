/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: ClientManagerServantLocator.java,v 1.3 2001/08/13 06:09:03 hyoon Exp $ 
 */ 
package deaf.client;

import  deaf.util.*;
 
import  java.io.*;
import  java.util.*;
 
import  org.apache.log4j.*;
import  org.omg.CORBA.*;
import  org.omg.CORBA.portable.*;
import  org.omg.PortableServer.*;
import  org.omg.PortableServer.POA.*;
import  org.omg.PortableServer.ServantLocatorPackage.*;


/**
 * ClientManagerServantLocator
 *
 * @version $Revision: 1.3 $
 * @author Hyoungsoo Yoon
 */
public class ClientManagerServantLocator
    extends LocalObject
    implements ServantLocator
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(ClientManagerServantLocator.class);
    }

    private ClientManagerImpl m_clientManagerImpl;
    private org.omg.CORBA.ORB m_orb;
    
    public ClientManagerServantLocator(ClientManagerImpl mImpl,
                                org.omg.CORBA.ORB orb)
    {
        m_clientManagerImpl = mImpl;
        m_orb = orb;
    }
  
    public org.omg.PortableServer.Servant preinvoke(byte[] oid,
                                                    POA the_poa,
                                                    String operation,
                                                    CookieHolder the_cookie)
        throws ForwardRequest
    {
        String clientManagerImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Preinvoke called for clientManagerImpl id [" + clientManagerImpl_id + "]");

        if(m_clientManagerImpl == null) {
            m_clientManagerImpl = new ClientManagerImpl();
        }
        ClientManagerServant mServant = new ClientManagerServant(the_poa, m_clientManagerImpl);
        return mServant;
    }
  

    public void postinvoke(byte[] oid,
                           POA the_poa,
                           String operation,
                           java.lang.Object the_cookie,
                           org.omg.PortableServer.Servant the_servant)
    {
        String clientManagerImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Postinvoke called for clientManagerImpl id [" + clientManagerImpl_id + "]");

        if (the_servant instanceof ClientManagerServant) {
            ClientManagerServant mServant = (ClientManagerServant) the_servant;
            //mServant.cleanup();
        }
    }

}






