/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: SkinEngineServantActivator.java,v 1.3 2001/08/13 06:09:03 hyoon Exp $ 
 */ 
package deaf.client;

import  deaf.util.*;

import  java.io.*;
import  java.util.*;
 
import  org.apache.log4j.*;
import  org.omg.CORBA.*;
import  org.omg.CORBA.portable.*;
import  org.omg.PortableServer.*;
import  org.omg.PortableServer.POA.*;


/**
 * SkinEngineServantActivator
 *
 * @version $Revision: 1.3 $
 * @author Hyoungsoo Yoon
 */
public class SkinEngineServantActivator
    extends LocalObject
    implements ServantActivator
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(SkinEngineServantActivator.class);
    }

    private SkinEngineImpl  m_skinEngineImpl;
    private org.omg.CORBA.ORB m_orb;

    public SkinEngineServantActivator(SkinEngineImpl eImpl,
                                  org.omg.CORBA.ORB orb)
    {
        m_skinEngineImpl = eImpl;
        m_orb = orb;
    }

  
    public Servant incarnate(byte[] oid,
                             POA the_poa)
        throws ForwardRequest
    {
        String skinEngineImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Incarnating skinEngineImpl id [" + skinEngineImpl_id + "]");

        if(m_skinEngineImpl == null) {
            m_skinEngineImpl = new SkinEngineImpl();
        }
        SkinEngineServant eServant = new SkinEngineServant(the_poa, m_skinEngineImpl);

        return eServant;
    }

    public void etherealize(byte[] oid,
                            org.omg.PortableServer.POA the_poa,
                            org.omg.PortableServer.Servant the_servant,
                            boolean cleanup_in_progress,
                            boolean remaining_activations)
    {
        String skinEngineImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Etherealizing skinEngineImpl id [" + skinEngineImpl_id +"]");

        if (the_servant instanceof SkinEngineServant) {
            SkinEngineServant eServant = (SkinEngineServant) the_servant;
            //eServant.cleanup();
        }
    }

}







