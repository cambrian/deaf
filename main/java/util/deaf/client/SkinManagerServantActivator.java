/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: SkinManagerServantActivator.java,v 1.3 2001/08/13 06:09:03 hyoon Exp $ 
 */ 
package deaf.client;

import  deaf.util.*;

import  java.io.*;
import  java.util.*;
 
import  org.apache.log4j.*;
import  org.omg.CORBA.*;
import  org.omg.CORBA.portable.*;
import  org.omg.PortableServer.*;
import  org.omg.PortableServer.POA.*;


/**
 * SkinManagerServantActivator
 *
 * @version $Revision: 1.3 $
 * @author Hyoungsoo Yoon
 */
public class SkinManagerServantActivator
    extends LocalObject
    implements ServantActivator
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(SkinManagerServantActivator.class);
    }

    private SkinManagerImpl  m_skinManagerImpl;
    private org.omg.CORBA.ORB m_orb;

    public SkinManagerServantActivator(SkinManagerImpl mImpl,
                                  org.omg.CORBA.ORB orb)
    {
        m_skinManagerImpl = mImpl;
        m_orb = orb;
    }

  
    public Servant incarnate(byte[] oid,
                             POA the_poa)
        throws ForwardRequest
    {
        String skinManagerImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Incarnating skinManagerImpl id [" + skinManagerImpl_id + "]");

        if(m_skinManagerImpl == null) {
            m_skinManagerImpl = new SkinManagerImpl();
        }
        SkinManagerServant mServant = new SkinManagerServant(the_poa, m_skinManagerImpl);

        return mServant;
    }

    public void etherealize(byte[] oid,
                            org.omg.PortableServer.POA the_poa,
                            org.omg.PortableServer.Servant the_servant,
                            boolean cleanup_in_progress,
                            boolean remaining_activations)
    {
        String skinManagerImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Etherializing skinManagerImpl id [" + skinManagerImpl_id + "]");

        if (the_servant instanceof SkinManagerServant) {
            SkinManagerServant mServant = (SkinManagerServant) the_servant;
            //mServant.cleanup();
        }
    }

}







