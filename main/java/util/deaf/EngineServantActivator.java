/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: EngineServantActivator.java,v 1.3 2001/08/13 06:09:03 hyoon Exp $ 
 */ 
package deaf;

import  deaf.util.*;

import  java.io.*;
import  java.util.*;
 
import  org.apache.log4j.*;
import  org.omg.CORBA.*;
import  org.omg.CORBA.portable.*;
import  org.omg.PortableServer.*;
import  org.omg.PortableServer.POA.*;


/**
 * EngineServantActivator
 *
 * @version $Revision: 1.3 $
 * @author Hyoungsoo Yoon
 */
public class EngineServantActivator
    extends LocalObject
    implements ServantActivator
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(EngineServantActivator.class);
    }

    private EngineImpl  m_engineImpl;
    private org.omg.CORBA.ORB m_orb;

    public EngineServantActivator(EngineImpl eImpl,
                                  org.omg.CORBA.ORB orb)
    {
        m_engineImpl = eImpl;
        m_orb = orb;
    }

  
    public Servant incarnate(byte[] oid,
                             POA the_poa)
        throws ForwardRequest
    {
        String engineImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Incarnating engineImpl id [" + engineImpl_id + "]");

        if(m_engineImpl == null) {
            m_engineImpl = new EngineImpl();
        }
        EngineServant eServant = new EngineServant(the_poa, m_engineImpl);

        return eServant;
    }

    public void etherealize(byte[] oid,
                            org.omg.PortableServer.POA the_poa,
                            org.omg.PortableServer.Servant the_servant,
                            boolean cleanup_in_progress,
                            boolean remaining_activations)
    {
        String engineImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Etherealizing engineImpl id [" + engineImpl_id +"]");

        if (the_servant instanceof EngineServant) {
            EngineServant eServant = (EngineServant) the_servant;
            //eServant.cleanup();
        }
    }

}







