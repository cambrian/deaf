/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: AppServiceServantLocator.java,v 1.3 2001/08/13 06:09:03 hyoon Exp $ 
 */ 
package deaf;

import  deaf.util.*;
 
import  java.io.*;
import  java.util.*;
 
import  org.apache.log4j.*;
import  org.omg.CORBA.*;
import  org.omg.CORBA.portable.*;
import  org.omg.PortableServer.*;
import  org.omg.PortableServer.POA.*;
import  org.omg.PortableServer.ServantLocatorPackage.*;


/**
 * AppServiceServantLocator
 *
 * @version $Revision: 1.3 $
 * @author Hyoungsoo Yoon
 */
public class AppServiceServantLocator
    extends LocalObject
    implements ServantLocator
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(AppServiceServantLocator.class);
    }

    private AppServiceImpl m_appServiceImpl;
    private org.omg.CORBA.ORB m_orb;
    
    public AppServiceServantLocator(AppServiceImpl sImpl,
                                org.omg.CORBA.ORB orb)
    {
        m_appServiceImpl = sImpl;
        m_orb = orb;
    }
  
    public org.omg.PortableServer.Servant preinvoke(byte[] oid,
                                                    POA the_poa,
                                                    String operation,
                                                    CookieHolder the_cookie)
        throws ForwardRequest
    {
        String appServiceImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Preinvoke called for appServiceImpl id [" + appServiceImpl_id + "]");

        if(m_appServiceImpl == null) {
            m_appServiceImpl = new AppServiceImpl();
        }
        AppServiceServant sServant = new AppServiceServant(the_poa, m_appServiceImpl);
        return sServant;
    }
  

    public void postinvoke(byte[] oid,
                           POA the_poa,
                           String operation,
                           java.lang.Object the_cookie,
                           org.omg.PortableServer.Servant the_servant)
    {
        String appServiceImpl_id = new String(oid);
        if(logCat.isDebugEnabled())
            logCat.debug("Postinvoke called for appServiceImpl id [" + appServiceImpl_id + "]");

        if (the_servant instanceof AppServiceServant) {
            AppServiceServant sServant = (AppServiceServant) the_servant;
            //sServant.cleanup();
        }
    }

}






