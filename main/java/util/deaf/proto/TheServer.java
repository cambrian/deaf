/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: TheServer.java,v 1.3 2001/08/13 06:09:03 hyoon Exp $ 
 */ 
package deaf.proto;

import  deaf.*;
import  deaf.util.*;

import  java.io.*;
import  java.util.*;
 
import  org.apache.log4j.*;
import  org.omg.CORBA.*;
import  org.omg.PortableServer.*;



/**
 * TheServer runs the SimpleService in background.
 *
 * @version $Revision: 1.3 $
 * @author Hyoungsoo Yoon
 */
public class TheServer implements deaf.CommonConstants
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(TheServer.class);
    }

    /**
     * Default constructor. Currently, it does nothing.
     */
    public TheServer()
    {
    }

    /**
     * Starts the server in a separate thread and waits for requests.
     */
    public void runServer(String args[]) 
    {
        // Start the service.
        SimpleService service = new SimpleService();
        ORB global_orb = service.startService(args);
        Thread theThread = new Thread(service);
        theThread.start();
    }

    /**
     * Start the server.
     */
    public static void main (String args[]) 
    {
        // Default configuration of Log4j
        BasicConfigurator.configure();
            
        try {
            TheServer server = new TheServer();
            server.runServer(args);
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }

}



