/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: TheClient.java,v 1.4 2001/08/15 05:55:20 hyoon Exp $ 
 */ 
package deaf.proto;

import  deaf.*;
import  deaf.util.*;

import  java.io.*;
import  java.util.*;
 
import  org.apache.log4j.*;
import  org.omg.CORBA.*;


/**
 * TheClient runs the SimpleClientelle in background.
 *
 * @version $Revision: 1.4 $
 * @author Hyoungsoo Yoon
 */
public class TheClient implements deaf.CommonConstants
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(TheClient.class);
    }


    /**
     * Default constructor. Currently, it does nothing.
     */
    public TheClient()
    {
    }

    /**
     * Starts the client in a separate thread and waits for requests.
     */
    public void runClient(String args[]) 
    {
        // Start the clientele.
        SimpleClientele clientele = new SimpleClientele();
        ORB global_orb = clientele.startClientele(args);
        //Thread theThread = new Thread(clientele);
        //theThread.start();
        clientele.runClientele();
    }

    /**
     * Start the client.
     */
    public static void main (String args[]) 
    {
        // Default configuration of Log4j
        BasicConfigurator.configure();
            
        try {
            TheClient client = new TheClient();
            client.runClient(args);
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }

}



