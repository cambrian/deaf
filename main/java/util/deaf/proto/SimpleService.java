/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: SimpleService.java,v 1.4 2001/08/15 05:55:20 hyoon Exp $ 
 */ 
package deaf.proto;

import  deaf.*;
import  deaf.util.*;

import  java.io.*;
import  java.util.*;
 
import  org.apache.log4j.*;
import  org.omg.CORBA.*;
import  org.omg.PortableServer.*;


/**
 * SimpleService is a wrapper class for Deaf AppService.
 * It fires up a Deaf App server and writes its IOR to a file.
 *
 * @version $Revision: 1.4 $
 * @author Hyoungsoo Yoon
 */
public class SimpleService implements Runnable, deaf.CommonConstants, SharedConstants
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(SimpleService.class);
    }

    /**
     * Global ORB
     */
    public static org.omg.CORBA_2_3.ORB global_orb = null;

    // Whether to use the CORBA naming service.
    private boolean  m_bUseNameService = false;

    
    /**
     * Default constructor. Currently, it does nothing.
     */
    public SimpleService()
    {
    }

    /**
     * Constructor with the pre-inilialized ORB.
     *
     * @param orb  the global ORB 
     */
    public SimpleService(ORB orb)
    {
        global_orb = (org.omg.CORBA_2_3.ORB) orb;
    }

    /**
     * Runnable.run()
     */
    public void run()
    {
        runService();
    }

    /**
     * Initialize the ORB and POA.
     *
     * @param  args  the Command line argument.
     * @return ORB the global orb initialized by the application.
     */
    public ORB startService(String[] args) 
    { 
        try 
        {
            // Initializing the global ORB
            if(global_orb == null) {
                logCat.info("Initializing the ORB");
                global_orb = (org.omg.CORBA_2_3.ORB) ORB.init(args, null);
            }            

            // Initializing POA
            org.omg.CORBA.Object poa_obj = global_orb.resolve_initial_references("RootPOA");
            org.omg.PortableServer.POA root_poa = org.omg.PortableServer.POAHelper.narrow(poa_obj);
            org.omg.PortableServer.POAManager poa_manager = root_poa.the_POAManager();
          

            // Check whether to use the CORBA naming service.
            String useNameService = ServerUtility.parseArgs(args, "-nameservice");
            if (NAMESERVICE_YES.equals(useNameService))
                m_bUseNameService = true;
            else
                m_bUseNameService = false;
            logCat.info("useNameService = " + m_bUseNameService);


            org.omg.CosNaming.NamingContextExt default_context = null;
            org.omg.CosNaming.NamingContext app_context = null;
            if(m_bUseNameService)
            {
                // Create an app naming context in the NameService.
                org.omg.CORBA.Object tmp_ref = global_orb.resolve_initial_references("NameService");
                default_context = org.omg.CosNaming.NamingContextExtHelper.narrow(tmp_ref);
                if(default_context == null)
                {
                    logCat.error("Failed to get reference to the NameService");
                    System.exit(1);
                }
                app_context = ServerUtility.create_app_context(default_context, PROTO_APPLICATION_NAME);
            }


            // Create servants, activate them, write their IORs to files.
            logCat.info("Creating objects");
            String strategy = ServerUtility.parseArgs(args, "-strategy");
            logCat.info("strategy = " + strategy);
            POA appSrv_poa = null;

            // Create the impl object.
            AppServiceImpl appSrv = new AppServiceImpl();
            String adapterName = "appService";
            byte[] appSrv_oid = adapterName.getBytes(); 

            if (STRATEGY_ACTIVATOR.equals(strategy))
            {
                 logCat.info("Creating and registering the ServantActivator.");
                 appSrv_poa = ServantUtility.create_activator_poa(root_poa, adapterName, poa_manager);
                 AppServiceServantActivator activator_impl = new AppServiceServantActivator(appSrv, global_orb);
                 ServantActivator servant_activator = activator_impl;
                 appSrv_poa.set_servant_manager(servant_activator);
            }
            else if (STRATEGY_LOCATOR.equals(strategy))
            {
                 logCat.info("Creating and registering the ServantLocator.");
                 appSrv_poa = ServantUtility.create_locator_poa(root_poa, adapterName, poa_manager);
                 AppServiceServantLocator locator_impl = new AppServiceServantLocator(appSrv, global_orb);
                 ServantLocator servant_locator = locator_impl;
                 appSrv_poa.set_servant_manager(servant_locator);
            }
            else if (STRATEGY_DEFAULTSERVANT.equals(strategy))
            {
                 logCat.info("Registering the default servant.");
                 appSrv_poa = ServantUtility.create_defaultservant_poa(root_poa, adapterName, poa_manager);
                 AppServiceServant app_servant = new AppServiceServant(appSrv);
                 appSrv_poa.set_servant(app_servant);
            }
            else
            {
                 logCat.info("Activating the servant.");
                 appSrv_poa = ServantUtility.create_simple_poa(root_poa, adapterName, poa_manager);
                 AppServiceServant app_servant = new AppServiceServant(appSrv);
                 appSrv_poa.activate_object_with_id(appSrv_oid, app_servant);
            }


            // Export the object reference.
            if(m_bUseNameService)
            {
                org.omg.CORBA.Object tmp_ref = appSrv_poa.create_reference_with_id(appSrv_oid, AppServiceHelper.id());
                org.omg.CosNaming.NameComponent [] name = default_context.to_name(PROTO_APPSERVICE_NAME);
                app_context.rebind(name, tmp_ref);
            }
            else
            {
                // Write the IOR to a file.
                String iorFile = SHARED_IOR_FOLDER + File.separator + APPSERVICE_IOR_FILE;
                ServerUtility.export_object(global_orb, appSrv_poa, appSrv_oid, iorFile, AppServiceHelper.id());
            }
            
            // Activate the POA Manager to allow new requests to arrive
            logCat.info("Activating the POA Manager");
            poa_manager.activate();
        }
        catch(org.omg.CORBA.ORBPackage.InvalidName ex)
        {
            logCat.error("Failed to obtain root poa " + ex );
        }
        catch(org.omg.PortableServer.POAPackage.WrongPolicy ex)
        {
            logCat.error("Invalid POA policy " + ex );
        }
        catch(org.omg.PortableServer.POAPackage.ServantAlreadyActive ex)
        {
            logCat.error("POA servant already active " + ex );
        }
        catch(org.omg.PortableServer.POAManagerPackage.AdapterInactive ex)
        {
            logCat.error("POAManager activate failed" + ex );
        } 
        catch (org.omg.CORBA.SystemException ex)
        {
            logCat.error(ex);      
            System.exit(ex.minor);
        }
        catch (Exception ex)
        {
            logCat.error(ex);
            System.exit(1);
        }

        // Return the global ORB so that other classes can access it.
        return global_orb;
    }

    /**
     * Start the ORB and wait for incoming requests.
     * It automatially shuts down the ORB when it's done.
     */
    public void runService()
    { 
        try 
        {
            // Give control to the ORB to let it process incoming requests
            logCat.info("Giving control to the ORB to process requests");
            global_orb.run();
            logCat.info ("SimpleService Done.");
        }
        catch (org.omg.CORBA.SystemException ex)
        {
            logCat.error(ex);      
            System.exit(ex.minor);
        }
        catch (Exception ex)
        {
            logCat.error(ex);
            System.exit(1);
        }
        finally
        {
            /*
            if (global_orb != null)
            {
                try {
                    global_orb.shutdown(true);
                } catch (Exception ex) {
                    // do nothing
                }
            }
            */
        }
    }

    /**
     * Test routine.
     */
    public static void main (String args[]) 
    {
        // Default configuration of Log4j
        BasicConfigurator.configure();
            
        try {
            SimpleService service = new SimpleService();
            service.startService(args);
            service.runService();
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }

}





