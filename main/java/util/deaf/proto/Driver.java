/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: Driver.java,v 1.4 2001/08/15 05:55:20 hyoon Exp $ 
 */ 
package deaf.proto;

import  deaf.*;
import  deaf.util.*;

import  java.io.*;
import  java.util.*;
 
import  org.apache.log4j.*;
import  org.omg.CORBA.*;
import  org.omg.PortableServer.*;


/**
 * Driver starts SimpleService in background,
 * and starts the test driver, SimpleClientele.
 *
 * @version $Revision: 1.4 $
 * @author Hyoungsoo Yoon
 */
public class Driver implements CommonConstants
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(Driver.class);
    }

    /**
     * Global ORB
     */
    public static org.omg.CORBA_2_3.ORB global_orb = null;

    /**
     * Default constructor. Currently, it does nothing.
     */
    public Driver()
    {
    }
    
    /**
     * Starts all events services and tests them with test routines.
     */
    public void runDriver(String args[])
    {
        try 
        {
            logCat.info("Starting test driver");
            
            // Starting the service.
            SimpleService service = new SimpleService(global_orb);
            global_orb = (org.omg.CORBA_2_3.ORB) service.startService(args);
            Thread sThread = new Thread(service);
            sThread.start();

            // Give enough time.
            try {
                Thread.sleep(2000);
            } catch(InterruptedException ex) {
                // ignore and move on
            }

            // Starting the client.
            SimpleClientele clientele = new SimpleClientele(global_orb);
            global_orb = (org.omg.CORBA_2_3.ORB) clientele.startClientele(args);
            //Thread cThread = new Thread(clientele);
            //cThread.start();
            clientele.runClientele();
            

            // Done.
            logCat.info("Done!");
        }
        catch (org.omg.CORBA.SystemException ex)
        {
            System.err.println(ex);      
            System.exit(ex.minor);
        }
        catch (Exception ex)
        {
            System.err.println(ex);
            System.exit(1);
        }
        finally
        {
            if (global_orb != null)
            {
                try {
                    global_orb.shutdown(true);
                } catch (Exception ex) {
                    /* do nothing */
                }
            }
        }
    }
    
    /**
     * Initialize the global ORB.
     */
    public void initialize(String args[]) 
    { 
        try 
        {
            // Initializing the global ORB
            if(global_orb == null) {
                System.out.println("Initializing the ORB");
                global_orb = (org.omg.CORBA_2_3.ORB) ORB.init(args, null);
            }
        }
        catch (org.omg.CORBA.SystemException ex)
        {
            System.err.println(ex);      
            System.exit(ex.minor);
        }
        catch (Exception ex)
        {
            System.err.println(ex);
            System.exit(1);
        }
    }

    /**
     * Main routine.
     */
    public static void main (String args[]) 
    {
        // Default configuration of Log4j
        BasicConfigurator.configure();
            
        try {
            Driver driver = new Driver();
            driver.initialize(args);
            driver.runDriver(args);
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }

}




