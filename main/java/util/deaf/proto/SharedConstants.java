/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: SharedConstants.java,v 1.3 2001/08/13 06:09:03 hyoon Exp $ 
 */ 
package deaf.proto;


/**
 * SharedConstants is a collection of variables used in deaf.proto project.
 *
 * @version $Revision: 1.3 $
 * @author Hyoungsoo Yoon
 */
public interface SharedConstants
{
    /**
     * Default folder name for the IORs (Can be absolute or relative to the CWD)
     */
    static final String SHARED_IOR_FOLDER = ".";

    /**
     * Default file name for the IOR of Deaf-based Server
     */
    static final String APPSERVICE_IOR_FILE = "appservice.ior";

    /**
     * Application name for proto sample.
     */
    static final String PROTO_APPLICATION_NAME = "Deaf_ProtoApp";

    /**
     * AppService name for proto sample.
     */
    static final String PROTO_APPSERVICE_NAME = "Deaf_AppService";


}




