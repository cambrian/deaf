/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: SimpleClientele.java,v 1.5 2001/08/17 06:10:16 hyoon Exp $ 
 */ 
package deaf.proto;

import  deaf.*;
import  deaf.app.*;
import  deaf.util.*;

import  java.io.*;
import  java.util.*;
 
import  org.apache.log4j.*;
import  org.omg.CORBA.*;


/**
 * SimpleClientele is a simple driver class for TheServer.
 *
 * @version $Revision: 1.5 $
 * @author Hyoungsoo Yoon
 */
public class SimpleClientele implements Runnable, deaf.CommonConstants, SharedConstants
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(SimpleClientele.class);
    }


    /**
     * Global ORB
     */
    public static org.omg.CORBA_2_3.ORB global_orb = null;

    // Whether to use the CORBA naming service.
    private boolean  m_bUseNameService = false;

    // The reference to the AppService proxy object.
    private AppServiceProxy m_appServiceProxy = null;


    /**
     * Default constructor. Currently, it does nothing.
     */
    public SimpleClientele()
    {
    }

    /**
     * Constructor with the pre-inilialized ORB.
     *
     * @param orb  the global ORB 
     */
    public SimpleClientele(ORB orb)
    {
        global_orb = (org.omg.CORBA_2_3.ORB) orb;
    }


    /**
     * Runnable.run()
     */
    public void run()
    {
        runClientele();
    }


    /**
     * Runnable.run()
     */
    public AppServiceProxy getApp()
    {
        return m_appServiceProxy;
    }



    /**
     * Intializes the ORB and constructs the AppServiceProxy.
     *
     * @param  args  the Command line argument.
     * @return ORB the global orb initialized by the application.
     */
    public ORB startClientele(String[] args)
    {
        try 
        {
            // Initializing the global ORB
            if(global_orb == null) {
                logCat.info("Initializing the ORB");
                global_orb = (org.omg.CORBA_2_3.ORB) ORB.init(args, null);
            }            

            // Check whether to use the CORBA naming service.
            String useNameService = ClientUtility.parseArgs(args, "-nameservice");
            if (NAMESERVICE_YES.equals(useNameService))
                m_bUseNameService = true;
            else
                m_bUseNameService = false;
            logCat.info("useNameService = " + m_bUseNameService);

            org.omg.CosNaming.NamingContextExt default_context = null;
            org.omg.CosNaming.NamingContext app_context = null;
            if(m_bUseNameService)
            {
                // Create an app naming context in the NameService.
                org.omg.CORBA.Object tmp_ref = global_orb.resolve_initial_references("NameService");
                default_context = org.omg.CosNaming.NamingContextExtHelper.narrow(tmp_ref);
                if(default_context == null)
                {
                    logCat.error("Failed to get reference to the NameService");
                    System.exit(1);
                }
                tmp_ref = default_context.resolve_str(PROTO_APPLICATION_NAME);
                app_context = org.omg.CosNaming.NamingContextHelper.narrow(tmp_ref);
            }


            // Create AppServiceProxy.
            logCat.info("Creating AppServiceProxy");
            AppService aServiceRef = null;

            if(m_bUseNameService)
            {
                org.omg.CORBA.Object objref1 = ClientUtility.resolve_object_name(default_context, app_context, PROTO_APPSERVICE_NAME);
                aServiceRef = AppServiceHelper.narrow(objref1); 
            }
            else
            {
                String iorFile = SHARED_IOR_FOLDER + File.separator + APPSERVICE_IOR_FILE;
                org.omg.CORBA.Object objref2 = ClientUtility.import_object(global_orb, iorFile);
                aServiceRef = AppServiceHelper.narrow(objref2); 
            }

            m_appServiceProxy = new AppServiceProxy(aServiceRef);
            logCat.info("AppServiceProxy created.");
        }
        catch (org.omg.CORBA.SystemException ex)
        {
            logCat.error(ex);      
            System.exit(ex.minor);
        }
        catch (Exception ex)
        {
            logCat.error(ex);
            System.exit(1);
        }

        // Return the global ORB so that other classes can access it.
        return global_orb;
    }

    /**
     * Runs the test routines to the AppService.
     * It automatially shuts down the ORB when it's done.
     */
    public void runClientele()
    { 
        try 
        {
            // Invoke methods.
            logCat.info("Calling AppService.getAppManager()");
            try {
                AppManagerProxy appMgr = getApp().getAppManager();
                logCat.info("AppManager = " + appMgr);
            } catch (AppException aex) {
                logCat.error(aex);
            } catch (Exception ex) {  // temporary
                logCat.error(ex);
            }
            
            
            // Give enough time to finish off all other method calls.
            try {
                Thread.sleep(2000);
            } catch(InterruptedException ex) {
                // ignore and move on
            }

            /*
            logCat.info("Calling AppService.shutdown()");
            try {
               getApp().shutdown();
            } catch (AppException aex) {
                logCat.error(aex);
            } catch (Exception ex) {  // temporary
                logCat.error(ex);
            }
            */

            logCat.info ("SimpleClientele Done.");
        }
        catch (org.omg.CORBA.SystemException ex)
        {
            logCat.error(ex);      
            System.exit(ex.minor);
        }
        catch (Exception ex)
        {
            logCat.error(ex);
            System.exit(1);
        }
        finally
        {
            if (global_orb != null)
            {
                try {
                    global_orb.shutdown(true);
                } catch (Exception ex) {
                    // do nothing 
                }
            }
        }
    }

    
    /**
     * Test routine.
     */
    public static void main (String args[]) 
    {
        // Default configuration of Log4j
        BasicConfigurator.configure();
            
        try {
            SimpleClientele clientele = new SimpleClientele();
            clientele.startClientele(args);
            clientele.runClientele();
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }

}




