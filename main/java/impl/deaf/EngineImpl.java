/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: EngineImpl.java,v 1.5 2001/08/17 06:10:15 hyoon Exp $ 
 */ 
package deaf;

import  org.apache.log4j.*;


public class EngineImpl
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(EngineImpl.class);
    }
    static protected String toNonNullString(Object o)
    {
        return (o == null) ? "_nil" : o.toString();
    }



    //
    private EngineHandle  m_handle = null;
    private ManagerImpl   m_manager = null;

    
    /**
     * Constructor.
     */
    public EngineImpl()
    {

    }
    
    /**
     * Constructor.
     */
    public EngineImpl(Manager mgr)
    {
        //m_manager = mgr;
    }
    


    ///////////////////////////////////////////////////////////////////////
    //
    // Methods defined in EngineOperations
    //
    ///////////////////////////////////////////////////////////////////////

    public deaf.EngineHandle getHandle() throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
           logCat.debug("Entering getHandle().");
        }
        
        return m_handle;
	}


    public deaf.ManagerProxy getManager() throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering getManager().");
        }
        
        return new ManagerProxy();
	}


    public boolean isIdentical(
        deaf.Engine engn
    ) throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering isIdentical(). Engine=" + toNonNullString(engn));
        }
        
        return false;
	}


    public void remove(
        deaf.SessionToken token
    ) throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering remove(). SessionToken=" + toNonNullString(token));
        }
        
	}


    public void invoke(
        deaf.DynRequest req,
        deaf.DynResponseHolder res
    ) throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering invoke(). DynRequest=" + toNonNullString(req) + ";DynResponseHolder=" + toNonNullString(res));
        }
        
	}



    ///////////////////////////////////////////////////////////////////////
    //
    // Server-side API's
    //
    ///////////////////////////////////////////////////////////////////////


}



