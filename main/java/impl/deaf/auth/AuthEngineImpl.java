/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: AuthEngineImpl.java,v 1.4 2001/08/17 06:10:15 hyoon Exp $ 
 */ 
package deaf.auth;

import deaf.*;

import org.omg.CORBA.*;


public class AuthEngineImpl
    extends EngineImpl
{
    public deaf.auth.AuthenticationManagerProxy getAuthenticationManager() throws deaf.AppException
	{
        return null;
	}


    public deaf.auth.AuthorizationManagerProxy getAuthorizationManager() throws deaf.AppException
	{
        return null;
	}


    public deaf.auth.SessionManagerProxy getSessionManager() throws deaf.AppException
	{
        return null;
	}


}



