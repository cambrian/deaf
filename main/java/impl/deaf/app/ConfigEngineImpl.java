/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: ConfigEngineImpl.java,v 1.5 2001/08/17 06:10:15 hyoon Exp $ 
 */ 
package deaf.app;

import  deaf.*;

import  org.apache.log4j.*;
import  org.omg.CORBA.*;


public class ConfigEngineImpl
    extends EngineImpl
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(ConfigEngineImpl.class);
    }

    
    /**
     * Constructor.
     */
    public ConfigEngineImpl()
    {

    }
    
    

    ///////////////////////////////////////////////////////////////////////
    //
    // Methods defined in ConfigEngineOperations
    //
    ///////////////////////////////////////////////////////////////////////

    public deaf.admin.PropManagerProxy getPropManager() throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering getPropManager().");
        }

        return null;
	}


    public deaf.ManagerHandle[] listAllModules(
        deaf.SessionToken token
    ) throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering listAllModules(). SessionToken=" + token);
        }


        return null;
	}


    public void registerModule(
        deaf.SessionToken token,
        deaf.ManagerHandle hand
    ) throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering registerModule(). SessionToken=" + token + "; ManagerHandle=" + hand);
        }



	}


    public void unregisterModule(
        deaf.SessionToken token,
        deaf.ManagerHandle hand
    ) throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering unregisterModule(). SessionToken=" + token + "; ManagerHandle=" + hand);
        }


	}



    ///////////////////////////////////////////////////////////////////////
    //
    // Server-side API's
    //
    ///////////////////////////////////////////////////////////////////////


    
}



