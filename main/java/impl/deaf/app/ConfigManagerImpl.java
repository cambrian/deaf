/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: ConfigManagerImpl.java,v 1.5 2001/08/17 06:10:15 hyoon Exp $ 
 */ 
package deaf.app;

import  deaf.*;

import  org.apache.log4j.*;
import  org.omg.CORBA.*;


public class ConfigManagerImpl
    extends ManagerImpl
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(ConfigManagerImpl.class);
    }

    
    /**
     * Constructor.
     */
    public ConfigManagerImpl()
    {

    }
    


    ///////////////////////////////////////////////////////////////////////
    //
    // Server-side API's
    //
    ///////////////////////////////////////////////////////////////////////

    

}



