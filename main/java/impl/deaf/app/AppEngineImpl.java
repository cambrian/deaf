/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: AppEngineImpl.java,v 1.5 2001/08/17 06:10:15 hyoon Exp $ 
 */ 
package deaf.app;

import  deaf.*;
import  deaf.admin.*;
import  deaf.client.*;

import  org.apache.log4j.*;
import  org.omg.CORBA.*;


public class AppEngineImpl
    extends EngineImpl
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(AppEngineImpl.class);
    }


    //
    ConfigManagerProxy   m_configMgr = null;
    LocatorManagerProxy  m_locatorMgr = null;

    //
    ClientManagerProxy[] m_clientMgrList = new ClientManagerProxy[] {};
    ModuleManagerProxy[] m_moduleMgrList = new ModuleManagerProxy[] {};

    
    /**
     * Constructor.
     */
    public AppEngineImpl()
    {

    }
    
    

    ///////////////////////////////////////////////////////////////////////
    //
    // Methods defined in AppEngineOperations
    //
    ///////////////////////////////////////////////////////////////////////
    
    public deaf.app.ConfigManagerProxy getConfigManager() throws deaf.AppException
	{
        return m_configMgr;
	}


    public deaf.admin.LocatorManagerProxy getLocatorManager() throws deaf.AppException
	{
        return m_locatorMgr;
	}


    public deaf.client.ClientManagerProxy getDefaultClient() throws deaf.AppException
	{
        return null;
	}


    public deaf.client.ClientManagerProxy getClient(
        deaf.ManagerHandle hand
    ) throws deaf.AppException
	{
        return null;
	}


    public deaf.ManagerHandle[] getClientList() throws deaf.AppException
	{
        return null;
	}


    public deaf.app.ModuleManagerProxy getModule(
        deaf.SessionToken token,
        deaf.ManagerHandle hand
    ) throws deaf.AppException
	{
        return null;
	}


    public deaf.ManagerHandle[] getModuleList(
        deaf.SessionToken token
    ) throws deaf.AppException
	{
        return null;
	}


    public void startModule(
        deaf.SessionToken token,
        deaf.ManagerHandle hand
    ) throws deaf.AppException
	{

	}


    public void shutdownAllModules(
        deaf.SessionToken token
    ) throws deaf.AppException
	{

	}


    public void shutdownModule(
        deaf.SessionToken token,
        deaf.ManagerHandle hand
    ) throws deaf.AppException
	{

	}



    ///////////////////////////////////////////////////////////////////////
    //
    // Server-side API's
    //
    ///////////////////////////////////////////////////////////////////////


}



