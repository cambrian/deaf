/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: ManagerImpl.java,v 1.5 2001/08/17 06:10:15 hyoon Exp $ 
 */ 
package deaf;

import  org.apache.log4j.*;


public class ManagerImpl
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(ManagerImpl.class);
    }
    static protected String toNonNullString(Object o)
    {
        return (o == null) ? "_nil" : o.toString();
    }
        

    //
    private ManagerPolicy m_policy = null;
    private ManagerHandle m_handle = null;

    //
    private EngineProxy[]  m_engineList = new EngineProxy[] {};

    
    
    /**
     * Constructor.
     */
    public ManagerImpl()
    {

    }


    ///////////////////////////////////////////////////////////////////////
    //
    // Methods defined in ManagerOperations
    //
    ///////////////////////////////////////////////////////////////////////

    public deaf.ManagerPolicy policy()
	{
        if(logCat.isDebugEnabled()) {
           logCat.debug("Entering policy().");
        }
        
        return m_policy;
	}


    public deaf.ServiceProxy getService() throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
           logCat.debug("Entering getService().");
        }
        
        return null;

	}


    public deaf.ManagerHandle getHandle() throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
           logCat.debug("Entering getHandle().");
        }
        
        return m_handle;
	}


    public deaf.EngineHandle createEngine(
        deaf.SessionToken token
    ) throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
           logCat.debug("Entering createEngine(). SessionToken=" + toNonNullString(token));
        }
        
        return null;
	}


    public deaf.EngineProxy getEngine(
        deaf.SessionToken token,
        deaf.EngineHandle hand
    ) throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
           logCat.debug("Entering createEngine(). SessionToken=" + toNonNullString(token) + "; EngineHandle=" + toNonNullString(hand));
        }
        
        return null;
	}


    public deaf.EngineHandle[] getEngineList(
        deaf.SessionToken token
    ) throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
           logCat.debug("Entering getEngineList(). SessionToken=" + toNonNullString(token));
        }
        
        return null;
	}


    public void removeEngine(
        deaf.SessionToken token,
        deaf.EngineHandle hand
    ) throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
           logCat.debug("Entering removeEngine(). SessionToken=" + toNonNullString(token) + "; EngineHandle=" + toNonNullString(hand));
        }

	}




    ///////////////////////////////////////////////////////////////////////
    //
    // Server-side API's
    //
    ///////////////////////////////////////////////////////////////////////

    

}



