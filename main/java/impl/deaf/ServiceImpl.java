/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: ServiceImpl.java,v 1.5 2001/08/17 06:10:15 hyoon Exp $ 
 */ 
package deaf;

import  org.apache.log4j.*;


public class ServiceImpl
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(ServiceImpl.class);
    }
    static protected String toNonNullString(Object o)
    {
        return (o == null) ? "_nil" : o.toString();
    }


    private int m_version = 0;
    private ServicePolicy m_policy = null;
    

    /**
     * Constructor.
     */
    public ServiceImpl()
    {

    }



    ///////////////////////////////////////////////////////////////////////
    //
    // Methods defined in ServiceOperations
    //
    ///////////////////////////////////////////////////////////////////////
    
    public int version()
	{
        if(logCat.isDebugEnabled()) {
           logCat.debug("Entering version().");
        }

        
        return m_version;
	}


    public deaf.ServicePolicy policy()
	{
        if(logCat.isDebugEnabled()) {
           logCat.debug("Entering policy().");
        }

        
        return m_policy;
	}


    ///////////////////////////////////////////////////////////////////////
    //
    // Server-side API's
    //
    ///////////////////////////////////////////////////////////////////////

    
    
}



