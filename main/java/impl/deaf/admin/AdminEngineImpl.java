/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: AdminEngineImpl.java,v 1.4 2001/08/17 06:10:15 hyoon Exp $ 
 */ 
package deaf.admin;

import deaf.*;

import org.omg.CORBA.*;


public class AdminEngineImpl
    extends EngineImpl
{
    public deaf.app.ModuleManagerProxy getModuleManager(
        deaf.SessionToken token
    ) throws deaf.AppException
	{
        return null;
	}


    public deaf.admin.LocatorManagerProxy getLocatorManager(
        deaf.SessionToken token
    ) throws deaf.AppException
	{
        return null;
	}


    public deaf.admin.PropManagerProxy getPropManager(
        deaf.SessionToken token
    ) throws deaf.AppException
	{
        return null;
	}


    public deaf.admin.LogManagerProxy getLogManager(
        deaf.SessionToken token
    ) throws deaf.AppException
	{
        return null;
	}


    public void startManager(
        deaf.SessionToken token,
        deaf.ManagerHandle hand
    ) throws deaf.AppException
	{

	}


    public void shutdownAllManagers(
        deaf.SessionToken token
    ) throws deaf.AppException
	{

	}


    public void shutdownManager(
        deaf.SessionToken token,
        deaf.ManagerHandle hand
    ) throws deaf.AppException
	{

	}


}



