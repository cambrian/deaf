/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: LogEngineImpl.java,v 1.4 2001/08/17 06:10:15 hyoon Exp $ 
 */ 
package deaf.admin;

import deaf.*;

import org.omg.CORBA.*;


public class LogEngineImpl
    extends EngineImpl
{
    public java.lang.String locale()
	{
        return null;
	}


    public void locale(
        java.lang.String _val
    )
	{

	}


    public java.lang.String format()
	{
        return null;
	}


    public void format(
        java.lang.String _val
    )
	{

	}


    public int defaultLevel()
	{
        return 0;
	}


    public void defaultLevel(
        int _val
    )
	{

	}


    public void writeMessage(
        int level,
        java.lang.String message
    ) throws deaf.AppException
	{

	}


}



