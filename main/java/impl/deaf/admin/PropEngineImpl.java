/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: PropEngineImpl.java,v 1.5 2001/08/17 06:10:15 hyoon Exp $ 
 */ 
package deaf.admin;

import  deaf.*;

import  java.util.*;

import  org.omg.CORBA.*;
import  org.apache.log4j.*;


public class PropEngineImpl
    extends EngineImpl
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(PropEngineImpl.class);
    }


    // TODO: Internal data structure should be tree...
    
    //
    private Map m_properties = new HashMap();
    

    
    /**
     * Constructor.
     */
    public PropEngineImpl()
    {

    }
    
    

    ///////////////////////////////////////////////////////////////////////
    //
    // Methods defined in PropEngineOperations
    //
    ///////////////////////////////////////////////////////////////////////

    public deaf.Property getProperty(
        java.lang.String name
    ) throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering getProperty(). String=" + name);
        }

        
        deaf.Property prop = null;
        if(m_properties.containsKey(name))
        {
            prop = new deaf.Property() {};
            prop.name = name;
            prop.value = (String) m_properties.get(name);
        }
        return prop;
	}


    public java.lang.String getPropertyValue(
        java.lang.String name
    ) throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering getPropertyValue(). String=" + name);
        }


        return (String) m_properties.get(name);
	}


    public void setProperty(
        deaf.Property prop
    ) throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
            logCat.debug("Entering setProperty(). Property=" + prop);
        }

        if(m_properties.containsKey(prop.name))
        {
            m_properties.remove(prop.name);
        }

        m_properties.put(prop.name, prop.value);
	}



    ///////////////////////////////////////////////////////////////////////
    //
    // Server-side API's
    //
    ///////////////////////////////////////////////////////////////////////



}



