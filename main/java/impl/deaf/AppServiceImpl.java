/** 
 *  Copyright &copy; 1998-2001. Hyoungsoo Yoon. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,               
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.        
 *  
 *  $Id: AppServiceImpl.java,v 1.5 2001/08/17 06:10:15 hyoon Exp $ 
 */ 
package deaf;

import  deaf.app.*;

import  org.apache.log4j.*;


public class AppServiceImpl
    extends ServiceImpl
{
    // Static log category variable for Log4j.
    static private Category logCat = null;
    static {
        logCat = Category.getInstance(AppServiceImpl.class);
    }

    
    //
    private AppManagerProxy m_appManager = null;

    
    /**
     * Constructor.
     */
    public AppServiceImpl()
    {
        // temporary
        //m_appManager = (new AppManagerProxy()).getAppManagerReference();
    }



    ///////////////////////////////////////////////////////////////////////
    //
    // Methods defined in AppServiceOperations
    //
    ///////////////////////////////////////////////////////////////////////

    public deaf.app.AppManagerProxy getAppManager() throws deaf.AppException
	{
        if(logCat.isDebugEnabled()) {
           logCat.debug("Entering getAppManager().");
        }
        

        //return m_appManager;
        return new AppManagerProxy();
	}



    ///////////////////////////////////////////////////////////////////////
    //
    // Server-side API's
    //
    ///////////////////////////////////////////////////////////////////////



    
}



