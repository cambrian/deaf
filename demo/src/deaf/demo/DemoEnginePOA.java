package deaf.demo;

//
// Interface definition : DemoEngine
//
// @author OpenORB Compiler
//
public abstract class DemoEnginePOA extends org.omg.PortableServer.Servant
		implements DemoEngineOperations, org.omg.CORBA.portable.InvokeHandler
{
	public DemoEngine _this()
	{
		return DemoEngineHelper.narrow(_this_object());
	}

	public DemoEngine _this( org.omg.CORBA.ORB orb )
	{
		return DemoEngineHelper.narrow(_this_object(orb));
	}

	private static String [] _ids_list =
	{
		"IDL:deaf/demo/DemoEngine:1.0", 
		"IDL:deaf/SessionEngine:1.0", 
		"IDL:deaf/Engine:1.0"
	};

	public String[] _all_interfaces( org.omg.PortableServer.POA poa, byte [] objectId)
	{
		return _ids_list;
	}

	private static java.util.Hashtable _ops_Hash = new java.util.Hashtable();
	static {
		_ops_Hash.put("_get_creation_time", new Integer(0) );
		_ops_Hash.put("_get_last_accessed_time", new Integer(4) );
		_ops_Hash.put("_get_max_inactive_interval", new Integer(8) );
		_ops_Hash.put("_set_max_inactive_interval", new Integer(9) );
		_ops_Hash.put("getId", new Integer(12) );
		_ops_Hash.put("getAttribute", new Integer(16) );
		_ops_Hash.put("setAttribute", new Integer(20) );
		_ops_Hash.put("getManager", new Integer(24) );
		_ops_Hash.put("isIdentical", new Integer(28) );
		_ops_Hash.put("invoke", new Integer(32) );
	};

	public org.omg.CORBA.portable.OutputStream _invoke ( String opName,
	                                                     org.omg.CORBA.portable.InputStream _is,
	                                                     org.omg.CORBA.portable.ResponseHandler handler)
	{
		org.omg.CORBA.portable.OutputStream _output = null;

		Integer opidx = (Integer)_ops_Hash.get(opName);
		if(opidx == null)
			throw new org.omg.CORBA.BAD_OPERATION();

		switch(opidx.intValue()) {
		case 0:
		{
			int arg = creation_time();
			_output = handler.createReply();
			_output.write_long(arg);
			return _output;
		}
		case 4:
		{
			int arg = last_accessed_time();
			_output = handler.createReply();
			_output.write_long(arg);
			return _output;
		}
		case 8:
		{
			int arg = max_inactive_interval();
			_output = handler.createReply();
			_output.write_long(arg);
			return _output;
		}
		case 9:
		{
			int result = _is.read_long();

			max_inactive_interval( result );
			_output = handler.createReply();
			return _output;
		}
		case 12:
		{

			java.lang.String _arg_result = getId();

			_output = handler.createReply();
			_output.write_string(_arg_result);

			return _output;
		}
		case 16:
		{
			java.lang.String arg0_in = _is.read_string();

			java.lang.Object _arg_result = getAttribute(arg0_in);

			_output = handler.createReply();
			org.omg.CORBA.AbstractBaseHelper.write(_output,_arg_result);

			return _output;
		}
		case 20:
		{
			java.lang.String arg0_in = _is.read_string();
			java.lang.Object arg1_in = org.omg.CORBA.AbstractBaseHelper.read(_is);

			setAttribute(arg0_in, arg1_in);

			_output = handler.createReply();

			return _output;
		}
		case 24:
		{

			deaf.Manager _arg_result = getManager();

			_output = handler.createReply();
			deaf.ManagerHelper.write(_output,_arg_result);

			return _output;
		}
		case 28:
		{
			deaf.Engine arg0_in = deaf.EngineHelper.read(_is);

			boolean _arg_result = isIdentical(arg0_in);

			_output = handler.createReply();
			_output.write_boolean(_arg_result);

			return _output;
		}
		case 32:
		{
			java.lang.String arg0_in = _is.read_string();
			org.omg.CORBA.StringHolder arg1_out = new org.omg.CORBA.StringHolder();

			invoke(arg0_in, arg1_out);

			_output = handler.createReply();

			_output.write_string(arg1_out.value);
			return _output;
		}
		}

		// never reached
		throw new org.omg.CORBA.INTERNAL();
	}
}
