package deaf.demo;

//
// Interface definition : DemoManager
//
// @author OpenORB Compiler
//
public abstract class DemoManagerPOA extends org.omg.PortableServer.Servant
		implements DemoManagerOperations, org.omg.CORBA.portable.InvokeHandler
{
	public DemoManager _this()
	{
		return DemoManagerHelper.narrow(_this_object());
	}

	public DemoManager _this( org.omg.CORBA.ORB orb )
	{
		return DemoManagerHelper.narrow(_this_object(orb));
	}

	private static String [] _ids_list =
	{
		"IDL:deaf/demo/DemoManager:1.0", 
		"IDL:deaf/SessionManager:1.0", 
		"IDL:deaf/Manager:1.0"
	};

	public String[] _all_interfaces( org.omg.PortableServer.POA poa, byte [] objectId)
	{
		return _ids_list;
	}

	public org.omg.CORBA.portable.OutputStream _invoke ( String opName,
	                                                     org.omg.CORBA.portable.InputStream _is,
	                                                     org.omg.CORBA.portable.ResponseHandler handler)
	{
		org.omg.CORBA.portable.OutputStream _output = null;

		if ( opName.equals("remove") )
		{
			deaf.Engine arg0_in = deaf.EngineHelper.read(_is);

			remove(arg0_in);

			_output = handler.createReply();

			return _output;
		}
		else
		if ( opName.equals("getEngine") )
		{

			deaf.Engine _arg_result = getEngine();

			_output = handler.createReply();
			deaf.EngineHelper.write(_output,_arg_result);

			return _output;
		}
		else
		if ( opName.equals("getEngineList") )
		{

			deaf.Engine[] _arg_result = getEngineList();

			_output = handler.createReply();
			deaf.EngineListHelper.write(_output,_arg_result);

			return _output;
		}
		else
			throw new org.omg.CORBA.BAD_OPERATION();
	}
}
