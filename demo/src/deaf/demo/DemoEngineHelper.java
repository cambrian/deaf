package deaf.demo;

//
// Helper class for : DemoEngine
//
// @author OpenORB Compiler
//
public class DemoEngineHelper
{
	//
	// Insert DemoEngine into an any
	// @param	a an any
	// @param	t DemoEngine value
	//
	public static void insert( org.omg.CORBA.Any a, deaf.demo.DemoEngine t )
	{
		a.insert_Object( t , type() );
	}

	//
	// Extract DemoEngine from an any
	// @param	a an any
	// @return the extracted DemoEngine value
	//
	public static deaf.demo.DemoEngine extract( org.omg.CORBA.Any a )
	{
		if ( !a.type().equal( type() ) )
			throw new org.omg.CORBA.MARSHAL();
		try {
			return deaf.demo.DemoEngineHelper.narrow( a.extract_Object() );
		}
		catch ( org.omg.CORBA.BAD_PARAM ex ) {
			throw new org.omg.CORBA.MARSHAL();
		}
	}

	//
	// Internal TypeCode value
	//
	private static org.omg.CORBA.TypeCode _tc = null;
	private static boolean _working = false;

	//
	// Return the DemoEngine TypeCode
	// @return a TypeCode
	//
	public static org.omg.CORBA.TypeCode type()
	{
		if ( _tc != null )
			return _tc;
		else
		{
		  synchronized(org.omg.CORBA.TypeCode.class)
		  {
			if ( _working )
				return org.omg.CORBA.ORB.init().create_recursive_tc( id() );
			_working = true;
			org.omg.CORBA.ORB orb = org.omg.CORBA.ORB.init();
			_tc = orb.create_interface_tc(id(),"DemoEngine");
			return _tc;
		  }
		}
	}

	//
	// Return the DemoEngine IDL ID
	// @return an ID
	//
	public static String id()
	{
		return _id;
	}

	private final static String _id = "IDL:deaf/demo/DemoEngine:1.0";

	//
	// Read DemoEngine from a marshalled stream
	// @param	istream the input stream
	// @return the readed DemoEngine value
	//
	public static deaf.demo.DemoEngine read( org.omg.CORBA.portable.InputStream istream )
	{
		return( deaf.demo.DemoEngine )istream.read_Object( deaf.demo._DemoEngineStub.class );
	}

	//
	// Write DemoEngine into a marshalled stream
	// @param	ostream the output stream
	// @param	value DemoEngine value
	//
	public static void write( org.omg.CORBA.portable.OutputStream ostream, deaf.demo.DemoEngine value )
	{
		ostream.write_Object((org.omg.CORBA.portable.ObjectImpl)value);
	}

	//
	// Narrow CORBA::Object to DemoEngine
	// @param	obj the CORBA Object
	// @return DemoEngine Object
	//
	public static DemoEngine narrow( org.omg.CORBA.Object obj )
	{
		if ( obj == null )
		   return null;

		try
		{
			return ( DemoEngine)obj;
		}
		catch ( ClassCastException ex )
		{
		}

		if ( obj._is_a( id() ) )
		{
			_DemoEngineStub stub = new _DemoEngineStub();
			stub._set_delegate( ((org.omg.CORBA.portable.ObjectImpl)obj)._get_delegate() );
			return stub;
		}

		throw new org.omg.CORBA.BAD_PARAM();
	}

}
