package deaf.demo;

//
// Interface definition : DemoManager
//
// @author OpenORB Compiler
//
public interface DemoManager extends DemoManagerOperations, deaf.SessionManager, org.omg.CORBA.Object, org.omg.CORBA.portable.IDLEntity
{
}
