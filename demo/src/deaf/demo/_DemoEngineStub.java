package deaf.demo;

//
// Interface definition : DemoEngine
//
// @author OpenORB Compiler
//
public class _DemoEngineStub extends org.omg.CORBA.portable.ObjectImpl
		implements DemoEngine
{
	static final String[] _ids_list =
	{
		"IDL:deaf/demo/DemoEngine:1.0", 
		"IDL:deaf/SessionEngine:1.0", 
		"IDL:deaf/Engine:1.0"
	};

	public String[] _ids()
	{
		return _ids_list;
	}

	final public static java.lang.Class _opsClass = deaf.demo.DemoEngineOperations.class;

	//
	// Read accessor for creation_time attribute
	// @return	the attribute value
	//
	public int creation_time()
	{
		while( true )
		{
			if (!this._is_local() )
			{
				org.omg.CORBA.portable.InputStream _input = null;
				try
				{
					org.omg.CORBA.portable.OutputStream _output = this._request("_get_creation_time",true);
					_input = this._invoke(_output);
					return _input.read_long();
				}
				catch( org.omg.CORBA.portable.RemarshalException _exception )
				{
					continue;
				}
				catch( org.omg.CORBA.portable.ApplicationException _exception )
				{
					java.lang.String _exception_id = _exception.getId();
					throw new org.omg.CORBA.UNKNOWN("Unexcepected User Exception: "+ _exception_id);
				}
				finally
				{
					this._releaseReply(_input);
				}
			}
			else
			{
				org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("_get_creation_time",_opsClass);
				if ( _so == null )
				   continue;
				deaf.SessionEngineOperations _self = ( deaf.SessionEngineOperations ) _so.servant;
				try
				{
					return _self.creation_time();
				}
				finally
				{
					_servant_postinvoke(_so);
				}
			}
		}
	}

	//
	// Read accessor for last_accessed_time attribute
	// @return	the attribute value
	//
	public int last_accessed_time()
	{
		while( true )
		{
			if (!this._is_local() )
			{
				org.omg.CORBA.portable.InputStream _input = null;
				try
				{
					org.omg.CORBA.portable.OutputStream _output = this._request("_get_last_accessed_time",true);
					_input = this._invoke(_output);
					return _input.read_long();
				}
				catch( org.omg.CORBA.portable.RemarshalException _exception )
				{
					continue;
				}
				catch( org.omg.CORBA.portable.ApplicationException _exception )
				{
					java.lang.String _exception_id = _exception.getId();
					throw new org.omg.CORBA.UNKNOWN("Unexcepected User Exception: "+ _exception_id);
				}
				finally
				{
					this._releaseReply(_input);
				}
			}
			else
			{
				org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("_get_last_accessed_time",_opsClass);
				if ( _so == null )
				   continue;
				deaf.SessionEngineOperations _self = ( deaf.SessionEngineOperations ) _so.servant;
				try
				{
					return _self.last_accessed_time();
				}
				finally
				{
					_servant_postinvoke(_so);
				}
			}
		}
	}

	//
	// Read accessor for max_inactive_interval attribute
	// @return	the attribute value
	//
	public int max_inactive_interval()
	{
		while( true )
		{
			if (!this._is_local() )
			{
				org.omg.CORBA.portable.InputStream _input = null;
				try
				{
					org.omg.CORBA.portable.OutputStream _output = this._request("_get_max_inactive_interval",true);
					_input = this._invoke(_output);
					return _input.read_long();
				}
				catch( org.omg.CORBA.portable.RemarshalException _exception )
				{
					continue;
				}
				catch( org.omg.CORBA.portable.ApplicationException _exception )
				{
					java.lang.String _exception_id = _exception.getId();
					throw new org.omg.CORBA.UNKNOWN("Unexcepected User Exception: "+ _exception_id);
				}
				finally
				{
					this._releaseReply(_input);
				}
			}
			else
			{
				org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("_get_max_inactive_interval",_opsClass);
				if ( _so == null )
				   continue;
				deaf.SessionEngineOperations _self = ( deaf.SessionEngineOperations ) _so.servant;
				try
				{
					return _self.max_inactive_interval();
				}
				finally
				{
					_servant_postinvoke(_so);
				}
			}
		}
	}

	//
	// Write accessor for max_inactive_interval attribute
	// @param	value	the attribute value
	//
	public void max_inactive_interval( int value )
	{
		while( true )
		{
			if (!this._is_local() )
			{
					org.omg.CORBA.portable.InputStream _input = null;
				try
				{
					org.omg.CORBA.portable.OutputStream _output = this._request("_set_max_inactive_interval",true);
					_output.write_long(value);
					_input = this._invoke(_output);
					return;
				}
				catch( org.omg.CORBA.portable.RemarshalException _exception )
				{
					continue;
				}
				catch( org.omg.CORBA.portable.ApplicationException _exception )
				{
					java.lang.String _exception_id = _exception.getId();
					throw new org.omg.CORBA.UNKNOWN("Unexcepected User Exception: "+ _exception_id);
				}
				finally
				{
					this._releaseReply(_input);
				}
			}
			else
			{
				org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("_set_max_inactive_interval",_opsClass);
				if ( _so == null )
				   continue;
				deaf.SessionEngineOperations _self = ( deaf.SessionEngineOperations ) _so.servant;
				try
				{
					_self.max_inactive_interval(value);
				}
				finally
				{
					_servant_postinvoke(_so);
				}
			}
		}
	}

	//
	// Operation getId
	//
	public java.lang.String getId()
	{
		while( true )
		{
			if (!this._is_local() )
			{
				org.omg.CORBA.portable.InputStream _input = null;
				try
				{
					org.omg.CORBA.portable.OutputStream _output = this._request("getId",true);
					_input = this._invoke(_output);
					java.lang.String _arg_ret = _input.read_string();
					return _arg_ret;
				}
				catch( org.omg.CORBA.portable.RemarshalException _exception )
				{
					continue;
				}
				catch( org.omg.CORBA.portable.ApplicationException _exception )
				{
					java.lang.String _exception_id = _exception.getId();
					throw new org.omg.CORBA.UNKNOWN("Unexcepected User Exception: "+ _exception_id);
				}
				finally
				{
					this._releaseReply(_input);
				}
			}
			else
			{
				org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("getId",_opsClass);
				if ( _so == null )
				   continue;
				deaf.SessionEngineOperations _self = ( deaf.SessionEngineOperations ) _so.servant;
				try
				{
					return _self.getId();
				}
				finally
				{
					_servant_postinvoke(_so);
				}
			}
		}
	}

	//
	// Operation getAttribute
	//
	public java.lang.Object getAttribute(java.lang.String name)
	{
		while( true )
		{
			if (!this._is_local() )
			{
				org.omg.CORBA.portable.InputStream _input = null;
				try
				{
					org.omg.CORBA.portable.OutputStream _output = this._request("getAttribute",true);
					_output.write_string(name);
					_input = this._invoke(_output);
					java.lang.Object _arg_ret = org.omg.CORBA.AbstractBaseHelper.read(_input);
					return _arg_ret;
				}
				catch( org.omg.CORBA.portable.RemarshalException _exception )
				{
					continue;
				}
				catch( org.omg.CORBA.portable.ApplicationException _exception )
				{
					java.lang.String _exception_id = _exception.getId();
					throw new org.omg.CORBA.UNKNOWN("Unexcepected User Exception: "+ _exception_id);
				}
				finally
				{
					this._releaseReply(_input);
				}
			}
			else
			{
				org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("getAttribute",_opsClass);
				if ( _so == null )
				   continue;
				deaf.SessionEngineOperations _self = ( deaf.SessionEngineOperations ) _so.servant;
				try
				{
					return _self.getAttribute( name);
				}
				finally
				{
					_servant_postinvoke(_so);
				}
			}
		}
	}

	//
	// Operation setAttribute
	//
	public void setAttribute(java.lang.String name, java.lang.Object value)
	{
		while( true )
		{
			if (!this._is_local() )
			{
				org.omg.CORBA.portable.InputStream _input = null;
				try
				{
					org.omg.CORBA.portable.OutputStream _output = this._request("setAttribute",true);
					_output.write_string(name);
					org.omg.CORBA.AbstractBaseHelper.write(_output,value);
					_input = this._invoke(_output);
					return;
				}
				catch( org.omg.CORBA.portable.RemarshalException _exception )
				{
					continue;
				}
				catch( org.omg.CORBA.portable.ApplicationException _exception )
				{
					java.lang.String _exception_id = _exception.getId();
					throw new org.omg.CORBA.UNKNOWN("Unexcepected User Exception: "+ _exception_id);
				}
				finally
				{
					this._releaseReply(_input);
				}
			}
			else
			{
				org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("setAttribute",_opsClass);
				if ( _so == null )
				   continue;
				deaf.SessionEngineOperations _self = ( deaf.SessionEngineOperations ) _so.servant;
				try
				{
					_self.setAttribute( name,  value);
					return;
				}
				finally
				{
					_servant_postinvoke(_so);
				}
			}
		}
	}

	//
	// Operation getManager
	//
	public deaf.Manager getManager()
	{
		while( true )
		{
			if (!this._is_local() )
			{
				org.omg.CORBA.portable.InputStream _input = null;
				try
				{
					org.omg.CORBA.portable.OutputStream _output = this._request("getManager",true);
					_input = this._invoke(_output);
					deaf.Manager _arg_ret = deaf.ManagerHelper.read(_input);
					return _arg_ret;
				}
				catch( org.omg.CORBA.portable.RemarshalException _exception )
				{
					continue;
				}
				catch( org.omg.CORBA.portable.ApplicationException _exception )
				{
					java.lang.String _exception_id = _exception.getId();
					throw new org.omg.CORBA.UNKNOWN("Unexcepected User Exception: "+ _exception_id);
				}
				finally
				{
					this._releaseReply(_input);
				}
			}
			else
			{
				org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("getManager",_opsClass);
				if ( _so == null )
				   continue;
				deaf.EngineOperations _self = ( deaf.EngineOperations ) _so.servant;
				try
				{
					return _self.getManager();
				}
				finally
				{
					_servant_postinvoke(_so);
				}
			}
		}
	}

	//
	// Operation isIdentical
	//
	public boolean isIdentical(deaf.Engine engine)
	{
		while( true )
		{
			if (!this._is_local() )
			{
				org.omg.CORBA.portable.InputStream _input = null;
				try
				{
					org.omg.CORBA.portable.OutputStream _output = this._request("isIdentical",true);
					deaf.EngineHelper.write(_output,engine);
					_input = this._invoke(_output);
					boolean _arg_ret = _input.read_boolean();
					return _arg_ret;
				}
				catch( org.omg.CORBA.portable.RemarshalException _exception )
				{
					continue;
				}
				catch( org.omg.CORBA.portable.ApplicationException _exception )
				{
					java.lang.String _exception_id = _exception.getId();
					throw new org.omg.CORBA.UNKNOWN("Unexcepected User Exception: "+ _exception_id);
				}
				finally
				{
					this._releaseReply(_input);
				}
			}
			else
			{
				org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("isIdentical",_opsClass);
				if ( _so == null )
				   continue;
				deaf.EngineOperations _self = ( deaf.EngineOperations ) _so.servant;
				try
				{
					return _self.isIdentical( engine);
				}
				finally
				{
					_servant_postinvoke(_so);
				}
			}
		}
	}

	//
	// Operation invoke
	//
	public void invoke(java.lang.String command, org.omg.CORBA.StringHolder result)
	{
		while( true )
		{
			if (!this._is_local() )
			{
				org.omg.CORBA.portable.InputStream _input = null;
				try
				{
					org.omg.CORBA.portable.OutputStream _output = this._request("invoke",true);
					_output.write_string(command);
					_input = this._invoke(_output);
					result.value = _input.read_string();
					return;
				}
				catch( org.omg.CORBA.portable.RemarshalException _exception )
				{
					continue;
				}
				catch( org.omg.CORBA.portable.ApplicationException _exception )
				{
					java.lang.String _exception_id = _exception.getId();
					throw new org.omg.CORBA.UNKNOWN("Unexcepected User Exception: "+ _exception_id);
				}
				finally
				{
					this._releaseReply(_input);
				}
			}
			else
			{
				org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("invoke",_opsClass);
				if ( _so == null )
				   continue;
				deaf.EngineOperations _self = ( deaf.EngineOperations ) _so.servant;
				try
				{
					_self.invoke( command,  result);
					return;
				}
				finally
				{
					_servant_postinvoke(_so);
				}
			}
		}
	}

}
