package deaf.demo;

//
// Interface definition : DemoManager
//
// @author OpenORB Compiler
//
public class _DemoManagerStub extends org.omg.CORBA.portable.ObjectImpl
		implements DemoManager
{
	static final String[] _ids_list =
	{
		"IDL:deaf/demo/DemoManager:1.0", 
		"IDL:deaf/SessionManager:1.0", 
		"IDL:deaf/Manager:1.0"
	};

	public String[] _ids()
	{
		return _ids_list;
	}

	final public static java.lang.Class _opsClass = deaf.demo.DemoManagerOperations.class;

	//
	// Operation remove
	//
	public void remove(deaf.Engine engine)
	{
		while( true )
		{
			if (!this._is_local() )
			{
				org.omg.CORBA.portable.InputStream _input = null;
				try
				{
					org.omg.CORBA.portable.OutputStream _output = this._request("remove",true);
					deaf.EngineHelper.write(_output,engine);
					_input = this._invoke(_output);
					return;
				}
				catch( org.omg.CORBA.portable.RemarshalException _exception )
				{
					continue;
				}
				catch( org.omg.CORBA.portable.ApplicationException _exception )
				{
					java.lang.String _exception_id = _exception.getId();
					throw new org.omg.CORBA.UNKNOWN("Unexcepected User Exception: "+ _exception_id);
				}
				finally
				{
					this._releaseReply(_input);
				}
			}
			else
			{
				org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("remove",_opsClass);
				if ( _so == null )
				   continue;
				deaf.ManagerOperations _self = ( deaf.ManagerOperations ) _so.servant;
				try
				{
					_self.remove( engine);
					return;
				}
				finally
				{
					_servant_postinvoke(_so);
				}
			}
		}
	}

	//
	// Operation getEngine
	//
	public deaf.Engine getEngine()
	{
		while( true )
		{
			if (!this._is_local() )
			{
				org.omg.CORBA.portable.InputStream _input = null;
				try
				{
					org.omg.CORBA.portable.OutputStream _output = this._request("getEngine",true);
					_input = this._invoke(_output);
					deaf.Engine _arg_ret = deaf.EngineHelper.read(_input);
					return _arg_ret;
				}
				catch( org.omg.CORBA.portable.RemarshalException _exception )
				{
					continue;
				}
				catch( org.omg.CORBA.portable.ApplicationException _exception )
				{
					java.lang.String _exception_id = _exception.getId();
					throw new org.omg.CORBA.UNKNOWN("Unexcepected User Exception: "+ _exception_id);
				}
				finally
				{
					this._releaseReply(_input);
				}
			}
			else
			{
				org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("getEngine",_opsClass);
				if ( _so == null )
				   continue;
				deaf.ManagerOperations _self = ( deaf.ManagerOperations ) _so.servant;
				try
				{
					return _self.getEngine();
				}
				finally
				{
					_servant_postinvoke(_so);
				}
			}
		}
	}

	//
	// Operation getEngineList
	//
	public deaf.Engine[] getEngineList()
	{
		while( true )
		{
			if (!this._is_local() )
			{
				org.omg.CORBA.portable.InputStream _input = null;
				try
				{
					org.omg.CORBA.portable.OutputStream _output = this._request("getEngineList",true);
					_input = this._invoke(_output);
					deaf.Engine[] _arg_ret = deaf.EngineListHelper.read(_input);
					return _arg_ret;
				}
				catch( org.omg.CORBA.portable.RemarshalException _exception )
				{
					continue;
				}
				catch( org.omg.CORBA.portable.ApplicationException _exception )
				{
					java.lang.String _exception_id = _exception.getId();
					throw new org.omg.CORBA.UNKNOWN("Unexcepected User Exception: "+ _exception_id);
				}
				finally
				{
					this._releaseReply(_input);
				}
			}
			else
			{
				org.omg.CORBA.portable.ServantObject _so = _servant_preinvoke("getEngineList",_opsClass);
				if ( _so == null )
				   continue;
				deaf.ManagerOperations _self = ( deaf.ManagerOperations ) _so.servant;
				try
				{
					return _self.getEngineList();
				}
				finally
				{
					_servant_postinvoke(_so);
				}
			}
		}
	}

}
