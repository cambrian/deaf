package deaf.demo;

//
// Helper class for : DemoManager
//
// @author OpenORB Compiler
//
public class DemoManagerHelper
{
	//
	// Insert DemoManager into an any
	// @param	a an any
	// @param	t DemoManager value
	//
	public static void insert( org.omg.CORBA.Any a, deaf.demo.DemoManager t )
	{
		a.insert_Object( t , type() );
	}

	//
	// Extract DemoManager from an any
	// @param	a an any
	// @return the extracted DemoManager value
	//
	public static deaf.demo.DemoManager extract( org.omg.CORBA.Any a )
	{
		if ( !a.type().equal( type() ) )
			throw new org.omg.CORBA.MARSHAL();
		try {
			return deaf.demo.DemoManagerHelper.narrow( a.extract_Object() );
		}
		catch ( org.omg.CORBA.BAD_PARAM ex ) {
			throw new org.omg.CORBA.MARSHAL();
		}
	}

	//
	// Internal TypeCode value
	//
	private static org.omg.CORBA.TypeCode _tc = null;
	private static boolean _working = false;

	//
	// Return the DemoManager TypeCode
	// @return a TypeCode
	//
	public static org.omg.CORBA.TypeCode type()
	{
		if ( _tc != null )
			return _tc;
		else
		{
		  synchronized(org.omg.CORBA.TypeCode.class)
		  {
			if ( _working )
				return org.omg.CORBA.ORB.init().create_recursive_tc( id() );
			_working = true;
			org.omg.CORBA.ORB orb = org.omg.CORBA.ORB.init();
			_tc = orb.create_interface_tc(id(),"DemoManager");
			return _tc;
		  }
		}
	}

	//
	// Return the DemoManager IDL ID
	// @return an ID
	//
	public static String id()
	{
		return _id;
	}

	private final static String _id = "IDL:deaf/demo/DemoManager:1.0";

	//
	// Read DemoManager from a marshalled stream
	// @param	istream the input stream
	// @return the readed DemoManager value
	//
	public static deaf.demo.DemoManager read( org.omg.CORBA.portable.InputStream istream )
	{
		return( deaf.demo.DemoManager )istream.read_Object( deaf.demo._DemoManagerStub.class );
	}

	//
	// Write DemoManager into a marshalled stream
	// @param	ostream the output stream
	// @param	value DemoManager value
	//
	public static void write( org.omg.CORBA.portable.OutputStream ostream, deaf.demo.DemoManager value )
	{
		ostream.write_Object((org.omg.CORBA.portable.ObjectImpl)value);
	}

	//
	// Narrow CORBA::Object to DemoManager
	// @param	obj the CORBA Object
	// @return DemoManager Object
	//
	public static DemoManager narrow( org.omg.CORBA.Object obj )
	{
		if ( obj == null )
		   return null;

		try
		{
			return ( DemoManager)obj;
		}
		catch ( ClassCastException ex )
		{
		}

		if ( obj._is_a( id() ) )
		{
			_DemoManagerStub stub = new _DemoManagerStub();
			stub._set_delegate( ((org.omg.CORBA.portable.ObjectImpl)obj)._get_delegate() );
			return stub;
		}

		throw new org.omg.CORBA.BAD_PARAM();
	}

}
