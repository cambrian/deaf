package deaf.demo;

//
// Holder class for : DemoManager
//
// @author OpenORB Compiler
//
final public class DemoManagerHolder
		implements org.omg.CORBA.portable.Streamable
{
	//
	// Internal DemoManager value
	//
	public deaf.demo.DemoManager value;

	//
	// Default constructor
	//
	public DemoManagerHolder()
	{ }

	//
	// Constructor with value initialisation
	// @param	initial	the initial value
	//
	public DemoManagerHolder( deaf.demo.DemoManager initial )
	{
		value = initial;
	}

	//
	// Read DemoManager from a marshalled stream
	// @param	istream the input stream
	//
	public void _read( org.omg.CORBA.portable.InputStream istream )
	{
		value = DemoManagerHelper.read(istream);
	}

	//
	// Write DemoManager into a marshalled stream
	// @param	ostream the output stream
	//
	public void _write( org.omg.CORBA.portable.OutputStream ostream )
	{
		DemoManagerHelper.write(ostream,value);
	}

	//
	// Return the DemoManager TypeCode
	// @return a TypeCode
	//
	public org.omg.CORBA.TypeCode _type()
	{
		return DemoManagerHelper.type();
	}

}
