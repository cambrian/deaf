package deaf.demo;

//
// Holder class for : DemoEngine
//
// @author OpenORB Compiler
//
final public class DemoEngineHolder
		implements org.omg.CORBA.portable.Streamable
{
	//
	// Internal DemoEngine value
	//
	public deaf.demo.DemoEngine value;

	//
	// Default constructor
	//
	public DemoEngineHolder()
	{ }

	//
	// Constructor with value initialisation
	// @param	initial	the initial value
	//
	public DemoEngineHolder( deaf.demo.DemoEngine initial )
	{
		value = initial;
	}

	//
	// Read DemoEngine from a marshalled stream
	// @param	istream the input stream
	//
	public void _read( org.omg.CORBA.portable.InputStream istream )
	{
		value = DemoEngineHelper.read(istream);
	}

	//
	// Write DemoEngine into a marshalled stream
	// @param	ostream the output stream
	//
	public void _write( org.omg.CORBA.portable.OutputStream ostream )
	{
		DemoEngineHelper.write(ostream,value);
	}

	//
	// Return the DemoEngine TypeCode
	// @return a TypeCode
	//
	public org.omg.CORBA.TypeCode _type()
	{
		return DemoEngineHelper.type();
	}

}
