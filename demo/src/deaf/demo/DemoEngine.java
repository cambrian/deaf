package deaf.demo;

//
// Interface definition : DemoEngine
//
// @author OpenORB Compiler
//
public interface DemoEngine extends DemoEngineOperations, deaf.SessionEngine, org.omg.CORBA.Object, org.omg.CORBA.portable.IDLEntity
{
}
