<html>
<!--
   $Id: helloDeaf.jsp,v 1.1.1.1 2001/02/27 06:05:05 cvs Exp $
-->

<%@ page session="false"%>
<%@ page import = "HelloDeafBean" %>

<jsp:useBean id="hellodeaf" class="HelloDeafBean" scope="session"/>
<jsp:setProperty name="hellodeaf" property="*"/>

<html>
<head><title>Hello DEAF</title></head>
<body bgcolor="white">

Hello DEAF from <%= hellodeaf.getSpeaker() %>.

</body>
</html>

