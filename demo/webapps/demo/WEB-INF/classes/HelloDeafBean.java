/*
 * $Id: HelloDeafBean.java,v 1.1.1.1 2001/02/27 06:05:05 cvs Exp $
 */

import java.util.*;

/**
 * The Hello DEAF class to be used in helloDeaf.jsp
 *
 * @author Hyoungsoo Yoon
 */
public class HelloDeafBean {

    String speaker = "Hyoungoso Yoon";
    
    public HelloDeafBean() {
    }

    public void setSpeaker(String speaker) {
        this.speaker = speaker;
    }

    public String getSpeaker() {
        return speaker;
    }
}


