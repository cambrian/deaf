/*
 * $Id: HelloDeaf.java,v 1.1.1.1 2001/02/27 06:05:05 cvs Exp $
 */

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

/**
 * The Hello DEAF servlet
 *
 * @author Hyoungsoo Yoon
 */

public class HelloDeaf extends HttpServlet {


    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
        throws IOException, ServletException
    {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        out.println("<html>");
        out.println("<head>");

	    String title = "Hello DEAF";

	    out.println("<title>" + title + "</title>");
        out.println("</head>");
        out.println("<body bgcolor=\"white\">");
        out.println("<body>");
        out.println("<h1>" + title + "</h1>");

        out.println("</body>");
        out.println("</html>");
    }
}


