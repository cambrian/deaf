@echo off

:// Set classpath.
REM .\setcp

:// Compile idl files.
echo Compile idl files...
for %%i in (..\src\idl\*.idl) do call idl2java.bat %%i

:// Compile generated java files.
echo Compile generated java files...
javac ..\src\deaf\demo\*.java

:// Compile client/server java files.
echo Compile client/server java files...
javac ..\src\deaf\demo\client *.java
javac ..\src\deaf\demo\server *.java

:// Make jar file.
cd ..\src
echo Make jar file...
jar cvf demolib.jar deaf\demo\*.class 
jar cvf demo_client.jar deaf\demo\client\*.class
jar cvf demo_server.jar deaf\demo\server\*.class

del ..\build\demolib.jar
del ..\build\demo_client.jar
del ..\build\demo_server.jar
move demolib.jar ..\build
move demo_client.jar ..\build
move demo_server.jar ..\build

cd ..\bin
