@echo off

for %%i in (H:\openorb\OpenORB\lib\*.jar) do call addcp.bat %%i
for %%i in (H:\openorb\NamingService\lib\*.jar) do call addcp.bat %%i
for %%i in (H:\openorb\InterfaceRepository\lib\*.jar) do call addcp.bat %%i
set CLASSPATH=%CLASSPATH%;%JAVA_HOME%\lib\tools.jar
set CLASSPATH=%CLASSPATH%;..\lib\deaf.jar
set CLASSPATH=%CLASSPATH%;..\src

REM echo CLASSPATH=%CLASSPATH%
REM echo.
