@echo off
rem $Id: starttomcat.bat,v 1.1.1.1 2001/02/27 06:05:05 cvs Exp $
rem Startup batch file for tomcat server.

SET __TOMCAT_HOME=%TOMCAT_HOME%
SET TOMCAT_HOME=..\tomcat-3.2.1

copy ..\conf\server.xml %TOMCAT_HOME%\conf\server.xml
copy ..\conf\tomcat.policy %TOMCAT_HOME%\conf\tomcat.policy
call "%TOMCAT_HOME%\bin\tomcat" start %1 %2 %3 %4 %5 %6 %7 %8 %9

SET TOMCAT_HOME=%__TOMCAT_HOME%
SET __TOMCAT_HOME=

