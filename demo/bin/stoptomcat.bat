@echo off
rem $Id: stoptomcat.bat,v 1.1.1.1 2001/02/27 06:05:05 cvs Exp $
rem Startup batch file for tomcat server.

SET __TOMCAT_HOME=%TOMCAT_HOME%
SET TOMCAT_HOME=..\tomcat-3.2.1

call "%TOMCAT_HOME%\bin\tomcat" stop %1 %2 %3 %4 %5 %6 %7 %8 %9

SET TOMCAT_HOME=%__TOMCAT_HOME%
SET __TOMCAT_HOME=

